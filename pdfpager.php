<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) 
{
  header ("Location: login.php"); 
}
else
{

  include"classes/class.pdfpage.php";
  include"classes/class.reaction.php"; 
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle); 
  $pdf_page = new pdf_page($orgid);


}
include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
   <div class="content-main">
		<!--banner-->	
	     <div class="banner">
	    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Pdf Page</span>
			<a href='#!' class='btn btn-sm btn-default pull-right  toggle_media_list' data-type='Pdf Page' id='0'>Add New Pdf Page</a>
			</h2>
	    </div>
	<!--//banner-->
	 	<div class="blank">
		

			<div class="blank-page" id='media_list'>
				
				<div class='gallery'>
				<div id='results2'></div>
					<div class='list-group list-group-alternate'> 
		        	<?php

		        		$pdf_page_list = $pdf_page->list_pdf();
		        		if($pdf_page_list['success'] == 1)
		        		{
		        			$pdf_page_data = $pdf_page_list['data'];
		        			for($i=0;$i<count($pdf_page_data);$i++)
		        			{
		        				$title = $pdf_page_data[$i]['title'];
		        				$pdf = $pdf_page_data[$i]['pdf'];
		        				$pdf_page_id = $pdf_page_data[$i]['id'];
		        				$status = $pdf_page_data[$i]['status'];
		        				$timestamp = $pdf_page_data[$i]['timestamp'];
		        				$btn_active = '';
		        				if($status == 0)
		        				{
		        					$btn_active = "<a href='#!' data-stat='0' id='$pdf_page_id' class='btn btn-warning pull-right activation_element'>activate</a>";
		        				}
		        				else if($status == 1)
		        				{
		        					$btn_active = "<a href='#!' data-stat='1' id='$pdf_page_id' class='btn btn-success pull-right activation_element'>active</a>";
		        				}		        				
		        				
		        				print 
		        				"
									<div class='list-group-item' id='pdf_$pdf_page_id'data-title='$title' >
										$title
										<a href='files/docs/$pdf' target='_blank' id='$pdf_page_id' class='btn btn-primary pull-right view_pdf'>view</a>
										<a href='#!' id='$pdf_page_id' class='btn btn-danger pull-right delete_pdf'>delete</a>
										$btn_active
									</div>

		        				";
		        			}

		        		}
		        		else
		        		{
		        			print
		        			"
		        				<div class='alert alert-info'> You have no  pdf pages yet.</div>

		        			";
		        		}

		        	?>
	        		</div>
				</div>
		    </div>
		    <div class="blank-page" id='add_media' style='display:none'>

		    	<div class="gallery">
				    <div id='results'></div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Title</label>
				        <input type="text" class="form-control1" id="title">
				    </div>
				    <!-- Example 2 -->
			       <input type="file" name="files[]" id="filer_input" multiple="multiple">
			       <input id="media_type" type="hidden" value='photo'/>
			       <a href='#!' id='upload_pdf' class='btn btn-primary'> Upload PDF </a>
			       <!-- end of Example 2 -->

				</div>


		    </div>
		</div>
	</div>
    <script type='text/javascript'>
        $(document).ready(function(){

          $('#filer_input').filer({
            showThumbs: true,
            addMore: true,
            allowDuplicates: false
          });

        });
    </script> 
			
		<!--//content-->
<?php
include"footer.php";
?>        