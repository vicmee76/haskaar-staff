<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {
  header ("Location: login.php");
  
    


}
else{
  include"classes/class.media.php";
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle);
  $video = new mediaset($userid,$orgid,'provider','video');
  

}
?>
<?php

include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 		<!--banner-->	
		    <div class="banner">
		    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
				<span>video</span>
				<a href='#!' class='btn btn-sm btn-default pull-right toggle_media_list' data-type='Media' id='0'>Add Media</a>
				</h2>
		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
  <div class="blank-page">
		
 	 <!--gallery-->
 	 <div class="gallery" id='media_list'>
 	 <?php 
 	 
      $video_list = $video->list_media(0);    
      if($video_list['success'] == 1)
      {
        $video_data = $video_list['data'];
        for($i=0;$i<count($video_data);$i++)
        {
          $title = $video_data[$i]['title'];
          $video_file = $video_data[$i]['video'];
          $description = $video_data[$i]['description'];
          $guid = $video_data[$i]['videoid'];

          print
          "
            <div class='media_set'>
              <div class='thumb'>
                <img src='files/sample.jpg' />
                <div class='description'>$description</div>
              </div>
              <div class='reactions'>
                <span class='likes'>2 <i class='fa fa-thumbs-up' aria-hidden='true'></i></span>
                <span class='comments'>4 <i class='fa fa-comment' aria-hidden='true'></i></span>
                <span class='reports'><i class='fa fa-warning' aria-hidden='true'></i></span>
                <span class='delete'><i class='fa fa-trash' aria-hidden='true'></i></span>
                <span class='edit'><i class='fa fa-edit' aria-hidden='true'></i></span>
              </div>
            </div>

          ";
        }
      }
      else if($video_list['success'] == 0)
      {
        print 
        "
          <div class='alert alert-warning'> You currently have no video Files</div>

        ";
      }
      else
      {
        print 
        "
          <div class='alert alert-warning'> Sorry, We could not process your request at this time! retry or refresh</div>

        ";
      }
            
    ?>
 	  </div>
	<!--//gallery-->
  <div class='gallery' id='add_media' <?php echo $hide_this;?>>
    <div id="content">
    <div id='results'></div>
    <div class="form-group">
        <label class="control-label" for="inputSuccess1">Title</label>
        <input type="text" class="form-control1" id="file_title">
    </div>
    <div class="form-group">
        <label class="control-label" for="inputSuccess1">Description</label>
        <input type="text" class="form-control1" id="file_description">
    </div>
    <!-- Example 2 -->
       <input type="file" name="files[]" id="filer_input" multiple="multiple">
       <input id="media_type" type="hidden" value='video'/>
       <a href='#!' id='upload_media' class='btn btn-primary'> Upload </a>
    <!-- end of Example 2 -->

    </div>
  </div>
	        </div>
	       </div>
         </div>
           
			<input type='hidden' id='staffname' value='<?php echo $fullname;?>'/>
      <script type='text/javascript'>
        $(document).ready(function(){

          $('#filer_input').filer({
            showThumbs: true,
            addMore: true,
            allowDuplicates: false
          });

        });
      </script> 
		<!--//content-->
<?php
include"footer.php";
?>        