<?php 
session_start();

if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {
	 header ("Location: login.php");
	
    


}
else{
	include"config.php";
	include"modules/input_module.php";
	include"modules/sql_module.php";
	$userid=$_SESSION['userid'];
	$fullname=$_SESSION['fullname'];
	$userhash=$_SESSION['loggedin'];
	last_seen($userhash,$db_handle);
	$repliedtickets=mysql_return_rows("select * from messages where senderid='$userhash'",$db_handle);
	$pendingtickets=mysql_return_rows("select * from messages where receiverid='$userhash'",$db_handle);
	$todaysGoal=mysql_return_rows("select * from messages where senderid='$userhash' and DATE(dateofmsg) = CURDATE()",$db_handle);
	$rval="0.$repliedtickets";
	$pval="0.$pendingtickets";
	$tval="0.$todaysGoal";
	$repliedPercentage=100 * (int)$rval;
	$pendingPercentage=100 * (int)$pval;
	$todayPercentage=100 * (int)$tval;

}
?>
<?php

include"header.php";
include"menu.php";
?>

<script src="js/pie-chart.js" type="text/javascript"></script>
 <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

           
        });

    </script>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 <!--banner-->	
		    <div class="banner">
		   
				<h2>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Dashboard</span>
				</h2>
		    </div>
		<!--//banner-->
 	 <!--faq-->
 	
	<div class="content-top">
			
			
			<div class="col-md-4 ">
				<div class="content-top-1">
				<div class="col-md-6 top-content">
					<h5>Pending Tickets</h5>
					<label><?php echo $pendingtickets;?></label>
				</div>
				<div class="col-md-6 top-content1">	   
					<div id="demo-pie-1" class="pie-title-center" data-percent="<?php echo $pendingPercentage;?>"> <span class="pie-value"></span> </div>
				</div>
				 <div class="clearfix"> </div>
				</div>
				<div class="content-top-1">
				<div class="col-md-6 top-content">
					<h5>Replied Tickets</h5>
					<label><?php echo $repliedtickets;?></label>
				</div>
				<div class="col-md-6 top-content1">	   
					<div id="demo-pie-2" class="pie-title-center" data-percent="<?php echo $repliedPercentage;?>"> <span class="pie-value"></span> </div>
				</div>
				 <div class="clearfix"> </div>
				</div>
				<div class="content-top-1">
				<div class="col-md-6 top-content">
					<h5>Today's Goal</h5>
					<label><?php echo $todaysGoal;?></label>
				</div>
				<div class="col-md-6 top-content1">	   
					<div id="demo-pie-3" class="pie-title-center" data-percent="<?php echo $todayPercentage?>"> <span class="pie-value"></span> </div>
				</div>
				 <div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-8 content-top-2">
				<!---start-chart---->
				<!----->
				<div class="content-graph">
				<div class="content-color">
					<div class="content-ch"><p><i></i>Live </p><span></span>
					<div class="clearfix"> </div>
					</div>
					<div class="content-ch1"><p><i></i>Feeds</p><span></span>
					<div class="clearfix"> </div>
					</div>
				</div>
				<!--graph-->
		<link rel="stylesheet" href="css/graph.css">
		<!--//graph-->

				<div class="graph-container livefeedlist" style=' overflow:hidden'>
				
            
           
								</div>
	
		</div>
		</div>
		<div class="clearfix"> </div>
		</div>
		<!---->

		<!----->
		<div class="content-bottom">
			<div class="col-md-6 post-top">
				<div class="post">
					<form class="text-area">
						<textarea   required="" > What are you doing...</textarea>
					</form>
					<div class="post-at">
						<ul class="icon">
							
							
							<div  class="post-file">
							<i class="fa fa-location-arrow"></i>
							<input id="input-1" name="input1[]" type="file" multiple="" class="">
							</div>
							<div  class="post-file">
							<i class="fa fa-camera"></i>
							<input id="input-2" name="input2[]" type="file" multiple="" class="">
							</div>
							<div  class="post-file">
							<i class="fa fa-video-camera"></i>
							<input id="input-3" name="input3[]" type="file" multiple="" class="">
							</div>
							<div  class="post-file">
							<i class="fa fa-microphone"></i>
							<input id="input-4" name="input4[]" type="file" multiple="" class="">
							</div>
							
							<script>
							$(document).on('ready', function() {
								$("#input-1").fileinput({showCaption: false});
							});
							</script>
							<script>
							$(document).on('ready', function() {
								$("#input-2").fileinput({showCaption: false});
							});
							</script>
							<script>
							$(document).on('ready', function() {
								$("#input-3").fileinput({showCaption: false});
							});
							</script>
							<script>
							$(document).on('ready', function() {
								$("#input-4").fileinput({showCaption: false});
							});
							</script>
						</ul>
						<form class="text-sub">
							<input type="submit" value="post">
						</form>
						<div class="clearfix"> </div>
					</div>
				</div>
			<div class="post-bottom">
				<div class="post-bottom-1">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<p>15k <label>Likes</label></p>
				</div>
				<div class="post-bottom-2">
					<a href="#"><i class="fa fa-twitter"></i></a>
					<p>20M <label>Followers</label></p>
				</div>
				<div class="clearfix"> </div>
			</div>
				
			</div>
			<div class="col-md-6">
				<div class="weather">
					<div class="weather-top">
						<div class="weather-top-left">
							<div class="degree">
							<figure class="icons">
								<canvas id="partly-cloudy-day" width="64" height="64">
								</canvas>
							</figure>
							<span>37°</span>
							<div class="clearfix"></div>
							</div>
				<script>
							 var icons = new Skycons({"color": "#1ABC9C"}),
								  list  = [
									"clear-night", "partly-cloudy-day",
									"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
									"fog"
								  ],
								  i;

							  for(i = list.length; i--; )
								icons.set(list[i], list[i]);

							  icons.play();
						</script>
							<p>FRIDAY
								<label>13</label>
								<sup>th</sup>
								AUG
							</p>
						</div>
						<div class="weather-top-right">
							<p><i class="fa fa-map-marker"></i>Nigeria</p>
							<label>ForeCast</label>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="weather-bottom">
			<div class="weather-bottom1">
				<div class="weather-head">
				<h4>Cloudy</h4>
				<figure class="icons">
					<canvas id="cloudy" width="58" height="58"></canvas>
				</figure>					
					<script>
							 var icons = new Skycons({"color": "#999"}),
								  list  = [
									"clear-night", "cloudy",
									"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
									"fog"
								  ],
								  i;

							  for(i = list.length; i--; )
								icons.set(list[i], list[i]);

							  icons.play();
						</script>
				<h6>20°</h6>
				<div class="bottom-head">
					<p>August 16</p>
					<p>Monday</p>
				</div>
			</div>
			</div>
			<div class="weather-bottom1 ">
				<div class="weather-head">
			<h4>Sunny</h4>
			<figure class="icons">
				<canvas id="clear-day" width="58" height="58">
				</canvas>				
			</figure>					
					<script>
							 var icons = new Skycons({"color": "#999"}),
								  list  = [
									"clear-night", "clear-day",
									"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
									"fog"
								  ],
								  i;

							  for(i = list.length; i--; )
								icons.set(list[i], list[i]);

							  icons.play();
						</script>
			<h6>37°</h6>
			<div class="bottom-head">
					<p>August 17</p>
					<p>Tuesday</p>
				</div>
			</div>
			</div>
			<div class="weather-bottom1">
				<div class="weather-head">
				<h4>Rainy</h4>
				<figure class="icons">
					<canvas id="sleet" width="58" height="58">
					</canvas>
				</figure>
				<script>
							 var icons = new Skycons({"color": "#999"}),
								  list  = [
									"clear-night", "clear-day",
									"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
									"fog"
								  ],
								  i;

							  for(i = list.length; i--; )
								icons.set(list[i], list[i]);

							  icons.play();
						</script>
		
				<h6>7°</h6>
				<div class="bottom-head">
					<p>August 18</p>
					<p>Wednesday</p>
				</div>
			</div>
			</div>
			<div class="weather-bottom1 ">
				<div class="weather-head">
			<h4>Snowy</h4>
			<figure class="icons">
					<canvas id="snow" width="58" height="58">
					</canvas>
				</figure>
				<script>
							 var icons = new Skycons({"color": "#999"}),
								  list  = [
									"clear-night", "clear-day",
									"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
									"fog"
								  ],
								  i;

							  for(i = list.length; i--; )
								icons.set(list[i], list[i]);

							  icons.play();
						</script>
			<h6>-10°</h6>
			<div class="bottom-head">
					<p>August 16</p>
					<p>Thursday</p>
				</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<!--//content-->
		<div class="blank">
			<div class="blank-page">
				
	        	
	        </div>
	       </div>
           
			<input type='hidden' id='staffname' value='<?php echo $fullname;?>'/> 
		<!--//content-->
<?php
include"footer.php";
?>        