<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {
  header ("Location: login.php");
  
    


}
else{
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  $privilege=$_SESSION['privilege'];
  last_seen($userid,$db_handle);
   /*Get organization settings*/
  $ColNames=array('vote','fullname','hash','image','dateof','type');
  $StaffInfo=mysql_select("Select staff.staffid,members.fullname,members.hash,members.type,members.image,lastseen.dateof,upvotes.vote from staff inner join members on members.hash=staff.staffid inner join lastseen on lastseen.hash=staff.staffid left join upvotes on upvotes.staffid=staff.staffid where staff.orgid='$orgid' ",$db_handle,null,$ColNames);

  $StaffInfoDataRows=explode("]-[",$StaffInfo);
  $StaffCount=count($StaffInfoDataRows);
  

}
?>
<?php

include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 <!--banner-->	
		    <div class="banner">
		   
				<h2>
				<a href="index.php">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Staff</span>
                <?php if($privilege=="provider"){echo "<a href='#!' class='btn btn-sm btn-default pull-right addnewstaff' id='0'>Add new staff</a>";}?>
				</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
	

			<div class="blank-page">
            <div class='well' id='addStaff' style='display:none;'>
            <div id='results'></div>
             <form role="form" class="form-horizontal">
                <div class="form-group">
                            <label class="col-md-2 control-label">Search User</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" class="form-control1" id="memberInput" placeholder="Enter Full Name , Email or Phone Number">
                                </div>
                            </div>
                </div>
            </form>
            <table class="table table-striped hidden" id='tabledresults' >
                        <thead>
                            <tr>
                            
                                <th>#</th>
                                <th>Fullname</th>
                                <th>Phone</th>
                                <th>Email</th>
                               
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody id='membersResults'></tbody>
            </table>
                
              

            </div>
				 <div id='results2'></div>
	        	<div class="container" style='width:100%'>
    <div class="row">
    <?php for($i=0;$i<$StaffCount;$i++){
              $DataVal=explode("}-{",$StaffInfoDataRows[$i]);
              $ColNames=array('vote','fullname','hash','image','dateof');
              $vote=$DataVal[0];
              $fullname=$DataVal[1];
              $hash=$DataVal[2];
              $id=$hash;
              $image=$DataVal[3];
              $dateof=$DataVal[4];
              $type=$DataVal[5];
              $availability='Online';
              $cssAva='label-success';
              $status;
              if($vote==""){
                $vote=0;
              }
              if($type=="provider"){
                $status="Admin";
              }
              else{
                $availability='Offline';
                $cssAva='label-danger';
                $status="Staff";
              }
              $gotSomeMsgs=mysql_select("select * from messages where (senderid='$hash' and receiverid='$userid') or (senderid='$userid' and receiverid='$hash')",$db_handle,null,'msghash');
              if($gotSomeMsgs==0){$gotSomeMsgs="new";}
              if($privilege=="provider" && $type=="staff"){
                $delBtn="<a href='#' class='btn btn-xs btn-danger deletestaff' data-name='$fullname' id='$hash' ><span class='glyphicon glyphicon-remove'></span> Delete Staff</a>";
              }
              else{
                $delBtn='';
              }
              print"
              <div class='col-md-4' id='staff$id'>
              <div class='well well-sm'>
                <div class='media'>
                    <a class='thumbnail pull-left' href='#'>
                        <img class='media-object' src='images/$image'>
                    </a>
                    <div class='media-body'>
                        <h4 class='media-heading'>$fullname</h4>
                        <p><span class='label $cssAva'>$availability</span> <span class='label label-primary'>$vote Upvotes</span> <span class='label label-default'>$status</span></p>
                        <p>
                            <a href='ebox.php?GXy82398yBJttywey5vv540jFEe6hbnhFrmmnBDFgHyYTTtfghkpqcipijhawQwWWfjhg=C656r6ftjy4f5347GcDFGAYY563287HSFT73X3R734X89270RX476J3256X3792E3X73X7T23T3263R2867XE9873XY97XT973XT3XTE297X317323T&yak=$gotSomeMsgs&userid=$hash&fullname=$fullname&image=$image&P6238V564fuytr565W67BD7D3NYD7BW7I3' class='btn btn-xs btn-default composeMsg' id='$hash' ><span class='glyphicon glyphicon-comment'></span> Message</a>$delBtn
                            
                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
        ";
              
              }
            
            
            ?>
		
      
	</div>
</div>
	        </div>
	       </div>
           
			
		<!--//content-->
<?php
include"footer.php";
?>        