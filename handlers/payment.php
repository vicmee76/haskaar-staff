<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {   


}
else{
  	include"../classes/class.payment.php";
  	include"../config.php";
  	include"../modules/input_module.php";
  	include"../modules/sql_module.php";
  	$userid=$_SESSION['userid'];
  	$fullname=$_SESSION['fullname'];
  	$orgid=$_SESSION['loggedin'];
  	last_seen($userid,$db_handle); 
  	$payment = new payment($orgid);
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		$cmd = $_POST['cmd'];
		$error_message = "";
		
		$id = just_validate_input($id,$db_handle,'text',3);
		if($id=="error"){
			$error_message .= "Invalid payment Token </br>";
		}
		if($cmd == "edit" || $cmd == "delete" || $cmd == "transaction" || $cmd == "put" || $cmd == "history")
		{

		}
		else
		{
			$error_message .= "Unknown command <br/>";
		}
		
		if($error_message == ""){
			
			
			if($cmd == "edit")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$min = $_REQUEST['min'];
				$max = $_REQUEST['max'];
				$type = (int)$_REQUEST['type'];

				if($title !== null && $title !== "null")
				{
					$title = just_validate_input($title,$db_handle,'text',4);
				}
				else
				{
					$title = null;
				}
				if($description !== null && $description !== "null")
				{
					$description = just_validate_input($description,$db_handle,'text',5);
				}
				else
				{
					$description = null;
				}
				if($min!== null && $min !== "null")
				{
					$min = just_validate_input($min,$db_handle,'num',1);
				}
				else
				{
					$min = null;
				}
				if($max!== null && $max !== "null")
				{
					$max = just_validate_input($max,$db_handle,'num',1);
				}
				else
				{
					$max = null;
				}
				if($type!== null && $type !== "null")
				{
					if($type !== 1 && $type !== 2)
					{
						$type = "error";
					}
					else
					{
						$type = just_validate_input($type,$db_handle,'num',1);
					}
					
				}
				else
				{
					$type = null;
				}
				if($title !== "error" && $description !== "error" && $min !== "error" && $max !== "error" && $type !== "error")
				{								
					$edit_payment = $payment->update_payment($title,$description,$min,$max,$type,$id);
					if($edit_payment == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
				}
				else
				{
					echo "Error Occured!";
				}
			}
			else if($cmd == "delete")
			{
				$delete_payment = $payment->delete_payment($id);
				if($delete_payment == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "put")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$min = $_REQUEST['min'];
				$max = $_REQUEST['max'];
				$type = $_REQUEST['type'];
				
				$title = just_validate_input($title,$db_handle,'text',4);			
				$description = just_validate_input($description,$db_handle,'text',5);			
				$min = just_validate_input($min,$db_handle,'num',1);
				$max = just_validate_input($max,$db_handle,'num',1);
				$type = just_validate_input($type,$db_handle,'num',1);
				$id = just_validate_input(generate_guid($title,$min),$db_handle,'text',8);
				if($type !== 1 || $type !== 2)
				{
					$type == "error";
				}

				if($title !== "error" && $description !== "error" && $min !== "error" && $max !== "error" || $type !== "error" )
				{
					
					$add_payment = $payment->add_payment($title,$description,$min,$max,$type,$id);
					if($add_payment == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
					
					
				}
				else
				{
					echo "Please check your parameters and try again!";
				}
			}
			else if($cmd == "transaction")
			{
				$get_transaction = $payment->list_transactions($id);
				if($get_transaction['success'] == 1)
				{
					$trans_data = $get_transaction['data'];
					for($i=0;$i<count($trans_data);$i++)
					{
						$name = $trans_data[$i]['fullname'];
						$user_id = $trans_data[$i]['hash'];
						$token = $trans_data[$i]['token'];
						$amount = $trans_data[$i]['amount'];
						$datetime = date("M d Y",$trans_data[$i]['datepaid']);
						$title = $trans_data[$i]['title'];
						$image = $trans_data[$i]['image'];						
						$status = $trans_data[$i]['status'];
						$style = '';
						if($status == 0)
						{
							$status = "paid";
							$style = "bg_yellow";
						}
						else if($status == 1)
						{
							$status = "verified";
							$style = "bg_green";
						}
						else if($status == 2)
						{
							$status = "withdrawn";
							$style = "bg_grey";
						}			
						else if($status == 3)
						{
							$status = "canceled";
							$style = "bg_red";
						}
						print

						"
							<div class='transaction'>
								<div class='img_cont'>
									<img src='files/images/$image' />
								</div>
								<div class='title'>$name</div>
								<div class='amount'>NGN $amount</div>
								<div class='status $style'>$status</div>
								<div class='date'>$datetime</div>
							</div>

						";
					}
				}
				else
				{
					echo $get_transaction;
				}
			}
			else if($cmd == "history")
			{
				$history = $payment->transaction_history();
				if($history['success'] == 1)
				{
					$trans_data = $history['data'];
					for($i=0;$i<count($trans_data);$i++)
					{
						$name = $trans_data[$i]['fullname'];
						$user_id = $trans_data[$i]['hash'];
						$token = $trans_data[$i]['token'];						
						setlocale(LC_MONETARY, "en_NG");
						$amount = money_format("%i",$trans_data[$i]['amount']);
						$datetime = date("M d Y",$trans_data[$i]['datepaid']);
						$title = $trans_data[$i]['title'];
						$image = $trans_data[$i]['image'];	
						$status = $trans_data[$i]['status'];
						$style = '';
						if($status == 0)
						{
							$status = "paid";
							$style = "bg_yellow";
						}
						else if($status == 1)
						{
							$status = "verified";
							$style = "bg_green";
						}
						else if($status == 2)
						{
							$status = "withdrawn";
							$style = "bg_grey";
						}			
						else if($status == 3)
						{
							$status = "canceled";
							$style = "bg_red";
						}		
						print

						"
							<div class='transaction'>
								<div class='img_cont'>
									<img src='files/images/$image' />
								</div>
								<div class='title'>$name <i class='fa fa-angle-double-right'></i> $title</div>
								<div class='amount'>NGN $amount</div>
								<div class='status $style'>$status</div>
								<div class='date'>$datetime</div>
							</div>


						";
					}
				}
				else
				{
					echo 0;
				}
			}

		}
		else
		{
			echo $error_message;
		}



	}
	else
	{
		echo 0;
	}
	
	

	
	
}

function money_format($a,$b)
                    {
                        return $b;
                    }

?>