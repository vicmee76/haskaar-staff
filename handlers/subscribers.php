<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {    


}
else{
	include"../classes/class.subscribers.php";
	include"../config.php";
	include"../modules/input_module.php";
	include"../modules/sql_module.php";
	$userid=$_SESSION['userid'];
	$fullname=$_SESSION['fullname'];
	$orgid=$_SESSION['loggedin'];
	$privilege=$_SESSION['privilege'];
	last_seen($userid,$db_handle);
	$subscribers = new subscribers($orgid,$orgid,'web');
	if(isset($_POST['count'])){

	 	$counter = $_POST['count'];
	 	$counter = just_validate_input($counter,$db_handle,'num',1);
		if($counter !== "error" || $counter <=20)
		{
			$the_subscribers_list = $subscribers->list_subscribers($counter);
		    if($the_subscribers_list['success'] == 1)
		    {
		        $the_subscribers_data = $the_subscribers_list['data'];
		        for($i = 0; $i < count($the_subscribers_data); $i++)
		        {
		          $subscriber_img = $the_subscribers_data[$i]['image'];
		          $subscriber_name = $the_subscribers_data[$i]['fullname'];
		          $subscriber_id = $the_subscribers_data[$i]['hash'];

		          print
		          "
		            <div class='subscriber'>
		              <div class='photo'>
		                <img src='images/$subscriber_img'/>
		              </div>
		              <div class='actions'>
		                <span class='name open_profile_popup' id='$subscriber_id'> $subscriber_name</span>
		              </div>
		            </div>

		          ";
		        }

		        if($the_subscribers_list['more'] == 1)
		        {
		          $counter = $counter + 20;	
		          echo 
		          "
		            <a href='#!' id='$counter' data-type='subscribers.php' class='btn btn-info btn-block loadmore'>Load More Subscribers</a>

		          ";
		        }
		    }
		    else
		    {
		        $error = $subscribers->error_finder;
		        print
		        "
		          <div class='alert alert-warning'> No more subscribers</div>

		        ";

		    }
		}
	}
	else
	{
		print
		        "
		          <div class='alert alert-warning'>Could not process your request at the time!</div>

		        ";
	}
}


?>