<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {   


}
else{
  	include"../classes/class.reaction.php";
  	include"../config.php";
  	include"../modules/input_module.php";
  	include"../modules/sql_module.php";
  	$userid=$_SESSION['userid'];
  	$fullname=$_SESSION['fullname'];
  	$orgid=$_SESSION['loggedin'];
  	last_seen($userid,$db_handle); 
  	
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		$cmd = $_POST['cmd'];
		$error_message = "";
		
		$id = just_validate_input($id,$db_handle,'text',3);
		if($id=="error"){
			$error_message .= "Invalid Reactant</br>";
		}
		if($cmd == "like" || $cmd == "unlike" || $cmd == "comment" || $cmd == "view" || $cmd == "likers" || $cmd == "commenters" || $cmd == "viewers" || $cmd == "delete_comment" || $cmd == "edit_comment")
		{

		}
		else
		{
			$error_message .= "Unknown command <br/>";
		}
		
		if($error_message == ""){
			
			
			$reactions = new reactions($userid,remove_quotes($id));
			if($cmd == "edit_comment")
			{
				$comment = $_REQUEST['comment'];

				$comment = just_validate_input($comment,$db_handle,'text',4);
				
				if($comment !== "error")
				{								
					$edit_comment = $reactions->edit_comment($comment);
					if($edit_comment == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
				}
				else
				{
					echo "Error Occured!";
				}
			}
			else if($cmd == "delete_comment")
			{
				$delete_comment = $reactions->delete_comment();
				if($delete_comment == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "like")
			{
				$like = $reactions->like_stuff();
				if($like == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "view")
			{
				$view = $reactions->view_stuff();
				if($view == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "unlike")
			{
				$unlike = $reactions->unlike_stuff();
				if($unlike == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "comment")
			{
				$comment = $_REQUEST['comment'];
				
				
				$comment = just_validate_input($comment,$db_handle,'text',2);			
				
				
				if($comment !== "error")
				{
					
					$add_comment = $reactions->comment_stuff($comment);
					if($add_comment == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
					
					
				}
				else
				{
					echo "Please check your parameters and try again!";
				}
			}
			else if($cmd == "commenters")
			{
				$get_comments = $reactions->view_commenters();
				if($get_comments['success'] == 1)
				{
					$data= $get_comments['data'];
					for($i=0;$i<count($data);$i++)
					{
						$name = $data[$i]['fullname'];
						$user_id = $data[$i]['hash'];
						$comment = $data[$i]['comment'];
						$editted = $data[$i]['editted'];
						$datetime = date("M d Y",$data[$i]['dateofcomment']);
						$image = $data[$i]['image'];	
						
						print

						"
							<div class='comment_pop'>
								<div class='img_cont'>
									<img src='files/images/$image' />
								</div>
								<div class='info'><span class='name'>$name</span><span class='comment'>$comment</span>
								
									<div class='extra_info'><span class='date'>$datetime</span><span class='editted'>$editted</span></div>
								</div>
							</div>

						";
					}
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "likers")
			{
				$likers = $reactions->view_likers();
				if($likers['success'] == 1)
				{
					$data = $likers['data'];
					for($i=0;$i<count($data);$i++)
					{
						$name = $data[$i]['fullname'];
						$user_id = $data[$i]['hash'];						
						$image = $data[$i]['image'];	
						
						print

						"
							<div class='reaction'>
								<div class='img_cont'>
									<img src='files/images/$image' />
								</div>
								<div class='title'>$name </div>
								
							</div>


						";
					}
				}
				else
				{
					echo 0;
				}
			}

			else if($cmd == "viewers")
			{
				$viewers = $reactions->view_viewers();
				if($viewers['success'] == 1)
				{
					$data = $viewers['data'];
					for($i=0;$i<count($data);$i++)
					{
						$name = $data[$i]['fullname'];
						$user_id = $data[$i]['hash'];						
						$image = $data[$i]['image'];	
						
						print

						"
							<div class='reaction'>
								<div class='img_cont'>
									<img src='files/images/$image' />
								</div>
								<div class='title'>$name </div>
								
							</div>


						";
					}
				}
				else
				{
					echo 0;
				}
			}

		}
		else
		{
			echo $error_message;
		}



	}
	else
	{
		echo 0;
	}
	
	

	
	
}



?>