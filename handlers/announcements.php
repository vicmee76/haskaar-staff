<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {   


}
else{
  	include"../classes/class.announcement.php";
  	include"../config.php";
  	include"../modules/input_module.php";
  	include"../modules/sql_module.php";
  	$userid=$_SESSION['userid'];
  	$fullname=$_SESSION['fullname'];
  	$orgid=$_SESSION['loggedin'];
  	last_seen($userid,$db_handle); 
  	$announcement = new announcement($orgid);
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		$cmd = $_POST['cmd'];
		$error_message = "";
		
		$id = just_validate_input($id,$db_handle,'text',3);
		if($id=="error"){
			$error_message .= "Invalid announcement Token </br>";
		}
		if($cmd == "edit" || $cmd == "delete" || $cmd == "reset" || $cmd == "put")
		{

		}
		else
		{
			$error_message .= "Unknown command <br/>";
		}
		
		if($error_message == ""){
			
			
			if($cmd == "edit")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$duration = $_REQUEST['duration'];

				if($title !== null && $title !== "null")
				{
					$title = just_validate_input($title,$db_handle,'text',4);
				}
				else
				{
					$title = null;
				}
				if($description !== null && $description !== "null")
				{
					$description = just_validate_input($description,$db_handle,'text',5);
				}
				else
				{
					$description = null;
				}
				if($duration!== null && $duration !== "null")
				{
					$duration = just_validate_input($duration,$db_handle,'num',4);
				}
				else
				{
					$duration = null;
				}
				
				if($title !== "error" && $description !== "error" && $duration !== "error" )
				{								
					$edit_announcement = $announcement->edit_announcement($title,$description,$duration,$id);
					if($edit_announcement == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "delete")
			{
				$delete_announcement = $announcement->delete_announcement($id);
				if($delete_announcement == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "put")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$duration = $_REQUEST['duration'];
				
				$title = just_validate_input($title,$db_handle,'text',4);			
				$description = just_validate_input($description,$db_handle,'text',5);			
				$duration = just_validate_input($duration,$db_handle,'num',1);
				$id = just_validate_input(generate_guid($title,$duration),$db_handle,'text',8);;

				if($title !== "error" && $description !== "error" && $duration !== "error" )
				{
					
					$add_announcement = $announcement->add_announcement($title,$description,$duration,$id);
					if($add_announcement == 1)
					{
						echo 1;
					}
					else
					{
						echo "Something went wrong! try again later or refresh page!";
					}
					
					
				}
				else
				{
					echo "Please check your parameters and try again!";
				}
			}
			else if($cmd == "reset")
			{
				$reset_announcement = $announcement->reset_announcement($id);
				if($reset_announcement == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}

		}
		else
		{
			echo $error_message;
		}



	}
	else
	{
		echo 0;
	}
	
	

	
	
}
?>