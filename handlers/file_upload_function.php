<?php
function upload_file($type,$file)
{
    include('../classes/class.uploader.php');
    $ext = array();
    $folder = 'files';
    $error_message = '';
    $response = array();
    if($type == "photo")
    {
        array_push($ext, "jpg","jpeg","png","bmp");
        $folder = '../files/images/';
    }
    else if($type == "video")
    {
        array_push($ext,"mp4","avi","mpeg","mov","3gp");
        $folder = '../files/videos/';
    }
    else if ($type == "audio")
    {
        array_push($ext,"ogg","mp3","wav");
        $folder = '../files/audios/';
    }
    else if ($type == "document")
    {
        array_push($ext,"doc","docx","pdf");
        $folder = '../files/docs/';
    }
    else
    {
        $error_message .= "Unknown File <br/>";
    }

    if($error_message == "")
    {

        $uploader = new Uploader();
        $file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $file_name = sha1(rand(1232,923828) * time());
        $data = $uploader->upload($file, array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 90, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => $ext, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => $folder, //Upload directory {String}
            'title' => $file_name, //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete']){
            $info = $data['data'];
            
            
        
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            $response['success'] = 0;
            $response['message'] = $errors;
            return $response;
        }
        else
        {
            $complete_file_name = $file_name.".".$file_ext;
            $response['success'] = 1;
            $response['message'] = $complete_file_name;
            return $response;

        }
    }
    else
    {
        $response['success'] = 0;
        $response['message'] = $error_message;
        return $response;
    }
    
}
        

?>
