<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {   


}
else{
  	include"../classes/class.event.php";
  	include"../config.php";
  	include"../modules/input_module.php";
  	include"../modules/sql_module.php";
  	include"file_upload_function.php";
  	$userid=$_SESSION['userid'];
  	$fullname=$_SESSION['fullname'];
  	$orgid=$_SESSION['loggedin'];
  	last_seen($userid,$db_handle); 
  	$event = new events($orgid);
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		$cmd = $_POST['cmd'];
		$error_message = "";
		
		$id = just_validate_input($id,$db_handle,'text',8);
		if($id=="error"){
			$error_message .= "Invalid Event Token </br>";
		}
		if($cmd == "edit" || $cmd == "delete" || $cmd == "get" || $cmd == "put")
		{

		}
		else
		{
			$error_message .= "Unknown command <br/>";
		}
		
		if($error_message == ""){
			
			if($cmd == "get")
			{
				$event_details = $event->event_details($id);
				if($event_details['success'] == 1)
				{
					$event_data = $event_details['data'];
					$title = $event_data[0]['title'];
    				$timestamp = $event_data[0]['timestamp'];
    				$event_id = $event_data[0]['programid'];
    				$image = $event_data[0]['image'];
    				$description = $event_data[0]['description'];
    				$address = $event_data[0]['address'];
    				$contact = $event_data[0]['contact'];
    				$date_posted = date('D d M Y',$event_data[0]['datetime']);
    				$datetime = date('D d M Y',$timestamp);

    				print
    				"
    					<div class='event_details'>
			        		<div class='title' id='e_t'> $title
			        		</div>
			        		<div class='img_cont'>
			        			<img src='files/images/$image'/>
		        			</div>
		        			<div class='info'>
		        			<span class='variable'>Description <i class='fa fa-angle-double-right' aria-hidden='true' ></i><span class='value' id='e_d'> $description </span></span>
		        			<span class='variable'>Date and Time <i class='fa fa-angle-double-right' aria-hidden='true' ></i><span class='value' id='e_d_t'> $datetime</span></span>
		        			<span class='variable'>Venue <i class='fa fa-angle-double-right' aria-hidden='true' ></i><span class='value' id='e_v'> $address </span></span>
		        			<span class='variable'>Contact <i class='fa fa-angle-double-right' aria-hidden='true' ></i><span class='value' id='e_c'> $contact </span></span>
		        			<span class='variable'>Date Posted <i class='fa fa-angle-double-right' aria-hidden='true' ></i><span class='value'> $date_posted </span></span>

		        			<div class='askaar_btns'>
		        				<a href='#!' class='btn edit_event_request' id='$event_id'><i class='fa fa-edit'></i> Edit</a><a href='#!' class='btn delete_event' id='$event_id'><i class='fa fa-trash'></i> Delete</a></div>
				    		</div>
		        			</div>
			        	</div>

    				";
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "edit")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$date_time = $_REQUEST['date'];
				$address = $_REQUEST['address'];
				$contact = $_REQUEST['contact'];
				$type = $_REQUEST['type'];

				if($title !== null && $title !== "null")
				{
					$title = just_validate_input($title,$db_handle,'text',4);
				}
				else
				{
					$title = null;
				}
				if($description !== null && $description !== "null")
				{
					$description = just_validate_input($description,$db_handle,'text',5);
				}
				else
				{
					$description = null;
				}
				if($date_time!== null && $date_time !== "null")
				{
					$date_time = just_validate_input(strtotime($date_time),$db_handle,'text',4);
				}
				else
				{
					$date_time = null;
				}
				if($address !== null && $address !== "null")
				{
					$address = just_validate_input($address,$db_handle,'text',4);
				}
				else
				{
					$address = null;
				}
				if($contact !== null && $contact !== "null")
				{
					$contact = just_validate_input($contact,$db_handle,'text',4);
				}
				else
				{
					$contact = null;
				}
				if($type !== null && $type !== "null")
				{
					$file = $_FILES['file'];
					if($type !== "photo")
					{
						$type == "error";
					}
				}
				else
				{
					$file = null;
				}
				if($title !== "error" && $description !== "error" && $contact !== "error" && $date_time !== "error" && $address !== "error" && $type !== "error")
				{
					if($type !== null && $type !== "null")
					{
						$upload_file = upload_file('photo',$file);
						if($upload_file['success'] == 1)
						{
							$image = "'".$upload_file['message']."'";
						}
						else
						{
							$error_message = $upload_file['message'];
							$image = "error";
						}
					}
					else
					{
						$image = null;
					}
					if($image !== "error")
					{
						$edit_event = $event->edit_event($title,$description,$date_time,$address,$contact,$id,$image);
						if($edit_event == 1)
						{
							echo 1;
						}
						else
						{
							echo "Something went wrong! try again later or refresh page!";
						}
					}
					else
					{
						echo $error_message;
					}
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "delete")
			{
				$delete_event = $event->delete_event($id);
				if($delete_event == 1)
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			else if($cmd == "put")
			{
				$title = $_REQUEST['title'];
				$description = $_REQUEST['description'];
				$date_time = $_REQUEST['date'];
				$address = $_REQUEST['address'];
				$contact = $_REQUEST['contact'];
				$type = $_REQUEST['type'];
				$title = just_validate_input($title,$db_handle,'text',4);			
				$description = just_validate_input($description,$db_handle,'text',5);			
				$date_time = just_validate_input(strtotime($date_time),$db_handle,'text',4);			
				$address = just_validate_input($address,$db_handle,'text',4);			
				$contact = just_validate_input($contact,$db_handle,'text',4);
				$id = just_validate_input(generate_guid($title,$type,$date_time),$db_handle,'text',8);

				if($type !== null && $type !== "null")
				{
					$file = $_FILES['file'];
					if($type !== "photo")
					{
						$type == "error";
					}
				}
				
				if($title !== "error" && $description !== "error" && $contact !== "error" && $date_time !== "error" && $address !== "error" && $type !== "error")
				{
					if($type !== null)
					{
						$upload_file = upload_file('photo',$file);
						if($upload_file['success'] == 1)
						{
							$image = "'".$upload_file['message']."'";
						}
						else
						{
							$error_message = $upload_file['message'];
							$image = "error";
						}
					}
					else
					{
						$image = "error";
						$error_message = "Upload an Image <br/>";
					}
					if($image !== "error")
					{
						$add_event = $event->add_event($title,$description,$date_time,$address,$contact,$id,$image);
						if($add_event == 1)
						{
							echo 1;
						}
						else
						{
							echo "Something went wrong! try again later or refresh page!";
						}
					}
					else
					{
						echo $error_message;
					}
					
				}
				else
				{
					echo "Please check your parameters and try again!";
				}
			}

		}
		else
		{
			echo 0;
		}



	}
	else
	{
		echo 0;
	}
	
	

	
	
}
?>