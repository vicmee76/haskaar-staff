<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {   


}
else{
    include"../config.php";
    include"../modules/input_module.php";
    include"../modules/sql_module.php";
    $userid=$_SESSION['userid'];
    $fullname=$_SESSION['fullname'];
    $orgid=$_SESSION['loggedin'];
    $privilege=$_SESSION['privilege'];
    last_seen($userid,$db_handle);
    if(isset($_POST['title'])){
        include('../classes/class.uploader.php');
        include('../classes/class.media.php');
        $title = $_POST['title'];
        $description = $_POST['description'];
        $type = $_POST['type'];
        $title = just_validate_input($title,$db_handle,'text',5);
        $description = just_validate_input($description,$db_handle,'text',null);
        $error_message = "";
        $ext = array();
        $folder = 'files';
        if($title == "error")
        {
            $error_message .= "$type Title Is Too Short <br/>";
        }
        if($description == "error")
        {
            $error_message .= "$type Description is Invalid <br/>";
        }
        if($type == "photo")
        {
            array_push($ext, "jpg","jpeg","png","bmp");
            $folder = 'images';
        }
        else if($type == "video")
        {
            array_push($ext,"mp4","avi","mpeg","mov","3gp");
            $folder = 'videos';
        }
        else if ($type == "audio")
        {
            array_push($ext,"ogg","mp3","wav");
            $folder = 'audios';
        }
        else if ($type == "document")
        {
            array_push($ext,"doc","docx","pdf");
            $folder = 'docs';
        }
        else
        {
            $error_message .= "Unknown File <br/>";
        }

        if($error_message == "")
        {

            $uploader = new Uploader();
            $media = new mediaset($userid,$orgid,'provider',$type);
            $file_ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $file_name = generate_guid($orgid.$type);
            $file_guid = generate_guid($file_name);
            $data = $uploader->upload($_FILES['file'], array(
                'limit' => 10, //Maximum Limit of files. {null, Number}
                'maxSize' => 90, //Maximum Size of files {null, Number(in MB's)}
                'extensions' => $ext, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => '../files/$folder/', //Upload directory {String}
                'title' => $file_name, //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => true, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));

            if($data['isComplete']){
                $info = $data['data'];
                
                
            
            }

            if($data['hasErrors']){
                $errors = $data['errors'];
                print($errors[0][0]);
            }
            else
            {
                $complete_file_name = $file_name.".".$file_ext;
                $insert_media = $media->add_media($title,$description,"'$complete_file_name'","'$file_guid'");
                if($insert_media == 1)
                {
                    echo 1;
                }
                else
                {
                    echo "We could not complete this request at this time, Please try again!";
                }

            }
        }
    }
}
?>
