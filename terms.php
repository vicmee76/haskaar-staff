<?php 



?>
<!DOCTYPE html>
<html>
<head>
<title>Askaar</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Askaar" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/login.css" rel="stylesheet" type="text/css" media="all" />
<!-- //Custom Theme files -->
<!-- web font -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
<!-- //web font -->
<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
<style type='text/css'>
.login-forme {
    width: 80%;
    margin: 2.5em auto;
    background-color: #ffffff !important;
    padding: 15px;
    background-size: cover;
    font-weight:bold;
    font-size:14px;
    font-family:sans-serif;
    word-spacing: 0.2em;
    word-wrap:break-all;
    text-align:justify;
}
</style>
<!-- //js -->
</head>
<body>
	<!-- main -->
	<div class="main">
		<h1> Askaar Terms And Conditions</h1>
		<div class="login-forme"><pre>
<strong>Terms of Service</strong><br/><br/>
1. Your Acceptance
<br/>
This is an agreement between ELOKS WARE LIMITED the owner and operator of
www.askaarnet.com the “ASKAAR Site”, the ASKAAR Messenger ( including all content
provided by ELOKS WARE LIMITED through ASKAAR Messenger and the ASKAAR site,
the "ASKAAR Service", or the "Service"), and you (“you” or “You”), a client ( USER or
SERVICE PROVIDERS) of the Service. BY USING THE SERVICE, YOU ACKNOWLEDGE
AND AGREE TO THESE TERMS OF SERVICE, AND ASKAAR'S PRIVACY POLICY. If
you choose to not agree with any of these terms, you may not use the Service.<br/><br/>
2. ASKAAR Service
<br/>
These Terms of Service apply to clients of the ASKAAR Service. Information provided by our
users through the ASKAAR Service may contain links to third party websites that are not owned
or controlled by ELOKS WARE LIMITED. ELOKS WARE LIMITED has no control over, and
assumes no responsibility for, the content, privacy policies, or practices of any third party
websites. In addition, ELOKS WARE LIMITED will not and cannot censor or edit the content of
any third-party site. By using the Service, you expressly acknowledge and agree that ELOKS
WARE LIMITED shall not be responsible for any damages, claims or other liability arising from
or related to your use of any third-party website.<br/><br/>
3. ASKAAR Access
<br/>
A. Subject to your compliance with these Terms of Service, E.W.L hereby grants you permission
to use the Service, provided that: (i) your use of the Service as permitted is solely for your
personal use, and you are not permitted to resell or charge others for use of or access to the
Service, or in any other manner inconsistent with these Terms of Service; (ii) you will not
duplicate, transfer, give access to, copy or distribute any part of the Service in any medium
without ELOKS WARE LIMITED 's prior written authorization; (iii) you will not attempt to
reverse engineer, alter or modify any part of the Service; and (iv) you will otherwise comply
with the terms and conditions of these Terms of Service and Privacy Policy.
B. In order to access and use the features of the Service, you acknowledge and agree that you
will have to provide ASKAAR with your contact details. You expressly acknowledge and agree
that in order to provide the Service, you must provide accurate and complete information. You
are solely responsible for the status submission and room post that you submit and that are
displayed on the ASKAAR Service. ASKAAR will not be liable for your losses caused by any
unauthorized use of your account, you may be liable for the losses of ASKAAR or others due to
such unauthorized use.
<br/>
C. You agree not to use or launch any automated system, including without limitation, "robots,"
"spiders," "offline readers," etc. or "load testers" such as wget, apache bench, mswebstress,
httpload, blitz, Xcode Automator, Android Monkey, etc., that accesses the Service in a manner
that sends more request messages to the ASKAAR servers in a given period of time than a 
human can reasonably produce in the same period by using a ASKAAR application, and you are
forbidden from ripping the content unless specifically allowed, we do disallow any efforts to
reverse-engineer our system, our protocols, or explore outside the boundaries of the normal
requests made by ASKAAR clients. We have to disallow using request modification tools such
as fiddler or whisker, or the like or any other such tools activities that are meant to explore or
harm, penetrate or test the site. You must secure our permission before you measure, test, health
check or otherwise monitor any network equipment, servers or assets hosted on our domain. You
agree not to collect or harvest any personally identifiable information, including names or phone
number, from the Service, nor to use the communication systems provided by the Service for any
commercial solicitation or spam purposes. You agree not to spam, or solicit for commercial
purposes, any users of the Service.<br/><br/>
4. Intellectual Property Rights
<br/>
The design of the ASKAAR Service along with ASKAAR created text, scripts, graphics,
interactive features and the like, and the trademarks, service marks and logos contained therein
("Marks"), are owned by or licensed to ELOKS WARE LIMITED, subject to copyright and
other intellectual property rights under Nigeria and foreign laws and international conventions.
The Service is provided to you AS IS for your information and personal use only. ELOKS
WARE LIMITED reserves all rights not expressly granted in and to the Service. You agree to
not engage in the use, copying, or distribution of any of the Service other than expressly
permitted herein, including any use, copying, or distribution of Status Submissions of third
parties obtained through the Service for any commercial purposes.<br/><br/>
5. User Status Submissions
<br/>
A. the ASKAAR Service allows ASKAAR users to chat with service providers staff, chat with
other users, to view status text, profile photos, room post and other communications submitted
by you, as well as your names. These Submissions may be hosted, shared, and/or published as
part of the ASKAAR Service, and may be visible to other clients of the Service. For clarity,
Askaar is End to End Encrypted, chats/messages, that you send to
any ASKAAR staff/Service Provider will only be viewable by the
ASKAAR staff/Service Provider that you sent such information; but
Status, profile pictures, room post , location data and photos or files may be globally viewed by
ASKAAR clients that are on the ASKAAR network , unless you don’t provide such information,
you acknowledge and agree that any Status, profile pictures, location data and photos or files
submitted may be globally viewed by both staff and users that are on ASKAAR network, so
don’t submit or post status messages or profile photos that you don’t want to be seen globally. A
good rule of thumb is if you don’t want the whole world to know something or see something,
don’t submit it to the Service. ASKAAR does not guarantee any confidentiality with respect to
any submissions.
<br/>
B. You shall be solely responsible for your own Status submission and Room posts and the
consequences of posting or publishing them. Because ASKAAR is only acting as a repository of
data, user submitted statuses do not necessarily represent the views or opinions of ASKAAR, and 
ASKAAR makes no guarantees as to the validity, accuracy or legal status of any status, you
affirm, represent, and/or warrant that: (i) you own or have the necessary licenses, rights,
consents, and permissions to use and authorize ASKAAR to use all patent, trademark, trade
secret, copyright or other proprietary rights in and to any and all Status Submissions and Room
post to enable inclusion and use of the Status Submissions and Room post in the manner
contemplated by the Service and these Terms of Service; and (ii) you have the written consent,
release, and/or permission of each and every identifiable individual person in the Status
Submission and Room post to use the name or likeness of each and every such identifiable
individual person to enable inclusion and use of the Status Submissions and Room post in the
manner contemplated by the Service and these Terms of Service. To be clear: you retain all of
your ownership rights in your Status Submissions and Room post, but you have to have the
rights in the first place. However, by submitting the Status Submissions and Room post to
ASKAAR, you hereby grant ASKAAR a worldwide, non-exclusive, royalty-free, sublicense able
and transferable license to use, reproduce, distribute, prepare derivative works of, display, and
perform the Status Submissions in connection with the ASKAAR Service and ASKAAR's (and
its successor's) business, including without limitation for promoting and redistributing part or all
of the ASKAAR Service (and derivative works thereof) in any media formats and through any
media channels. You also hereby grant each subscriber (USER or STAFF) to your status and
room post on the ASKAAR Service a non-exclusive license to access your Status Submissions
through the Service.
<br/>
C. In connection with Status Submissions, you further agree that you will not: (i) submit material
that is copyrighted, protected by trade secret or otherwise subject to third party proprietary rights,
including privacy and publicity rights, unless you are the owner of such rights or have
permission from their rightful owner to post the material and to grant ASKAAR all of the license
rights granted herein; (ii) publish falsehoods or misrepresentations that could damage ASKAAR
or any third party; (iii) submit material that is unlawful, obscene, defamatory, libelous,
threatening, harassing, hateful, racially or ethnically offensive, or encourages conduct that would
be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise
inappropriate; (iv) post advertisements or solicitations of business; (v) impersonate another
person; (vi) send or store material containing software viruses, worms, Trojan horses or other
harmful computer code, files, scripts, agents or programs; (vii) interfere with or disrupt the
integrity or performance of the Service or the data contained therein; or (viii) attempt to gain
unauthorized access to the Service or its related systems or networks.
<br/>
D. Adult content must be identified as such. ASKAAR does not endorse any Status Submission,
Room post or any opinion, recommendation, or advice expressed therein, and ASKAAR
expressly disclaims any and all liability in connection with Status Submissions and Room post.
ASKAAR does not permit copyright infringing activities and infringement of intellectual
property rights via its Service, and ASKAAR will remove all Room post if properly notified that
such content infringes on another's intellectual property rights. ASKAAR reserves the right to
remove content without prior notice. ASKAAR may also terminate a user's access to the Service,
if they are determined to be a repeat infringer, or for any or no reason, including being annoying.
A repeat infringer is a User who has been notified of infringing activity more than once and/or
has had a Room post removed from the Service more than twice. ASKAAR may remove such 
Room post and/or terminate a User's access for uploading such material in violation of these
Terms of Service at any time, without prior notice and at its sole discretion.
<br/>
E. You understand that when using the ASKAAR Service you will be exposed to Status
Submissions, Service provider and Room post from a variety of sources, and that ASKAAR is
not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating
to such. You further understand and acknowledge that you may be exposed to Room post that are
inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive,
any legal or equitable rights or remedies you have or may have against ASKAAR with respect
thereto, and agree to indemnify and hold Eloks Ware Limited , its officers, directors, employees,
agents, affiliates, and/or licensors, harmless to the fullest extent allowed by law regarding all
matters related to your use of the ASKAAR Service.
<br/>
F. ASKAAR permits you to link to materials on the Service for personal purposes only. Eloks
Ware Limited reserves the right to discontinue any aspect of the ASKAAR Service at any time.
<br/>
<br/>
6. Warranty Disclaimer<br/>
YOU AGREE THAT YOUR USE OF THE ASKAAR SERVICE SHALL BE AT YOUR SOLE
RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, ELOKS WARE LIMITED, ITS
OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES,
EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICE AND YOU’RE USE
THEREOF. ELOKS WARE LIMITED MAKES NO WARRANTIES OR
REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS
SERVICE'S CONTENT AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY
(I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY
OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM
YOUR ACCESS TO AND USE OF OUR SERVICE, (III) ANY UNAUTHORIZED ACCESS
TO OR USE OF OUR SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION
AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION
OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICE, (IV) ANY BUGS,
VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR
THROUGH OUR SERVICE THROUGH THE ACTIONS OF ANY THIRD PARTY, AND/OR
(V) ANY ERRORS OR OMISSIONSIN ANY CONTENT OR FOR ANY LOSS OR DAMAGE
OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED,
EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE ASKAAR
SERVICE. ELOKS WARE LIMITED DOES NOT WARRANT, ENDORSE, GUARANTEE,
OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR
OFFERED BY A THIRD PARTY THROUGH THE ASKAAR SERVICE OR ANY
HYPERLINKED WEBSITE OR FEATURED IN ANY USER STATUS SUBMISSION OR
OTHER ADVERTISING, AND ELOKS WARE LIMITED WILL NOT BE A PARTY TO OR
IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN
YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES., YOU SHOULD
USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE. AND
AGAIN, USE THIS JUST FOR FUN. 
<br/><br/>
7. Limitation of Liability<br/>
IN NO EVENT SHALL ELOKS WARE LIMITED, ITS OFFICERS, DIRECTORS,
EMPLOYEES, OR AGENTS, BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER
RESULTING FROM ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT,
(II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER,
RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICE, (III) ANY
UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND
ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED
THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR
FROM OUR SERVERS, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE,
WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICE BY ANY THIRD
PARTY, (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR
DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT
POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE
ASKAAR CLIENT, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY
OTHER LEGAL THEORY, AND WHETHER OR NOT THE COMPANY IS ADVISED OF
THE POSSIBILITY OF SUCH DAMAGES, AND/OR (VI) THE DISCLOSURE OF
INFORMATION PURSUANT TO THESE TERMS OF SERVICE OR PRIVACY POLICY.
THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST
EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.
YOU SPECIFICALLY ACKNOWLEDGE THAT ELOKS WARE LIMITED SHALL NOT BE
LIABLE FOR USER SUBMISSIONS OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL
CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE
FROM THE FOREGOING RESTS ENTIRELY WITH YOU.
The Service is controlled and offered by Eloks Ware Limited from its facilities in Nigeria.<br/><br/>
8. Indemnity<br/>
You agree to defend, indemnify and hold harmless ELOKS WARE LIMITED, its officers,
directors, employees and agents, from and against any and all claims, damages, obligations,
losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising
from: (i) your use of and access to the ASKAAR Service; (ii) your violation of any term of these
Terms of Service; (iii) your violation of any third party right, including without limitation any
copyright, property, or privacy right; or (iv) any claim that one of your Status Submissions or
Room post caused damage to a third party. This defense and indemnification obligation will
survive these Terms of Service and your use of the ASKAAR Service<br/><br/>
9. Ability to Accept Terms of Service
You affirm that you are either more than 16 years of age, or an emancipated minor, or possess
legal parental or guardian consent, and are fully able and competent to enter into the terms,
conditions, obligations, affirmations, representations, and warranties set forth in these Terms of 
Service, and to abide by and comply with these Terms of Service. In any case, you affirm that
you are at least 16 years old as the ASKAAR Service is not intended for children under 16. If
you are under 16 years of age, you are not permitted to use the ASKAAR Service.<br/><br/>
10. General<br/>
You agree that: (i) the ASKAAR Service shall be deemed solely based in Lagos Nigeria; (ii) the
ASKAAR Service shall be deemed a passive server that does not give rise to personal
jurisdiction over ELOKS WARE LIMITED, either specific or general, in jurisdictions other than
Lagos Nigeria; and (iii) that you agree to subject to the jurisdiction of Lagos in the event of any
legal dispute. These Terms of Service shall be governed by the internal substantive laws of the
Lagos State, without respect to its conflict of laws principles. Any claim or dispute between you
and ELOKS WARE LIMITED that arises in whole or in part from the ASKAAR Service shall be
decided exclusively by a court of competent jurisdiction located in Lagos State or Imo State of
Nigeria. <br/>
These Terms of Service, and any other legal notices published by ELOKS WARE
LIMITED, including, but not limited to a user license agreement, shall constitute the entire
agreement between you and ELOKS WARE LIMITED concerning the ASKAAR Service. If any
provision of these Terms of Service is deemed invalid by a court of competent jurisdiction, the
invalidity of such provision shall not affect the validity of the remaining provisions of these
Terms of Service, which shall remain in full force and effect. No waiver of any term of this these
Terms of Service shall be deemed a further or continuing waiver of such term or any other term,
and ELOKS WARE LIMITED 's failure to assert any right or provision under these Terms of
Service shall not constitute a waiver of such right or provision. ELOKS WARE LIMITED
reserves the right to amend or modify these Terms of Service at any time, and it is your
responsibility to review these Terms of Service for any changes. If you do not agree to the
revised Terms, your only recourse is to discontinue the use of the ASKAAR Service. Your
continued use of the ASKAAR Service following any amendment of these Terms of Service will
signify your assent to and acceptance of its revised terms.


YOU AND ELOKS WARE LIMITED AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF OR RELATED TO THE ASKAAR 
SERVICE MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES. OTHERWISE, SUCH CAUSE 
OF ACTION IS PERMANENTLY BARRED.
TERMS Clients, both the USER and SERVICE PROVIDERs. ASKAAR is the product ELOKS WARE LIMITED is the owner
			</pre>
		</div>	
	</div>	
	<!-- //main -->
	<!-- copyright -->
	<div class="copyright">
		<p> © 2016 Askaar . All rights reserved </a></p>
	</div>
	<!-- //copyright --> 
</body>
</html>