<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {
  header ("Location: login.php");
  
    


}
else{
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  $privilege=$_SESSION['privilege'];
  last_seen($userid,$db_handle);
   /*Get organization library*/
  $ColNames=array('type','content','title','id');
  $LibInfo=mysql_select("Select * from library where orgid='$orgid' ",$db_handle,null,$ColNames);
  $LibInfoDataRows=explode("]-[",$LibInfo);
  $LibCount=count($LibInfoDataRows);
}
?>
<?php

include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
  <div class="banner">
		    	<h2>
				<a href="index.html">Home</a>
				<i class="fa fa-circle-thin"></i>
				<span>Library</span>
                
				</h2>
		    </div>
            <br/>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
	

			<div class="blank-page">
			
    
         <h3 class="head-top">Staff Library</h3>
         <div class="but_list">
           <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#addnew" id="addnew-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" style='padding:9px'> Add New</a></li>
              <li role="presentation"><a href="#solutions" role="tab" id="solutions-tab" data-toggle="tab" aria-controls="solutions" style='padding:9px'>  Solutions</a></li>
              <li role="presentation"><a href="#vocabulary" role="tab" id="vocabulary-tab" data-toggle="tab" aria-controls="vocabulary" style='padding:9px'>  Vocabulary</a></li>
              
            </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="addnew" aria-labelledby="addnew-tab">
          <div class="tab-pane active" id="horizontal-form">

            <form class="form-horizontal">
            <div class="form-group">
                    <label for="smallinput" class="col-sm-2 control-label label-input-sm">Select Library to Include</label>
                    <div class="col-sm-8">
                    <div id='typeSelectorAlert'></div>
                         <div class='btn-group'>
            <button type="button" class="btn btn-primary" id='vocabularyLibType'>Vocabulary</button>
            <button type="button" class="btn btn-info" id='solutionsLibType'>Solutions</button>
            <input type='hidden' id='libraryType' value=''/>
            <input type='hidden' id='currentInput' value=''/>
            </div>
                    </div>
            </div>
            
                <div class="form-group">
                    <label for="smallinput" class="col-sm-2 control-label label-input-sm" id='libTitle'>Enter Title</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control1 input-sm" id="libTitleInput" placeholder="Welcome to Askaar Staff...">
                    </div>
                </div>
                <div class="form-group" id='libMsg'>
                    <label for="txtarea1" class="col-sm-2 control-label">Message</label>
                    <div class="col-sm-8"><textarea name="txtarea1" id="libMsgInput" cols="50" rows="4" class="form-control1"></textarea></div>
                </div>
                <div class="form-group">
                    <label for="txtarea1" class="col-sm-2 control-label"></label>
                   <div class="col-sm-8">
                   <a href='#!' class='btn btn-info' id='insertNameWildcard'>Add Name wildcard</a>
                   <hr/>

                  
                   <a href='#!' class='btn btn-success btn-block' id='submitLibrary' >Add to library</i></a>
                   
                  </div>
                </div>
               
                

               
                
                </form></div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="solutions" aria-labelledby="solutions-tab">
            <div class="col-md-6" style='float:none;display:inline-block;min-height:300px;'>
          
            <div class="list-group list-group-alternate" id='solList'> 
              <?php for($i=0;$i<$LibCount;$i++){
              $DataVal=explode("}-{",$LibInfoDataRows[$i]);
              $ColNames=array('type','content','title');
              $content=$DataVal[1];
              $title=$DataVal[2];
              $id=$DataVal[3];
              if($DataVal[0]=="solutions"){
                print"
              <a href='#!' data-title='$title' id='$id'  data-content='$content' class='list-group-item editCon'><i class='fa fa-edit pull-right'></i> $title</a>";
                  
              }
              }
            
            
            ?>
               
            </div>
       </div>
       <div class="col-md-6" style='float:right'>
            <div class="tab-pane active" id="horizontal-form">
            <div id='solResults'></div>
            <form class="form-horizontal">
            
            <div class="form-group">
                    <label for="smallinput" class="col-sm-3 control-label label-input-sm">Edit Title</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control1 input-sm" id="solTitle" placeholder="Small Input">
                    </div>
                </div>
                <div class="form-group">
                    <label for="txtarea1" class="col-sm-3 control-label">Edit Message</label>
                    <div class="col-sm-8"><textarea name="txtarea1" id="solMsg" cols="50" rows="4" class="form-control1"></textarea></div>
                </div>
                <div class="form-group">
                    <label for="txtarea1" class="col-sm-3 control-label"></label>
                   <div class="col-sm-8"><div class='btn-group'>
                   <a href='#!' class='btn btn-success btn-block saveSol'>Save Solution</a>
                    <a href='#!' class='btn btn-danger btn-block delSol'>Delete Solution</a>

                   </div></div>
                </div>
                

               
                
                </form></div>
       </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="vocabulary" aria-labelledby="vocabulary-tab">
            <div class="col-md-6" style='float:none;display:inline-block;min-height:300px;'>
          
            <div class="list-group list-group-alternate" id='vocList'> 
              <?php for($i=0;$i<$LibCount;$i++){
              $DataVal=explode("}-{",$LibInfoDataRows[$i]);
              $ColNames=array('type','content','title');
              $content=$DataVal[1];
              $title=$DataVal[2];
              $id=$DataVal[3];
              if($DataVal[0]=="vocabulary"){
                  print"
                  <a href='#!' data-title='$title' id='$id' class='list-group-item editVoc'><i class='fa fa-edit pull-right'></i> $title</a>";
              }
              
              
              
              
              
            }
            
            
            ?>
                 
              
            </div>
       </div>
       <div class="col-md-6" style='float:right'>
            <div class="tab-pane active" id="horizontal-form">
            <div id='vocResults'></div>

            <form class="form-horizontal">
            
            
                <div class="form-group">
                    <label for="txtarea1" class="col-sm-3 control-label">Edit Message</label>
                    <div class="col-sm-8"><textarea name="txtarea1" id="VocMsg" cols="50" rows="4" class="form-control1"></textarea></div>
                </div>
                <div class="form-group">
                    <label for="txtarea1" class="col-sm-3 control-label"></label>
                   <div class="col-sm-8"><div class='btn-group'>
                   <a href='#!' class='btn btn-success btn-block saveVoc' id=''>Save Vocabulary</a>
                    <a href='#!' class='btn btn-danger btn-block delVoc' id=''>Delete Vocabulary</a>
                    </div>
                  </div>
                </div>
                

               
                
                </form></div>
       </div>
          </div>
          
        </div>
    </div></div>
  
	        </div>
	       </div>
       <input type='hidden' id='staffname' value='<?php echo $fullname;?>'/>    
			
		<!--//content-->
<?php
include"footer.php";
?>        