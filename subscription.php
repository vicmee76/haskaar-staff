<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) 
{
  header ("Location: login.php"); 
}
else
{

  include"classes/class.subscribers.php";
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle); 
  $my_country_name = '';
  $my_country_code = ''; 
  $sql = "select countries.name,countries.code from organization inner join countries on countries.code = organization.country where  organization.orgid = '$orgid'";
  $result = mysqli_query($db_handle,$sql);
  if($result === false)
  {
    $my_country_name = 'NIGERIA';
    $my_country_code = 'NG'; 
  }
  else
  {
    $count = mysqli_num_rows($result);
    if($count > 0)
    {
      while ($data = mysqli_fetch_array($result))
      {
        $my_country_name = $data['name'];
        $my_country_code = $data['code']; 
      }
    }
    else
    {
      $my_country_name = 'NIGERIA';
      $my_country_code = 'NG';
    }
  }

  $subscribers = new subscribers($orgid,$orgid,'web');
  $subscribers_stats = $subscribers->check_stats($my_country_code);

}

include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 		<!--banner-->	
		    <div class="banner">
		    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
				<span>Subscribers</span>
				<a href='#!' class='btn btn-sm btn-default pull-right sub_stats' id='0'>Show Statistics</a>
				</h2>
		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
	 <div class="blank-page">
   	<!--gallery-->
    <div class="gallery" id='sub_list'>
    
    <?php
      $the_subscribers_list = $subscribers->list_subscribers(0);
      if($the_subscribers_list['success'] == 1)
      {
        $the_subscribers_data = $the_subscribers_list['data'];
        for($i = 0; $i < count($the_subscribers_data); $i++)
        {
          $subscriber_img = $the_subscribers_data[$i]['image'];
          $subscriber_name = $the_subscribers_data[$i]['fullname'];
          $subscriber_id = $the_subscribers_data[$i]['hash'];

          print
          "
            <div class='subscriber'>
              <div class='photo'>
                <img src='images/$subscriber_img'/>
              </div>
              <div class='actions'>
                <span class='name open_profile_popup' id='$subscriber_id'> $subscriber_name</span>
              </div>
            </div>

          ";
        }

        if($the_subscribers_list['more'] == 1)
        {
          echo 
          "
            <a href='#!' id='20' data-type='subscribers.php' class='btn btn-info btn-block loadmore'>Load More Subscribers</a>

          ";
        }
      }
      else
      {
        $error = $subscribers->error_finder;
        print
        "
          <div class='alert alert-warning'> You currently have no subscribers</div>

        ";

      }


    ?>  	  
 	 	
 	  </div>
    <div class="gallery" id='sub_stat' <?php echo $hide_this;?>>

     
      
      <div class="list-group list-group-alternate"> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['total'];?></span> <i class="ti ti-email"></i> Total Subscribers </a> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['male'];?></span> <i class="ti ti-eye"></i> Male Subscribers </a> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['female'];?></span> <i class="ti ti-headphone-alt"></i> Female Subscribers </a> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['home'];?></span> <i class="ti ti-comments"></i> Subscribers From <?php echo $my_country_name;?> </a> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['abroad'];?></span> <i class="ti ti-bookmark"></i> Subscribers From Other Countries </a> 
        <a href="#" class="list-group-item"><span class="badge badge-success"><?php echo $subscribers_stats['today'];?></span> <i class="ti ti-bell"></i> Today's Subscribers </a> 
      </div>
   
      
    </div>
	  <!--//gallery-->
  </div>
 </div>
           
  <input type='hidden' id='staffname' value='<?php echo $fullname;?>'/>
  <!--//content-->
<?php
include"footer.php";
?>        