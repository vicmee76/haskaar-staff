<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) 
{
  header ("Location: login.php"); 
}
else
{

  include"classes/class.event.php";
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle); 
  $event = new events($orgid);

}
include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
   <div class="content-main">
		<!--banner-->	
	     <div class="banner">
	    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Events</span>
			<a href='#!' class='btn btn-sm btn-default pull-right  toggle_media_list' data-type='Event' id='0'>Add New Event</a>
			</h2>
	    </div>
	<!--//banner-->
	 	<div class="blank">
		

			<div class="blank-page" id='media_list'>
				<div class='event_container'>
					<div class='events'>
		        	<?php

		        		$event_list = $event->list_events();
		        		if($event_list['success'] == 1)
		        		{
		        			$event_data = $event_list['data'];
		        			for($i=0;$i<count($event_data);$i++)
		        			{
		        				$title = $event_data[$i]['title'];
		        				$timestamp = $event_data[$i]['timestamp'];
		        				$event_id = $event_data[$i]['programid'];
		        				$image = $event_data[$i]['image'];
		        				$datetime = date('D d M Y',$timestamp);
		        				$how_long='';
		        				$how_long_style = '';
		        				if($timestamp > time())
		        				{
		        					$how_long = "Up Coming";
		        					$how_long_style = "bg-warning";

		        				}
		        				else if($timestamp <= time() && $timestamp >= strtotime(date('m-D-Y')))
		        				{
		        					$how_long = "Today";
		        					$how_long_style = "bg-success";
		        				}
		        				else if(time() > $timestamp && strtotime(date('m:d:Y')) > $timestamp)
		        				{
		        					$how_long = "Past";
		        					$how_long_style = "bg-danger";
		        				}

		        				print 
		        				"
		        					<div class='event_item'>
		        						<div class='img_cont'>
		        							<img src='files/images/$image' />
	        							</div>
	        							<div class='title'>$title</div>
	        							<div class='actions'>
	        								<span class='time $how_long_style'>$how_long : $datetime</span>
	        								<span class='button show_event' id='$event_id'> View <i class='fa fa-angle-double-right' aria-hidden='true'></i></span>
        								</div>
    								</div>

		        				";
		        			}

		        		}

		        	?>
		        	</div>
		        	<div class='viewer'>
			        	
		        	</div>
		        </div>
		    </div>
		    <div class="blank-page" id='add_media' style='display:none'>

		    	<div class="gallery">
				    <div id='results'></div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Title</label>
				        <input type="text" class="form-control1" id="title">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Description</label>
				        <input type="text" class="form-control1" id="description">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Date & Time</label>
				        <input type="text" class="form-control1" id="datetime">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Address</label>
				        <input type="text" class="form-control1" id="address">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Contact</label>
				        <input type="text" class="form-control1" id="contact">
				    </div>				    				    
				    <!-- Example 2 -->
				       <input type="file" name="files[]" id="filer_input" multiple="multiple">
				       <input id="media_type" type="hidden" value='photo'/>
				       <a href='#!' id='add_event' class='btn btn-primary'> Post Event </a>
				       <a href='#!' id='' class='btn btn-warning edit_event' style='display:none'>Edit Event</a>
				    <!-- end of Example 2 -->

				</div>


		    </div>
		</div>
	</div>
    <script type='text/javascript'>
        $(document).ready(function(){

          $('#filer_input').filer({
            showThumbs: true,
            addMore: true,
            allowDuplicates: false
          });
          $("#datetime").datepicker();

        });
    </script> 
			
		<!--//content-->
<?php
include"footer.php";
?>        