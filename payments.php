<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) 
{
  header ("Location: login.php"); 
}
else
{

  include"classes/class.payment.php"; 
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle); 
  $payment = new payment($orgid);


}
include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
   <div class="content-main">
		<!--banner-->	
	     <div class="banner">
	    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Payments</span>
			<a href='#!' class='btn btn-sm btn-default pull-right  show_history' data-toggle='modal' data-target='#myModal' id='0'>Transaction History</a>
			<a href='#!' class='btn btn-sm btn-default pull-right  toggle_media_list' data-type='Payment' id='0'>Add New Payment</a>
			</h2>
	    </div>
	<!--//banner-->
	 	<div class="blank">
		

			<div class="blank-page" id='media_list'>
				
				<div class='gallery'>
				<div id='results2'></div> 
		        	<?php

		        		$payment_list = $payment->list_payments();
		        		if($payment_list['success'] == 1)
		        		{
		        			$payment_data = $payment_list['data'];
		        			for($i=0;$i<count($payment_data);$i++)
		        			{
		        				$title = $payment_data[$i]['title'];
		        				$description = $payment_data[$i]['description'];
		        				$payment_id = $payment_data[$i]['paymentid'];
		        				$min = $payment_data[$i]['min'];
		        				$max = $payment_data[$i]['max'];
		        				$type = $payment_data[$i]['amounttype'];
								setlocale(LC_MONETARY, "en_NG");
		        				$amount = 0;
		        				if($min == $max)
		        				{
		        					$amount =  money_format("%i",$min);
		        				}
		        				else
		        				{
		        					$amount = "(". money_format("%i",$min)." - ".money_format("%i",$max).")";
		        				}
		        				$timestamp = $payment_data[$i]['timestamp'];     				
		        				
		        				print 
		        				"
									<div class='payment' id='payment_$payment_id' >
										<div class='title'>
											$title <i class='fa fa-angle-right'></i> $amount
										</div>
										<div class='description'>
											$description
										</div>
										<a href='#!' id='$payment_id' class='btn btn-primary edit_payment_request' data-title='$title' data-desc='$description' data-min='$min' data-max='$max' data-type='$type'>Edit</a>
										<a href='#!' id='$payment_id' data-title='$title' class='btn btn-info view_transaction' data-toggle='modal' data-target='#myModal'>Transaction</a>
										<a href='#!' id='$payment_id' class='btn btn-danger delete_payment'>delete</a>
										
									</div>

		        				";
		        			}

		        		}
		        		else
		        		{
		        			print
		        			"
		        				<div class='alert alert-info'> You have not set up any payment yet! </div>

		        			";
		        		}

		        	?>
				</div>
		    </div>
		    <div class="blank-page" id='add_media' style='display:none'>

		    	<div class="gallery">
				    <div id='results'></div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Title</label>
				        <input type="text" class="form-control1" id="title">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Description</label>
				        <input type="text" class="form-control1" id="description">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Amount Type</label>
				        <select id='amount_type' class='form-control '>
				        	<option value='0'>Choose</option>
				        	<option value='1'>Range</option>
				        	<option value='2'>Definite</option>
			        	</select>
				    </div>
				    <div class="form-group range_element" style='display:none;'>
				        <label class="control-label" for="inputSuccess1">Minimum Amount</label>
				        <input type="text" class="form-control1" id="min">
				    </div>
				    <div class="form-group range_element" style='display:none;'>
				        <label class="control-label" for="inputSuccess1">Maximum Amount</label>
				        <input type="text" class="form-control1" id="max">
				    </div>
				    <div class="form-group definite_element" style='display:none;'>
				        <label class="control-label" for="inputSuccess1">Amount</label>
				        <input type="text" class="form-control1" id="amount">
				    </div>
				    <!-- Example 2 -->
			       <a href='#!' id='add_payment' class='btn btn-primary'> Add Payment </a>
			       <a href='#!' id='' class='btn btn-primary edit_payment' style='display:none;'> Edit Payment </a>
			       <a href='#!' id='' class='btn btn-danger cancel_operation' style='display:none;'> Abort </a>
			       <!-- end of Example 2 -->

				</div>


		    </div>
		</div>
	</div>
    <script type='text/javascript'>
        $(document).ready(function(){

          $('#filer_input').filer({
            showThumbs: true,
            addMore: true,
            allowDuplicates: false
          });

        });
    </script> 
			
		<!--//content-->
<?php
include"footer.php";
?>        