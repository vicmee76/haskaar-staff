<?php 
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) {
  header ("Location: login.php");
    
}
else{
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  /*echo $_SESSION['privilege'];*/
  last_seen($userid,$db_handle);

  /*Get organization settings*/
  $ColNames=array('status','name','about','slogan','email','image','cat','country','model');
  $OrganizationInfo=mysql_select("Select * from organization where orgid='$orgid' ",$db_handle,null,$ColNames);
  $OrgInfoDataRows=explode("}-{",$OrganizationInfo);
  $name=$OrgInfoDataRows[1];
  $status=$OrgInfoDataRows[0];
  $about=$OrgInfoDataRows[2];
  $slogan=$OrgInfoDataRows[3];
  $email=$OrgInfoDataRows[4];
  $image=$OrgInfoDataRows[5];
  $category=$OrgInfoDataRows[6];
  $country=$OrgInfoDataRows[7];
  $model = $OrgInfoDataRows[8];
  $_SESSION['status']=$status;



}
?>
<?php

include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
  <!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Organization Settings</span>
				</h2>
		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="blank">
	

			<div class="blank-page">
				
	       <br>

  <div class="container" style='width:100%'>
    <div class="row">
      <div class="">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 lead">
                Organization Identity
                <a href='guide-2.pdf' class='btn btn-info pull-right' target="_blank">Download Guide</a>
                <hr>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 text-center">
              
                <div id="img-preview-block" class="img-circle avatar avatar-original center-block" id='imageviewer' style="background-size:cover; 
                background-image:url('images/<?php echo $image;?>')"></div>
                <br> 
                <span class="btn btn-link btn-file">Edit avatar <input type="file" id="avatar"></span>
              </div>
              <div class="col-md-8">
              <div id='results'>
              <?php if($status==0){echo "<div class='alert alert-info '> You are yet to add up the organization information,once you have updated the organization information then users can see your organization across askaar</div>";}?>
              </div>
                <div class="form-group">
                  <label for="user_last_name">Organization Name</label>
                  <input type="text" class="form-control" value="<?php echo $name;?>" id="orgname">
                </div>
                <div class="form-group">
                  <label for="user_first_name">Organisation Slogan</label>
                  <input type="text" class="form-control" value="<?php echo $slogan;?>" id="orgslogan">
                </div>
                <div class="form-group">
                  <label for="user_middle_name">Email</label>
                  <input type="text" class="form-control" value="<?php echo $email;?>" id="orgemail">
                </div>
                <div class="form-group">
                  <label for="user_middle_name">Description</label>
                  <textarea class="form-control" id="orgabout" ><?php echo $about?></textarea>
                </div>
                <div class="form-group">
                <label for="select" >Select Country</label>
                
                <div >
                  <select class="form-control" id="country">
                    <?php
                      $sql = "select * from countries";
                      $result = mysqli_query($db_handle,$sql);
                      if($result === false)
                      {

                      }
                      else
                      {
                        while($data = mysqli_fetch_assoc($result))
                        {
                          $country_code = $data['code'];
                          $country_name = $data['name'];
                          if($country == $country_code)
                          {
                            echo "<option value='$country_code' selected='selected'>$country_name</option>";
                          }
                          else
                          {
                            echo "<option value='$country_code'>$country_name</option>";
                          }
                        }
                      }
                    ?>
                  </select>
                  
                </div>
              </div>
              <div class="form-group">
                <label for="select" >Select Model</label>
                <a href='#!' class='pull-right models_guide' >Don't understand the models? Click Me </a>
                <div >
                  <select class="form-control" id="country">
                    <?php
                      $sql = "select * from models order by name";
                      $result = mysqli_query($db_handle,$sql);
                      if($result === false)
                      {

                      }
                      else
                      {
                        while($data = mysqli_fetch_assoc($result))
                        {
                          $id = $data['id'];
                          $name = ucwords($data['name']);
                          if($model == $id)
                          {
                            echo "<option value='$id' selected='selected'>$name</option>";
                          }
                          else
                          {
                            echo "<option value='$id'>$name</option>";
                          }
                        }
                      }
                    ?>
                  </select>
                  
                </div>
              </div>
             <div class="form-group">
                <label for="select" >Select Category</label>
                <div >
                  <select class="form-control" id="category">
                  <?php 
                    
                    $getCat=mysqli_query($db_handle,"Select * from categories where status=1 or status=0 order by id asc");
                    $catnumrows=mysqli_num_rows($getCat);
                    if($catnumrows>0){
                      $catcounter=1;
                      while($catdata=mysqli_fetch_assoc($getCat)){
                          $catname=$catdata['name'];
                          $catid=$catdata['id'];
                          if($catid== (int)$category){
                            echo "<option value='$catid' selected='selected'>$catname</option>";
                          }
                          else{
                            echo "<option value='$catid'>$catname</option>";
                          }
                          
                          $catcounter=$catcounter+1;
                      }

                    }



                  ?>
                  </select>
                </div>
              </div>
                
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-user-modal">
                  Delete Account
                </button>
                
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <hr>
                <button class="btn btn-primary pull-right" id='saveIdentity'><i class="glyphicon glyphicon-floppy-disk"></i> 
                <?php if($status==1){echo "Save Changes";}else{echo "Publish Account";} ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





  <!-- So this is for password	 -->
   <br>

  <div class="container" style='width:100%'>
    <div class="row">
      <div class="">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12 lead">
                Security Settings
                <hr>
              </div>
            </div>
            <div class="row">
              <div id='results2'></div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="user_last_name">Enter Current Password</label>
                  <input type="password" class="form-control" id="oldpword">
                </div>
                <div class="form-group">
                  <label for="user_first_name">Enter New Password</label>
                  <input type="password" class="form-control" id="newpword">
                </div>
                <div class="form-group">
                  <label for="user_middle_name">Re-enter New Password</label>
                  <input type="password" class="form-control" id="renewpword">
                </div>
               
                          
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr>
                <button class="btn btn-primary pull-right changePword"><i class="glyphicon glyphicon-floppy-disk"></i> Save Changes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    
	        </div>
	       </div>
           
			
		<!--//content-->
<?php
include"footer.php";
?>        