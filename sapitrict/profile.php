
<?php
if(isset($_REQUEST['userid'])){
  	include"../config.php";
  	include"../modules/input_module.php";
    include"../modules/sql_module.php";
    include_once"../classes/class.profile.php";
    include"../handlers/file_upload_function.php";
	  validateToken($db_handle);
  	$userid = $_REQUEST['userid'];
    $apptoken = $_REQUEST['apptoken'];
    $command = $_REQUEST['cmd'];
    $error_message = "";
    $profile = new profile($userid,$userid);
    last_seen($userid,$db_handle);
  	$userid=just_validate_input($userid,$db_handle,'text',1);
    if($userid == "error")
    {
      $error_message .= "Invalid User /r/n";
    }
    if($command == "get_user_profile" || $command == "get_my_profile" || $command == "edit_profile" || $command == "get_org_profile" || $command == "create_profile" || $command == "set_profile_photo");
    {

    }
    else
    {
      $error_message .= "Invalid Command";
    }

    if($error_message == "")
    {
      if($command == "get_user_profile")
      {
        $member_id = $_REQUEST['member_id'];
        $member_id =remove_quotes(just_validate_input($member_id,$db_handle,'text',7));
        $profile->set_client_id($member_id);
        $get_user_type = $profile->validate_user_type();
        if($get_user_type == "user")
        {
          $user_details = $profile->get_user_profile();
          if($user_details !== 0)
          {
            header("content-type:application/json");
            echo json_encode($user_details);
          }
          else
          {
            echo 0;
          }
        }
        else
        {
          $staff_details = $profile->get_staff_profile();
          if($staff_details !== 0)
          {
            header("content-type:application/json");
            echo json_encode($staff_details);
          }
          else
          {
            echo 0;
          }
        }

      }
      elseif($command == "get_org_profile")
      {
        $member_id = $_REQUEST['member_id'];
        $member_id =remove_quotes(just_validate_input($member_id,$db_handle,'text',7));
        $profile->set_client_id($member_id);
        $user_details = $profile->get_org_profile();
        if($user_details !== 0)
        {
          header("content-type:application/json");
          echo json_encode($user_details);
        }
        else
        {
          echo 0;
        }
        
      }

      elseif($command == "edit_profile")
      {
        $fullname = $_REQUEST['fullname'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $status = $_REQUEST['status'];
        $relationship = ucwords($_REQUEST['relationship']);
        $religion = ucwords($_REQUEST['religion']);
        $education = ucwords($_REQUEST['education']);
        $country = strtoupper($_REQUEST['country']);
        $error_message = "";
        $fullname = just_validate_input($fullname,$db_handle,'text',3);
        $email = just_validate_input($email,$db_handle,'email',4);
        $phone = just_validate_input($phone,$db_handle,'num',6);
        $relationships = "Friend Mentor Flirt Business None";
        $statuses = "Single Married Divorced Single-Parent Widowed None";
        $educations = "Student Graduate Research Others None";
        $religions = "Christian Muslim Atheist Traditional Others None";
        $countries = "AF AX AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BA BW BR IO BN BG BF BI KH CM CA CV KY CF TD CL CN CX CC CO KM CG CD CK CR CI HR CU CY CZ DK DJ DM DO EC EG SV GQ ER EE ET FK FO FJ FI FR GF PF GA GM GE DE GH GI GR GL GD GP GU GT GG GN GW GY HT VA HN HK HU IS IN ID IR IQ IE IM IL IT JM JP JE JO KZ KE KI KP KR KW KG LA LV LB LS LR LY LI LT LU MO MK MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ MM NA NR NP NL AN NC NZ NI NE NG NU NF MP NO OM PK PW PS PA PG PY PE PH PN PL PT PR QA RO RU RW RE BL SH KN LC MF PM VC WS SM ST SA SN RS SC SL SG SK SI SB SO ZA SS GS ES LK SD SR SJ SZ SE CH SY TW TJ TZ TH TL TG TK TO TT TN TR TM TC TV UG UA AE GB US UY UZ VU VE VN VG VI WF YE ZM ZW";
        if(strpos($relationships,$relationship) === false)
        {
          $error_message .= "Incorrect Relationship /r/n";
        }
        else
        {
          $relationship = just_validate_input($relationship,$db_handle,'text',4);
        }
        if(strpos($religions,$religion) === false)
        {
          $error_message .= "Incorrect religion /r/n";
        }
        else
        {
          $religion = just_validate_input($religion,$db_handle,'text',4);
        }
        if(strpos($educations,$education) === false)
        {
          $error_message .= "Incorrect education /r/n";
        }
        else
        {
          $education = just_validate_input($education,$db_handle,'text',4);
        }
        if(strpos($statuses,$status) === false)
        {
          $error_message .= "Incorrect status /r/n";
        }
        else
        {
          $status = just_validate_input($status,$db_handle,'text',4);
        }

        //Run Edit Now

        if($error_message == "")
        {
          $edit_profile = $profile->edit_profile($fullname,$email,$phone,$relationship,$religion,$status,$education);
          if($edit_profile == 1)
          {
            echo 1;
          }
          else
          {
            echo "Try again later!";
          }
        }
        else
        {
          echo $error_message;
        }
      }
    }

    elseif($command == "create_profile")
    {

    }

    elseif($command == "set_profile_photo")
    {
      $has_file = $_REQUEST['has_file'];
      $file = $_FILES['file'];      
    }

    

  
            
            
     
	

}



?>
