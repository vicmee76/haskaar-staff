<?php
if(isset($_REQUEST['userid'])){
  	include"../config.php";
  	include"../modules/input_module.php";
    include"../modules/sql_module.php";
    include_once"../classes/class.reaction.php";
	  validateToken($db_handle);
  	$userid = $_REQUEST['userid'];
    $apptoken = $_REQUEST['apptoken'];
    $cmd = $_REQUEST['cmd'];
    $id = $_REQUEST['id'];
    $error_message = "";
    last_seen($userid,$db_handle);
  	$userid=just_validate_input($userid,$db_handle,'text',1);       
    $id = just_validate_input($id,$db_handle,'text',3);

    if($id=="error"){
      $error_message .= "Invalid Reactant /r/n";
    }
    if($userid == "error")
    {
      $error_message .= "Invalid User /r/n";
    } 

    if($cmd == "like" || $cmd == "unlike" || $cmd == "comment" || $cmd == "view" || $cmd == "likers" || $cmd == "commenters" || $cmd == "viewers" || $cmd == "delete_comment" || $cmd == "edit_comment")
    {

    }
    else
    {
      $error_message .= "Unknown command <br/>";
    }
    
    if($error_message == ""){
      
      
      $reactions = new reactions(remove_quotes($userid),remove_quotes($id));
      if($cmd == "edit_comment")
      {
        $comment = $_REQUEST['comment'];
        $comment_id = (int)$_REQUEST['comment_id'];

        $comment = just_validate_input($comment,$db_handle,'text',2);
        
        if($comment !== "error")
        {               
          $edit_comment = $reactions->edit_comment($comment,$comment_id);
          if($edit_comment == 1)
          {
            echo 1;
          }
          else
          {
            echo 0;
          }
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "delete_comment")
      {
        $delete_comment = $reactions->delete_comment();
        if($delete_comment == 1)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "like")
      {
        $like = $reactions->like_stuff();
        if($like == 1)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "view")
      {
        $view = $reactions->view_stuff();
        if($view == 1)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "unlike")
      {
        $unlike = $reactions->unlike_stuff();
        if($unlike == 1)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "comment")
      {
        $comment = $_REQUEST['comment'];
        
        
        $comment = just_validate_input($comment,$db_handle,'text',2);     
        
        
        if($comment !== "error")
        {
          
          $add_comment = $reactions->comment_stuff($comment);
          if($add_comment == 1)
          {
            echo 1;
          }
          else
          {
            echo "Something went wrong! try again later or refresh page!";
          }
          
          
        }
        else
        {
          echo "Please check your parameters and try again!";
        }
      }
      else if($cmd == "commenters")
      {
        $get_comments = $reactions->view_commenters();
        if($get_comments['success'] == 1)
        {
          $data= $get_comments['data'];
          for($i=0;$i<count($data);$i++)
          {
            $name = $data[$i]['fullname'];
            $user_id = $data[$i]['hash'];
            $comment = $data[$i]['comment'];
            $editted = $data[$i]['editted'];
            $datetime = date("M d Y",$data[$i]['dateofcomment']);
            $image = $data[$i]['image'];  
            
          }
          header("content-type:application/json");
          echo json_encode($data);
        }
        else
        {
          echo 0;
        }
      }
      else if($cmd == "likers")
      {
        $likers = $reactions->view_likers();
        if($likers['success'] == 1)
        {
          $data = $likers['data'];
          for($i=0;$i<count($data);$i++)
          {
            $name = $data[$i]['fullname'];
            $user_id = $data[$i]['hash'];           
            $image = $data[$i]['image'];  
            
          }
          header("content-type:application/json");
          echo json_encode($data);

        }
        else
        {
          echo 0;
        }
      }

      else if($cmd == "viewers")
      {
        $viewers = $reactions->view_viewers();
        if($viewers['success'] == 1)
        {
          $data = $viewers['data'];
          for($i=0;$i<count($data);$i++)
          {
            $name = $data[$i]['fullname'];
            $user_id = $data[$i]['hash'];           
            $image = $data[$i]['image'];  
        
          }
          header("content-type:application/json");
          echo json_encode($data);          
        }
        else
        {
          echo 0;
        }
      }

    }
    else
    {
      echo $error_message;
    }     
	

}



?>
