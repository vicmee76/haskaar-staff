
<?php
if(isset($_REQUEST['userid'])){
  	include"../config.php";
  	include"../modules/input_module.php";
    include"../modules/sql_module.php";
    include"../classes/class.feed.php";
    include_once"../classes/class.profile.php";
    include"../handlers/file_upload_function.php";
	  validateToken($db_handle);
  	$userid = $_REQUEST['userid'];
    $apptoken = $_REQUEST['apptoken'];
    $rcounter = (int)$_REQUEST['counter'];
    $command = $_REQUEST['cmd'];
    $error_message = "";
    $feed = new feed($useid,$userid);
    $profile = new profile($userid,$userid);
    last_seen($userid,$db_handle);
  	$userid=just_validate_input($userid,$db_handle,'text',1);
    if($userid == "error")
    {
      $error_message .= "Invalid User /r/n";
    }
    if($command == "get_all_feed" || $command == "get_user_feed" || $command == "get_my_feed" || $command == "get_feed_details" || $command == "edit_feed" || $command == "delete_feed" || $command == "get_room_feed" || $command == "add_feed")
    {

    }
    else
    {
      $error_message .= "Invalid Command";
    }

    if($command == "get_all_feed")
    {
      $feed_list = $feed->get_all_feed($counter);
      if($feed_list !== 0)
      {
        header("content-type:application/json");
        echo json_encode($feed_list);
      }
      else
      {
        echo 0;
      }
    }
    elseif($command == "get_user_feed")
    {
      $user_id = $_REQUEST['memberid'];
      $user_id = remove_quotes(just_validate_input($user_id,$db_handle,'text',5));
      $feed->set_client_id($user_id);
      $feed_list = $feed->get_user_feed($counter);
      if($feed_list !== 0)
      {
        header("content-type:application/json");
        echo json_encode($feed_list);
      }
      else
      {
        echo 0;
      }
    }
    elseif($command == "get_my_feed")
    {
      $user_id = $userid;
      $user_id = remove_quotes(just_validate_input($user_id,$db_handle,'text',5));
      $feed->set_client_id($user_id);
      $feed_list = $feed->get_user_feed($counter);
      if($feed_list !== 0)
      {
        header("content-type:application/json");
        echo json_encode($feed_list);
      }
      else
      {
        echo 0;
      }
    }
    elseif($command == "get_feed_details")
    {
      $feed_id = $_REQUEST['feedid'];
      $feed_id = remove_quotes(just_validate_input($feed_id,$db_handle,'text',5));
      $feed->set_client_id($user_id)
      $feed_list = $feed->get_feed_details($feed_id);
      if($feed_list !== 0)
      {
        header("content-type:application/json");
        echo json_encode($feed_list);
      }
      else
      {
        echo 0;
      }
    }
    elseif($command == "get_room_feed")
    {
      $room_id = $_REQUEST['roomid'];
      $room_id = remove_quotes(just_validate_input($room_id,$db_handle,'text',5));
      $feed_list = $feed->get_room_feed($counter,$room_id);
      if($feed_list !== 0)
      {
        header("content-type:application/json");
        echo json_encode($feed_list);
      }
      else
      {
        echo 0;
      }
    }
    elseif($command == "add_feed")
    {
      $msg = $_REQUEST['update'];
      $has_file = $_REQUEST['has_file'];
      $has_tags = $_REQUEST['has_tags'];
      $roomid = $_REQUEST['roomid'];
      $tags = array();
      $files = array();
      $type = "post";
      $error_message = "";

      if((int)$has_tags == 1)
      {
        $the_tags = explode("~",$_REQUEST['tags']);
        for($i=0;$i < count($the_tags);$i++)
        {
          $is_user = $profile->validate_user($the_tags[$i]);
          if($is_user == 1)
          {
            $tags[] = $the_tags[$i];
          }
          else
          {
            $error_message .= "Tagging Error /r/n";
          }
        }

      }
      else
      {
        $tags = null;
      }

      if((int)$has_file == 1)
      {
        $file = $_FILES['file'];
        $type = "photo";
        $upload_file = upload_file('photo',$file);
        if($upload_file['success'] == 1)
        {
          $files[] = "'".$upload_file['message']."'";
        }
        else
        {
          $error_message .= "Upload Error /r/n";
        }
      }

      if($msg == "")
      {
        $msg = null;
      }
      else
      {
        $msg = just_validate_input($msg,$db_handle,'text',1)
        if($msg == "error")
        {
          $error_message .= "Invalid Update /r/n";
        }
      }
      if($roomid !== "")
      {
        $roomid = just_validate_input($roomid,$db_handle,'num',1);
        if($roomid == "error")
        {
          $error_message .= "Invalid Room Id /r/n";
        }
      }
      else
      {
        $error_message .= "Invalid Room Id /r/n";
      }

      if($error_message == "")
      {
        $guid = $generate_guid($userid.(rand(4,28)*time()));
        $guid = just_validate_input($guid,$db_handle,'text',8);
        $add_feed = $feed->add_feed($msg,$tags,$files,$type,$guid,$roomid);
        if($add_feed == 1)
        {
          echo 1;
        }
        else
        {
          echo "error Occured!";
        }
      }
      else
      {
        echo $error_message;
      }

    }  

    elseif($command == "delete_feed")
    {
      $feed_id = $_REQUEST['feed_id'];
      $feed_id = remove_quotes(just_validate_input($feed_id,$db_handle,'text',7));
      if($feed_id == "error")
      {
        echo 0;
      }
      else
      {
        $delete_feed = $feed->delete_feed($feed_id);
        if($delete_feed == 1)
        {
          echo 1;
        }
        else
        {
          echo 0;
        }
      }
    }  

  
            
            
     
	

}



?>
