<?php
if(isset($_REQUEST['userid'])){
  	include"../config.php";
  	include"../modules/input_module.php";
    include"../modules/sql_module.php";
    include_once"../classes/class.event.php";
	  validateToken($db_handle);
  	$userid = $_REQUEST['userid'];
    $apptoken = $_REQUEST['apptoken'];
    $cmd = $_REQUEST['cmd'];
    $org_id = $_REQUEST['org_id'];
    $id = $_REQUEST['id'];
    $error_message = "";
    last_seen($userid,$db_handle);
  	$userid=just_validate_input($userid,$db_handle,'text',1);
    $org_id = remove_quotes(just_validate_input($org_id,$db_handle,'text',3));       
    $id = just_validate_input($id,$db_handle,'text',null);

    if($id=="error"){
      $error_message .= "Invalid ID /r/n";
    }
    if($org_id=="error"){
      $error_message .= "Invalid Organization /r/n";
    }
    if($userid == "error")
    {
      $error_message .= "Invalid User /r/n";
    } 

    if($cmd == "get" || $cmd == "get_full")
    {

    }
    else
    {
      $error_message .= "Unknown command <br/>";
    }
    
    if($error_message == "")
    {
      $event = new events($org_id);
      if($cmd == "get")
      {
        $event_list = $event->list_events();
        header("content-type:application/json");
        echo json_encode($event_list);
        if($event_list['success'] == 1)
        {
          $event_data = $event_list['data'];
          for($i=0;$i<count($event_data);$i++)
          {
            $title = $event_data[$i]['title'];
            $timestamp = $event_data[$i]['timestamp'];
            $event_id = $event_data[$i]['programid'];
            $image = $event_data[$i]['image'];
            $datetime = date('D d M Y',$timestamp);
            $how_long='';
            $how_long_style = '';
            if($timestamp > time())
            {
              $how_long = "Up Coming";
              $how_long_style = "bg-warning";

            }
            else if($timestamp <= time() && $timestamp >= strtotime(date('m-D-Y')))
            {
              $how_long = "Today";
              $how_long_style = "bg-success";
            }
            else if(time() > $timestamp && strtotime(date('m:d:Y')) > $timestamp)
            {
              $how_long = "Past";
              $how_long_style = "bg-danger";
            }

          }
        }
      }
      elseif($cmd == "get_full")
      {
        $event_details = $event->event_details($id);
        header("content-type:application/json");
        echo json_encode($event_data);
        if($event_details['success'] == 1)
        {
          $event_data = $event_details['data'];
          $title = $event_data[0]['title'];
          $timestamp = $event_data[0]['timestamp'];
          $event_id = $event_data[0]['programid'];
          $image = $event_data[0]['image'];
          $description = $event_data[0]['description'];
          $address = $event_data[0]['address'];
          $contact = $event_data[0]['contact'];
          $date_posted = date('D d M Y',$event_data[0]['datetime']);
          $datetime = date('D d M Y',$timestamp); 

        }
        
      }
      
    }
	

}



?>
