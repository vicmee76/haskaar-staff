<?php
if(isset($_REQUEST['userid'])){
  	include"../config.php";
  	include"../modules/input_module.php";
    include"../modules/sql_module.php";
    include"../classes/class.subscribers.php";
	  validateToken($db_handle);
  	$userid = $_REQUEST['userid'];
    $apptoken = $_REQUEST['apptoken'];
    $rcounter = (int)$_REQUEST['counter'];
    $provider = $_REQUEST['providerid'];
    $command = $_REQUEST['command']
    last_seen($userid,$db_handle);
  	$userid = just_validate_input($userid,$db_handle,'text',1);
    $provider = just_validate_input($provider,$db_handle,'text',2);
    $subscribers = new subscribers($_REQUEST['userid'],remove_quotes($provider),'mobile');

    if($command == "list")
    {
      $json_data = $subscribers->list_subscribers($rcounter);
      header('content-type:Application/json');
      echo $json_data;  
    }
    
    elseif ($command == "subscribe")
    {
      $response = $subscribers->subscribe();
      echo $response;
    }

    elseif ($command == "unsubscribe")
    {
      $response = $subscribers->un_subscribe();
      echo $response;
    }

}
?>
