var globsockuser='2345678';
function Alert_Div(message,canHide,type,cmd){
    //Do you wanna hide lol
    
    if(canHide==1){
        var DisM="alert-dismissable";
    }
    else{
        var DisM=""
    }
    //Color And alert Type
    if(type=="info"){
        var typeOf="alert-info";
    }
    else if (type=="warning"){
        var typeOf="alert-warning";
    }
    else if (type=="success"){
        var typeOf="alert-success";
    }
    else if (type=="primary"){
        var typeOf="alert-primary";
    }
    else if (type=="default"){
        var typeOf="alert-default";
    }
    
    //so here is for commands you dont know yet 
    if(cmd==null || cmd ==''){
        cmd='';
    }
    var divConst="<div class='alert "+typeOf+" "+cmd+" "+DisM+"'>"+message+"</div>";
    return divConst; 
    
    
    
    
}

(function() {
    "use strict";

    // custom scrollbar

    $("html").niceScroll({styler:"fb",cursorcolor:"#1ABC9C", cursorwidth: '12', cursorborderradius: '10px', background: '#424f63', spacebarenabled:false, cursorborder: '0',  zindex: '1000'});

    $(".scrollbar1").niceScroll({styler:"fb",cursorcolor:"rgba(97, 100, 193, 0.78)", cursorwidth: '6', cursorborderradius: '0',autohidemode: 'false', background: '#F1F1F1', spacebarenabled:false, cursorborder: '0'});

	
	
    $(".scrollbar1").getNiceScroll();
    if ($('body').hasClass('scrollbar1-collapsed')) {
        $(".scrollbar1").getNiceScroll().hide();
    }

})(jQuery);

$(document).ready( function() {
var socket = io('http://23.238.173.209:4000');




/*Navigate via Message Tabs*/
$(".chttab").on('click',function(e){
    e.preventDefault;
    $(this).siblings().removeClass('linkactive');
    $(this).addClass('linkactive');
    var contentType=$(this).attr('tab-id');
    
    if(contentType=='messages'){
        $(".messagesTab").show();
        $(".solutionsTab").hide();
        $(".vocabularyTab").hide();
    }
    else if(contentType=='vocabulary'){
        $(".messagesTab").hide();
        $(".solutionsTab").hide();
        $(".vocabularyTab").show();

    }
    else if(contentType=='solutions'){
        $(".messagesTab").hide();
        $(".solutionsTab").show();
        $(".vocabularyTab").hide();

    }
  });

/*Upload Organization Image*/
$(function() {
    $("#upload-img").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onload = function(e){ // set image data as background of div
              
              $("#img-preview-block").css({'background-image': 'url('+e.target.result +')', "background-size": "cover"});
            }
        }
    });
});

/*Submit Library form*/
$("body").on("click","#submitLibrary",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'>");

    var libType=$("#libraryType").val();
    var title,message;
    if(libType=="vocabulary"){
        title=$("#libTitleInput").val();
        sendLib(title,'');

    }
    else if(libType=="solutions"){
        title=$("#libTitleInput").val();
        message=$("#libMsgInput").val();
        sendLib(title,message);

    }
    else{
      $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-danger'>Please Select library type</div>"); 
      $(".fa-spinner").remove(); 
    }
    function sendLib(title,msg){
        var errorCounter=0;
        var errorLog='';
        if(title.length<4){
            errorCounter+=1;
            errorLog=errorLog + "Phrase or Title too Short <br/>";
        }
        if(msg.length<20 && libType=='solutions'){
            errorCounter+=1;
            errorLog=errorLog + "You can not have a solution(FAQ) less than 20 Characters!";
        }
        if(errorCounter==0){
            $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-success'>Uploading Library...</div>");
            if(libType=="solutions"){
                $("#solList").append("<a href='#!' data-title='"+title+"' id='unknown'  data-content='"+msg+"' class='list-group-item editCon'><i class='fa fa-edit pull-right'></i>"+title+"</a>");
            }
            else{
                $("#vocList").append("<a href='#!' data-title='"+title+"' id='unknown'  class='list-group-item editVoc'><i class='fa fa-edit pull-right'></i>"+title+"</a>");
            }
            $.ajax({
            url:'handlers/postlib.php',
            data:{title:title,message:msg,type:libType},
            type:'POST',
            success:function(res){
                console.log(res);
                if(res==1){
                    $(".fa-spinner").remove(); 
                    $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-success'>"+libType+" Added to Library successfully!</div>");
                    $(".fa-spinner").remove();
                    var staffname=$("#staffname").val();
                    var livefeedmsg="<div class='livefeed'><span class='livename'>"+staffname+"</span> Just added a new library</div>";

                    socket.emit('livefeed',livefeedmsg);
                }
                else{
                    $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-danger'>"+res+"</div>");
                    $(".fa-spinner").remove(); 
                }
            }

            });

        }
        else{
            $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    }

})

/*Set library type to vocabulary*/
$("body").on("click","#vocabularyLibType",function(e){
    e.preventDefault;
    $("#libraryType").val("vocabulary");
    $("#libMsg").hide('slow');
    $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-info'>Vocabulary Selected</div>");
    $("#libTitle").html("Enter Phrase");
    $("#currentInput").val("");
    
})

/*Set library type to solutions*/
$("body").on("click","#solutionsLibType",function(e){
    e.preventDefault;
    $("#libraryType").val("solutions");
    $("#typeSelectorAlert").html("<div class='alert alert-dismissable alert-info'>Solutions Selected</div>");
    $("#libTitle").html("Enter Title");
    $("#libMsg").show('slow');
    $("#currentInput").val("");
    
})

/*Set current Input to Title*/
$("body").on("focus","#libTitleInput",function(){
    $("#currentInput").val("title");
})

/*Set current Input to Message*/
$("body").on("focus","#libMsgInput",function(){
    $("#currentInput").val("message");
})

/*Insert Name wild card*/
$("body").on("click","#insertNameWildcard",function(e){
    e.preventDefault;
    var currentInput=$("#currentInput").val();
    if(currentInput=="title"){
        $("#libTitleInput").val($("#libTitleInput").val() + "+fullname+");
    }
    else if(currentInput=="message") {
        $("#libMsgInput").val($("#libMsgInput").val() + "+fullname+");

    }
})
/*Check if staff wants to change country*/
$('#country').on('change', function() {
  
  $("#curCountry").attr("data-trig","yes");
});

/*Save organization identitys*/
$("#saveIdentity").on('click',function(e){
    e.preventDefault;
    $(".alert").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var name=$("#orgname").val();
    var email=$("#orgemail").val();
    var slogan=$("#orgslogan").val();
    var about=$("#orgabout").val();
    var country=$("#country").val();
    var category=$("#category").val();
    var countryChangeTriggered= $("#curCountry").attr("data-trig");
    var curCountry=$("#curCountry").attr("data-val");

    var namelen=(name.trim()).length;
    var emaillen=(email.trim()).length;
    var sloganlen=(slogan.trim()).length;
    var aboutlen=(about.trim()).length;

    var errorCounter=0;
    var errorLog='';
    if(namelen<3){
        errorLog=errorLog+ "organization name too short <br/>";
        errorCounter+=1;
    }
    if(aboutlen<15){
        errorLog=errorLog+ "organization description too short <br/>";
        errorCounter+=1;
    }
    if(emaillen<5 || email.indexOf('@')==-1 || email.indexOf('.')==-1){
        errorLog=errorLog + " email is incorrect <br/>";
        errorCounter+=1;
    }
    if(country=='' || country.length<2){
        errorLog=errorLog + " Unknown Country <br/>";
        errorCounter+=1;
    }
    if(category==''){
        errorLog=errorLog + " Unknown Category <br/>";
        errorCounter+=1;
    }
    var fd=new FormData();
    fd.append('name',name);
    fd.append('slogan',slogan);
    fd.append('about',about);
    fd.append('email',email);
    if(countryChangeTriggered=="yes"){
       fd.append('country',country); 
    }
    else{
       fd.append('country',curCountry); 
    }
    
    fd.append('category',category);
    var file = $("#avatar")[0].files[0];
    var filename=$("#avatar").val();
    if(filename.length>3){

        $("#img-preview-block").css('background-image',"url('"+filename+"')");
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            errorLog=errorLog + " Please upload images only <br/>";
            errorCounter+=1;
        }
        if(size>80000000){
            errorLog=errorLog + " File too large. 8MB max <br/>";
            errorCounter+=1;
        }
        fd.append('file',file);

    }

    if(errorCounter==0){
    

    var request = $.ajax({
    url: "handlers/setorginfo.php",
    type: "POST",
    data:fd,
    dataType: "html",
    cache: false,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("Your changes have been made!",1,'success',null);
           
            var livefeedmsg="<div class='livefeed'><span class='livename'>Admin</span> Just updated page information</div>";

            socket.emit('livefeed',livefeedmsg);
            
        }
        else{
            MsgReturn=Alert_Div(html,1,'warning',null);

        }
    $("#results").html(MsgReturn);
    $(".fa-spinner").remove();
    
    
  }
  
});}
else{
 $("#results").html(Alert_Div(errorLog,1,"warning",null));
 $(".fa-spinner").remove();

}
});


/*Send Solution to edit*/
$("body").on("click",".editCon",function(){
    var title=$(this).attr("data-title");
    var content=$(this).attr("data-content");
    var id=$(this).attr("id");
    $("#solTitle").val(title);
    $("#solMsg").val(content);
    $(".saveSol").attr("id",id);
    $(".delSol").attr("id",id);
});

/*Send vocabulary to edit*/
$("body").on("click",".editVoc",function(){
    var title=$(this).attr("data-title");
    var id=$(this).attr("id");
    $("#VocMsg").val(title);
    $(".saveVoc").attr("id",id);
    $(".delVoc").attr("id",id);
    
});

/*Show add new photo form*/
$("body").on("click",".addnewphoto",function(){
    var visibility=$(this).attr("id");
    $(".alert").remove();
    if(visibility==0){
        $("#addAlbum").show('slow');
        $(this).attr("id",1);
        $(this).html("Hide Form");
    }
    else{
        $("#addAlbum").hide('slow');
        $(this).attr("id",0);
        $(this).html("Add new photo");
    }
    
});
/*Show add new staff form*/
$("body").on("click",".addnewstaff",function(){
    var visibility=$(this).attr("id");
    $(".alert").remove();
    if(visibility==0){
        $("#addStaff").show('slow');
        $(this).attr("id",1);
        $(this).html("Hide Form");
    }
    else{
        $("#addStaff").hide('slow');
        $(this).attr("id",0);
        $(this).html("Add Staff");
    }
    
});

/*Edit Vocabulary*/
$("body").on("click",".saveVoc",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");

    var msg=$("#VocMsg").val();
    var id=$(this).attr("id");
    var errorCounter=0;
    var errorLog='';
    if(id==''){
        errorCounter+=1;
        errorLog=errorLog + "This is an invalid Item,Select again <br/>";
    }
    if(msg.length<4 ){
        errorCounter+=1;
        errorLog=errorLog + "Phrase too short <br/>";
    }
    if(errorCounter==0){
        $("#vocResults").html("<div class='alert alert-dismissable alert-success'>Saving changes...</div>");
        $.ajax({
        url:'handlers/editlib.php',
        data:{title:msg,id:id,message:''},
        type:'POST',
        success:function(res){
            console.log(res);
            if(res==1){
                $(".fa-spinner").remove(); 
                $("#vocResults").html("<div class='alert alert-dismissable alert-success'> Vocabulary Has been Editted successfully!</div>");
                $("#"+id).attr("data-title",msg);
                $("#"+id).html(msg);
                var staffname=$("#staffname").val();
                var livefeedmsg="<div class='livefeed'><span class='livename'>"+staffname+"</span> Just Editted a vocabulary</div>";

                socket.emit('livefeed',livefeedmsg);
                
            }
            else{
                $("#vocResults").html("<div class='alert alert-dismissable alert-danger'>"+res+"</div>");
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            $("#vocResults").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    

})

/*Edit Solutions*/
$("body").on("click",".saveSol",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");

    var msg=$("#solMsg").val();
    var title=$("#solTitle").val();
    var id=$(this).attr("id");
    var errorCounter=0;
    var errorLog='';
    if(id==''){
        errorCounter+=1;
        errorLog=errorLog + "This is an invalid Item,Select again <br/>";
    }
    if(msg.length<4 ){
        errorCounter+=1;
        errorLog=errorLog + "Solution (FAQ) can not be less than 10 characters <br/>";
    }
    if(title.length<4){
        errorCounter+=1;
        errorLog=errorLog + "Title too short <br/>";
    }
    if(errorCounter==0){
        $("#solResults").html("<div class='alert alert-dismissable alert-success'>Saving changes...</div>");
        $.ajax({
        url:'handlers/editlib.php',
        data:{title:title,id:id,message:msg},
        type:'POST',
        success:function(res){
            console.log(res);
            if(res==1){
                $(".fa-spinner").remove(); 
                $("#solResults").html("<div class='alert alert-dismissable alert-success'> Solution Has been Editted successfully!</div>");
                $("#"+id).attr("data-title",title);
                $("#"+id).attr("data-content",msg);
                $("#"+id).html(title);
                var staffname=$("#staffname").val();
                var livefeedmsg="<div class='livefeed'><span class='livename'>"+staffname+"</span> Just Editted a Solution</div>";

                socket.emit('livefeed',livefeedmsg);
                
            }
            else{
                $("#solResults").html("<div class='alert alert-dismissable alert-danger'>"+res+"</div>");
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            $("#solResults").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    

});

/*Global image indicator*/
$("body").on("change","#avatar",function(){
    var file = $("#avatar")[0].files[0];
    var filename=$("#avatar").val();
    if(filename.length>3){

        $("#img-preview-block").css('background-color',"rgb(44, 199, 6)");
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            $("#img-preview-block").css('background-color',"rgb(199, 6, 6)");
        }
        if(size>80000000){
            $("#img-preview-block").css('background-color',"rgb(199, 6, 6)");
        }
    }
})




/*Upload new photo to album*/
$("body").on('click',".uploadAlbum",function(e){
    e.preventDefault;
    $(".alert").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var name=$("#title").val();
    var namelen=(name.trim()).length;
    var errorCounter=0;
    var errorLog='';
    if(namelen<3){
        errorLog=errorLog+ "Title name too short <br/>";
        errorCounter+=1;
    }
    
    var fd=new FormData();
    fd.append('name',name);
   
    var file = $("#avatar")[0].files[0];
    var filename=$("#avatar").val();
    if(filename.length>3){

        
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            errorLog=errorLog + " Please upload images only <br/>";
            errorCounter+=1;
        }
        if(size>80000000){
            errorLog=errorLog + " File too large. 8MB max <br/>";
            errorCounter+=1;
        }
        fd.append('file',file);

    }

    if(errorCounter==0){
    

    var request = $.ajax({
    url: "handlers/setnewalbum.php",
    type: "POST",
    data:fd,
    dataType: "html",
    cache: false,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("You have uploaded a new image successfully",1,'success',null);
            $.post('handlers/fetchimage.php',function(res){
                $(".gallery").append(res);
            })
            var staffname=$("#staffname").val();
            var livefeedmsg="<div class='livefeed'><span class='livename'>"+staffname+"</span> Just Added a photo to album</div>";

            socket.emit('livefeed',livefeedmsg);
            
        }
        else{
            MsgReturn=Alert_Div(html,1,'warning',null);

        }
    $("#results").html(MsgReturn);
    $(".fa-spinner").remove();
    
    
  }
  
});}
else{
 $("#results").html(Alert_Div(errorLog,1,"warning",null));
 $(".fa-spinner").remove();
 
}
});

/*Search members */
$("#memberInput").on("keyup",function(e){
    e.preventDefault;
    
    var memberinfo=$(this).val();
    var trimmedText=memberinfo.trim();
    if(memberinfo !=="" && trimmedText.length >= 3){
    var request = $.ajax({
        url: "handlers/membersearch.php",
        type: "POST",
        data: {token:memberinfo},
        dataType: "html",
        cache: false,
        success: function(html){
        var MsgReturn;
        if(html !==0){
            MsgReturn=html;
        }
        else{
            MsgReturn=Alert_Div("Member could not be found",1,'warning',null);
            
        }
        
        $("#tabledresults").removeClass("hidden");
        $("#membersResults").html(MsgReturn);
        }
        
    });
    }
    else{
        $("#membersDiv").removeClass("hidden");
        $("#tabledresults").addClass("hidden");
        
    }
});

/*Add New Staff*/
$("body").on("click",".makeStaff",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");

    
    var id=$(this).attr("id");
    var name=$(this).attr('data-name');
    var errorCounter=0;
    var errorLog='';
    if(id==''){
        errorCounter+=1;
        errorLog=errorLog + "This is an invalid Item,Select again <br/>";
    }
    
    if(errorCounter==0){
        $("#results").html("<div class='alert alert-dismissable alert-success'>Adding "+name+" as staff...</div>");
        $.ajax({
        url:'handlers/addstaff.php',
        data:{id:id},
        type:'POST',
        success:function(res){
            
            if(res==1){
                $(".fa-spinner").remove(); 
                
                $("#results").html("<div class='alert alert-dismissable alert-success'>"+name+" has been added as staff successfully!</div>");
                $("#"+id).removeClass("btn-primary");
                $("#"+id).addClass("btn-success");
                $("#"+id).removeClass("makeStaff");
                $("#"+id).html("Now Staff");
                
                var livefeedmsg="<div class='livefeed'><span class='livename'>"+name+"</span> Just became a staff</div>";

                socket.emit('livefeed',livefeedmsg);
                
            }
            else{
                $("#results").html("<div class='alert alert-dismissable alert-danger'>"+res+"</div>");
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            $("#results").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    

})

/*Delete Staff*/
$("body").on("click",".deletestaff",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");

    
    var id=$(this).attr("id");
    var name=$(this).attr('data-name');
    var errorCounter=0;
    var errorLog='';
    if(id==''){
        errorCounter+=1;
        errorLog=errorLog + "This is an invalid Item,Select again <br/>";
    }
    
    if(errorCounter==0){
        $("#results2").html("<div class='alert alert-dismissable alert-success'>Removing "+name+" as staff...</div>");
        $.ajax({
        url:'handlers/deletestaff.php',
        data:{id:id},
        type:'POST',
        success:function(res){
            
            if(res==1){
                $(".fa-spinner").remove(); 
                
                $("#results2").html("<div class='alert alert-dismissable alert-success'>"+name+" has been removed as staff successfully!</div>");
                $("#staff"+id).remove();
                
                var livefeedmsg="<div class='livefeed'><span class='livename'>"+name+"</span> has been suspended as staff</div>";

            socket.emit('livefeed',livefeedmsg);
                
                
            }
            else{
                $("#results2").html("<div class='alert alert-dismissable alert-danger'>"+res+"</div>");
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            $("#results2").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    

})
/*Update Post For Staff*/
$("body").on("click",".UpdatePost",function(e){
    e.preventDefault;

    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");

    
    var id=$("#roomId").val();
    var post=$("#postMsg").val();
    var postlen=post.trim();
    var errorCounter=0;
    var errorLog='';
    if(postlen.length<10){
        errorCounter+=1;
        errorLog=errorLog + "Your Post is too short, Compose something constructive<br/>";
    }
    
    if(errorCounter==0){
        $("#results").html("<div class='alert alert-dismissable alert-success'>Updating Post</div>");
        $.ajax({
        url:'handlers/insertpost.php',
        data:{post:post},
        type:'POST',
        success:function(res){
            
            if(res!==0 && res!==2){
                $(".fa-spinner").remove(); 
                
                $("#results").html("<div class='alert alert-dismissable alert-success'>Post has been updated</div>");
                $("#staff"+id).remove();
                var staffname=$("#staffname").val();
                var livefeedmsg="<div class='livefeed'><span class='livename'>"+staffname+"</span>Just added a new post</div>";
                socket.emit('livefeed',livefeedmsg);
                $("#postMsg").val('');
                $(".timeline").prepend(res);
                
                
            }
            else if(res==2){
                $("#results").html("How did you manage to send in a short post again? Hmmm!");
                $(".fa-spinner").remove(); 
            }
            else if(res==0){
                $("#results").html("<div class='alert alert-dismissable alert-danger'>Could not make post, Please try again</div>");
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            $("#results").html("<div class='alert alert-dismissable alert-danger'>"+errorLog+"</div>");
            $(".fa-spinner").remove(); 
        }
        
    

})

/*Show comments on rooms*/

$("body").on("click",".showcomments",function(e){
    e.preventDefault;
    var id=$(this).attr("id");
    var stateofbtn=$(this).attr("data-state");
    if(stateofbtn==0){
        $("#comments"+id).removeClass("hidden");
        $(this).attr("data-state",1);
    }
    else{
        $("#comments"+id).addClass("hidden");
        $(this).attr("data-state",0);
    }
})


/*Load conversation*/
$("body").on("click",".loadMsgConvo",function(e){
    e.preventDefault;
    
    var msgid=$(this).attr("id");
    var userid=$(this).attr("data-userid");
    var msgfullname=$(this).attr("data-name");
    var msgimage=$(this).attr("data-src");
    var useddata=$(this).attr("data-used");
    var orgid=$("#SocketOrgId").val();
    var msgidlen=msgid.trim();
    var errorCounter=0;
    var errorLog='';
    if(msgidlen.length<10){
        errorCounter+=1;
        errorLog=errorLog + "This message is invalid<br/>";
    }
    socket.on(orgid,function(res){
            console.log(res.message);
            var message=res.message;
            if(message=="newmessage"){
                $.post("handlers/refreshmessages.php",{type:"org"},function(res){
                    $("#inboxposts").html(res);
                })
            }
        })
    
    if(errorCounter==0 && useddata==0){
        $("#SocketMsgId").val(msgid);
        $("#SocketUserId").val(userid);
        $("#imgpalace").html("<img src="+msgimage+" />");
        $("#oto").html("<h1>"+msgfullname+"</h1");
        $("#userfullname").val(msgfullname);
        $(this).parent().removeClass("msg-indicator-new").addClass("msg-indicator-unattended");
        $(".loadMsgConvo").attr("data-used",0);
        $(this).attr("data-used",1);
        var iheomume=0;
        socket.removeAllListeners(globsockuser);
        socket.on(msgid,function(res){
            globsockuser=msgid;
            console.log(res.message);
            var message=res.message;
            
            if(message=="istyping"  && res.userid==userid){
                if(iheomume==0){
                $('<div class="message loading new"><span></span></div>').appendTo($('.mCSB_container'));
                updateScrollbar();
                iheomume=1;

            }
            }
            else if(message=="stoppedtyping" && res.userid==userid){
                $('.message.loading').remove();
                updateScrollbar();
                iheomume=0;
            }
            else if(message=="newmessage"  && res.userid==userid){
                var msg=res.data;
                 $('.message.loading').remove();
                 $('<div class="message new">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
                setDate();
                updateScrollbar();

            }
        });
       // $("#results").html("<div class='alert alert-dismissable alert-success'>Updating Post</div>");
        $.ajax({
        url:'handlers/getconvo.php',
        data:{msgid:msgid},
        type:'POST',
        success:function(res){
            
            if(res!==0 ){
               $('.mCSB_container').html(res);
               
                
                
            }
            else{
               
                $(".fa-spinner").remove(); 
            }
        }

        });

        }
        else{
            
            $(".fa-spinner").remove(); 
        }


})
//Edit Solution
$("body").on("click",".editSolu",function(e){
        e.preventDefault;
        $(".popover").remove();
        var content=$(this).attr("data-content");
        var fullname=$("#userfullname").val();
        var thestuff="<div class='popover'><textarea class='textarena' cols='60' rows='8'>"+content+"</textarea><div class='btn-group btn-group-justified'><a href='#!' class='btn btn-success sendSolu' >Send</a><a href='#!' class='btn btn-danger cancelSolu'>Cancel</a></div></div>";
        $("body").append(thestuff);


});

//cancel Solution Edit
$("body").on("click",".cancelSolu",function(e){
    e.preventDefault;
    $(".popover").remove();
})



//Like Post
$("body").on("click",".likepost",function(e){
        e.preventDefault;
        var postid=$(this).attr("id");
        var likecount=parseInt($(this).attr("data-count"));
        var likestate=parseInt($(this).attr("data-liked"));
        if(likestate==1){
            $(this).css("color","#337ab7");
            $(this).html("<i class='glyphicon glyphicon-heart'></i> "+(likecount-1)+" likes");
            $(this).attr("data-count",(likecount-1));
            $(this).attr("data-liked",0);
            $.post("raaksaapi/unlikepost.php",{postid:postid},function(s){console.log(s)});
            
        }
        else{
            $(this).css("color","#f20000");
            $(this).html("<i class='glyphicon glyphicon-heart'></i> "+(likecount+1)+" likes");
            $(this).attr("data-count",(likecount+1));
            $(this).attr("data-liked",1);
            $.post("raaksaapi/likepost.php",{postid:postid},function(s){console.log(s)});
            
        }


});

//Delete Post
$("body").on("click",".deletepost",function(e){
    e.preventDefault;
    var postid=$(this).attr("id");
    $("#post"+postid).remove();
    $.post("raaksaapi/deletepost.php",{postid:postid},function(s){console.log(s)});
})

//Respond To Post
$("body").on("keyup",".respondPost",function(e){
    var response=$(this).val();
    var postid=$(this).attr("id");


    if(e.which == 13) {

        if(response.length>5){
            $.post("handlers/respondpost.php",{postid:postid,res:response},function(s){
                    if(s==0){
                        alert("Sorry!, Can not process response at this moment");
                    }
                    else if(s==1){
                        alert("How did you manage to send in a short response again? Hmmm!");
                    }
                    else if(s==2){
                        alert("Your Organization can not respond to a post which has been already responded to by another organization");
                    }
                    else{
                        $("#postbody"+postid).append(s);
                    }
            }); 
            $(this).val("");
        }
        else{
            alert("Your response can not be less than 5 characters");
        }
        
    }

})


/*Change the password*/
$(".changePword").on('click',function(e){
    e.preventDefault;
    $(".alert").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var oldpassword=$("#oldpword").val();
    var newpassword=$("#newpword").val();
    var renewpassword=$("#renewpword").val();
   

    var oldpwrdlen=(oldpassword.trim()).length;
    var newpwrdlen=(newpassword.trim()).length;
    var renewpwrdlen=(renewpassword.trim()).length;
    

    var errorCounter=0;
    var errorLog='';
    if(newpwrdlen<6 || renewpwrdlen<6){
        errorLog=errorLog+ "New password is too short <br/>";
        errorCounter+=1;
    }
    if(oldpwrdlen<6){
        errorLog=errorLog+ "Old password is wrong <br/>";
        errorCounter+=1;
    }
    if(newpassword!==renewpassword){
        errorLog=errorLog + " New passwords do not match <br/>";
        errorCounter+=1;
    }
   
    var fd=new FormData();
    fd.append('old',oldpassword);
    fd.append('new',newpassword);
   
   

    if(errorCounter==0){
    

    var request = $.ajax({
    url: "handlers/changepword.php",
    type: "POST",
    data:fd,
    dataType: "html",
    cache: false,
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("Password changed successfully!",1,'success',null);
           
            
        }
        else{
            MsgReturn=Alert_Div(html,1,'warning',null);

        }
    $("#results2").html(MsgReturn);
    $(".fa-spinner").remove();
    
    
  }
  
});}
else{
 $("#results2").html(Alert_Div(errorLog,1,"warning",null));
 $(".fa-spinner").remove();

}
});

//Load More Data
$("body").on("click",".loadmore",function(e)
{
    e.preventDefault;

    var counter = $(this).attr("id");
    var type = $(this).attr("data-type");
    var data = {};
    var errorMessage = '';
    var errorCounter = 0;
    if(type.length < 2)
    {
        errorMessage+="Invalid Page";
        errorCounter+=1;
    }
    if(counter.length < 0 || counter <20)
    {
        errorMessage+="Invalid Counter";
        errorCounter+=1;
    }
    if(type == "subscribers.php")
    {
        data = {count:counter};
    }

    if(errorCounter == 0)
    {
        $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
        
        var url_link = type;
        var request = $.ajax({
        url: "handlers/"+url_link,
        type: "POST",
        data:data,
        dataType: "html",
        cache: false,
        
        success: function(html){
            var MsgReturn;
            if(html!==0){
               $(".loadmore").remove(); 
               $("#sub_list").append(html);
               
                
            }
            else{
                MsgReturn=Alert_Div(html,1,'warning',null);
                $("#sub_list").append(MsgReturn);

            }
        }
        });
        
    }
})

//Open statistics For Subscribers
$("body").on("click",".sub_stats",function(e)
{
    var state = $(this).attr('id');
    if(state == 0)
    {
        $('#sub_stat').show();
        $("#sub_list").hide();
        $(this).attr("id",1);
        $(this).html("Show Subscribers");
    }
    else
    {
        $('#sub_stat').hide();
        $("#sub_list").show();
        $(this).attr("id",0);
        $(this).html("Show Statistics");
    }
});

//Toggle Media List And Add New Media
$("body").on("click",".toggle_media_list",function(e)
{
    var state = $(this).attr('id');
    var type = $(this).attr("data-type");
  
    if(state == 0)
    {
        $('#add_media').show();
        $("#media_list").hide();
        $(this).attr("id",1);
        $(this).html("Show "+type);
    }
    else
    {
        $('#add_media').hide();
        $("#media_list").show();
        $(this).attr("id",0);
        $(this).html("Add "+type);
    }
});

//Toggle Media List And Add New Media
$("body").on("click",".gini",function(e)
{
    alert("uwa!");
});

//Open Profile Pop Up
$("body").on("click",".open_profile_popup",function(e)
{
    e.preventDefault;
    $(".profile_pop").remove();
    var hash = $(this).attr("id");
    var elem = $(this);
    var errorCounter = 0;
    var errorMessage = "";

    if(hash.length < 5)
    {
        errorCounter+= 1;
        errorMessage+= "Invalid User Token <br/>";

    }

    if(errorCounter == 0)
    {
        var data = {hash:hash};
        var request = $.ajax({
        url: "handlers/profile.php",
        type: "POST",
        data:data,
        dataType: "html",
        cache: false,        
        success: function(html){
            var MsgReturn;
         
            if(html!==0){
               
               elem.parent().append(html);       
                
            }
            else
            {
                MsgReturn=Alert_Div(html,1,'warning',null);
                //$("#sub_list").append(MsgReturn);

            }
        }
        });
    }
    else
    {
        alert(errorMessage);
    }

});

//Close Profile Pop up
$("body").on("click",".close_prof_pop",function(e)
{
    e.preventDefault;
    $(".profile_pop").hide();
    $("#prof_tin").remove();
})

//Media Upload
$("#upload_media").on('click',function(e){
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title=$("#file_title").val();
    var description=$("#file_description").val();
    var media_type=$("#media_type").val();

    var titlelen=(title.trim()).length;
    var descriptionlen=(description.trim()).length;
    var typelen=(media_type.trim()).length;
   
    var errorCounter=0;
    var errorLog='';
    if(titlelen<3){
        errorLog=errorLog+ "Title Is Too Short <br/>";
        errorCounter+=1;
    }
    if(typelen<4){
        errorLog=errorLog+ " Invalid media Type <br/>";
        errorCounter+=1;
    }
    
    var fd=new FormData();
    fd.append('title',title);
    fd.append('description',description);
    fd.append('type',media_type);
    var file = $("#filer_input")[0].files[0];
    var filename=$("#filer_input").val();
    if(filename.length>3){

        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        /*if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            errorLog=errorLog + " Please upload images only <br/>";
            errorCounter+=1;
        }*/
        if(size>8000000000){
            errorLog=errorLog + " File too large. 80MB max <br/>";
            errorCounter+=1;
        }
        fd.append('file',file);

    }

    if(errorCounter==0){
    

        var request = $.ajax({
        url: "handlers/media_upload.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html==1){

                MsgReturn=Alert_Div("Your upload was successfull!",1,'success',null);
                $(".jFiler-item").remove();
                $("#file_title").val("");
                $("#file_description").val("");
                $("#filer_input").val("");
               
            }
            else{

                MsgReturn=Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//Show Event
$("body").on("click",".show_event",function(e)
{
    e.preventDefault;
    $(this).append("<i class='fa fa-spinner fa-spin' aria-hidden='true'></i>");
    var id = $(this).attr("id");
    var cmd = "get";
    var fd = new FormData;
    fd.append('id',id);
    fd.append('cmd',cmd);
    if(id.length > 5)
    {
        var request = $.ajax({
        url: "handlers/events.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html !== 0){

               MsgReturn = (html);
               
            }
            else
            {

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $(".viewer").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
});

//Add new Event
$("#add_event").on('click',function(e){
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title = $("#title").val();
    var description = $("#description").val();
    var media_type = $("#media_type").val();
    var contact = $("#contact").val();
    var address = $("#address").val();
    var datetime = $("#datetime").val();

    var titlelen = (title.trim()).length;
    var descriptionlen = (description.trim()).length;
    var typelen = (media_type.trim()).length;
    var contact_len = (contact.trim()).length;
    var address_len = (address.trim()).length;
    var datetime_len = (datetime.trim()).length;
   
    var errorCounter = 0;
    var errorLog = '';
    if(titlelen < 3)
    {
        errorLog = errorLog+ "Title Is Too Short <br/>";
        errorCounter+= 1;
    }
    if(typelen < 4){
        errorLog = errorLog+ " Invalid media Type <br/>";
        errorCounter+=1;
    }
    if(contact_len < 4)
    {
        errorLog+= "You have to enter a contact Information (e.g. phone,email etc...) <br/>";
        errorCounter+=1;
    }
    if(address_len < 4)
    {
        errorLog+= "Invalid Address <br/>";
        errorCounter+= 1;
    }
    if(datetime_len < 4)
    {
        errorLog+= "Invalid Date , Select (year,month,day) <br/>";
        errorCounter+= 1;
    }

    
    var fd = new FormData();
    fd.append('title',title);
    fd.append('description',description);
    fd.append('contact',contact);
    fd.append('address',address);
    fd.append('date',datetime);
    fd.append('type',media_type);
    fd.append('cmd','put');
    fd.append('id','null');
    var file = $("#filer_input")[0].files[0];
    var filename = $("#filer_input").val();
    if(filename.length>3){

        var imagefile = file.type;
        var match = ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        /*if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            errorLog=errorLog + " Please upload images only <br/>";
            errorCounter+=1;
        }*/
        if(size>8000000000){
            errorLog=errorLog + " File too large. 80MB max <br/>";
            errorCounter+=1;
        }
        fd.append('file',file);

    }
    else
    {
        errorLog = errorLog + " Please Select a file <br/>";
        errorCounter+= 1;
    }

    if(errorCounter == 0){
    

        var request = $.ajax({
        url: "handlers/events.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your event was successfully Added!",1,'success',null);
                $(".jFiler-item").remove();
                $("#title").val("");
                $("#description").val("");
                $("#contact").val("");
                $("#address").val("");
                $("#datetime").val("");
                $("#filer_input").val("");
               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//Toggle Edit Form
$("body").on("click",".edit_event_request",function(e)
{
    e.preventDefault;
    var title = $("#e_t").html();
    var description = $("#e_d").html();
    var datetime = $("#e_d_t").html();
    var contact = $("#e_c").html();
    var address = $("#e_v").html();
    var id = $(this).attr("id");

    $('#add_media').show();
    $("#media_list").hide();
    $(".toggle_media_list").attr("id",1);
    $(".toggle_media_list").html("Show Event");

    $("#title").val(title);
    $("#description").val(description);
    $("#contact").val(contact);
    $("#datetime").val(datetime);
    $("#address").val(address);

    $("#add_event").hide();
    $(".edit_event").show();
    $(".edit_event").attr("id",id);


});

//Add new Event
$(".edit_event").on('click',function(e){
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title = $("#title").val();
    var description = $("#description").val();
    var media_type = $("#media_type").val();
    var contact = $("#contact").val();
    var address = $("#address").val();
    var datetime = $("#datetime").val();
    var id = $(this).attr("id");

    var titlelen = (title.trim()).length;
    var descriptionlen = (description.trim()).length;
    var typelen = (media_type.trim()).length;
    var contact_len = (contact.trim()).length;
    var address_len = (address.trim()).length;
    var datetime_len = (datetime.trim()).length;
    var id_len = (id.trim()).length;
   
    var errorCounter = 0;
    var errorLog = '';
    if(titlelen < 3)
    {
        errorLog = errorLog+ "Title Is Too Short <br/>";
        errorCounter+= 1;
    }
    if(typelen < 4){
        errorLog = errorLog+ " Invalid media Type <br/>";
        errorCounter+=1;
    }
    if(contact_len < 4)
    {
        errorLog+= "You have to enter a contact Information (e.g. phone,email etc...) <br/>";
        errorCounter+=1;
    }
    if(address_len < 4)
    {
        errorLog+= "Invalid Address <br/>";
        errorCounter+= 1;
    }
    if(datetime_len < 4)
    {
        errorLog+= "Invalid Date , Select (year,month,day) <br/>";
        errorCounter+= 1;
    }
    if(id_len <8)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token! Goto Back to Events and Start Again! <br/>";
    }

    
    var fd = new FormData();
    fd.append('title',title);
    fd.append('description',description);
    fd.append('contact',contact);
    fd.append('address',address);
    fd.append('date',datetime);    
    fd.append('cmd','edit');
    fd.append('id',id);
    var file = $("#filer_input")[0].files[0];
    var filename = $("#filer_input").val();
    if(filename.length>3){

        var imagefile = file.type;
        var match = ["image/jpeg","image/png","image/jpg"];
        var size  = file.size;
        /*if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            errorLog=errorLog + " Please upload images only <br/>";
            errorCounter+=1;
        }*/
        if(size>8000000000){
            errorLog=errorLog + " File too large. 80MB max <br/>";
            errorCounter+=1;
        }
        fd.append('file',file);
        fd.append('type',media_type);

    }
    else
    {
        fd.append('type',"null");
    }

    if(errorCounter == 0){
    

        var request = $.ajax({
        url: "handlers/events.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your event was successfully Editted!",1,'success',null);
                $(".jFiler-item").remove();
                $("#title").val("");
                $("#description").val("");
                $("#contact").val("");
                $("#address").val("");
                $("#datetime").val("");
                $("#filer_input").val("");
                $(".edit_event").hide();
                $("#add_event").show();
               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//Delete Event
$("body").on("click",".delete_event",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");

    var id_len = (id.trim()).length;
    var errorLog = "";
    var errorCounter = 0;

    if(id_len < 5)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token <br/>";
    }
    if(errorCounter == 0){  
        sure = confirm("Are you sure you want to get Rid of this Event :( ?")
        if(sure)
        {
            var fd = new FormData();
            fd.append('id',id);
            fd.append('cmd','delete');  

            var request = $.ajax({
            url: "handlers/events.php",
            type: "POST",
            data:fd,
            dataType: "html",
            cache: false,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(html)
            {
                var MsgReturn;
                if(html == 1){

                    MsgReturn = Alert_Div("Your Event was successfully Deleted!",1,'success',null);
                    $(".viewer").html("");
                   
                }
                else{

                    MsgReturn = Alert_Div(html,1,'warning',null);

                }
                $("#results").html(MsgReturn);
                $(".fa-spinner").remove();
            
            
            }
          
            });
        }
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//View And Edit ANNOUNCEMENT
$("body").on("click",".edit_notice",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var description = $(this).attr("data-desc");
    var title = $(this).attr("data-title");
    var duration = $(this).attr("data-duration");
    var datetime = $(this).attr("data-dt");
    var expiry = $(this).attr("data-exp");
    var views = $(this).attr("data-views");
    var summary = "This Announcment was posted on "+datetime+" and would stay live for "+duration+" days,Expiring on the "+expiry+".<br/> Where as, It has gained "+views+" views which you can <a href='#!' class='show_reactors' id='"+id+"' data-type='viewers'>view here</a>.";

    $('#add_media').show();
    $("#media_list").hide();
    $(".toggle_media_list").attr("id",1);
    $(".toggle_media_list").html("Show Announcement");

    $("#title").val(title);
    $("#description").val(description);
    $("#duration").val(duration);
    $("#summary").html(summary);
    $("#naration").show();

    $("#add_announcement").hide();
    $(".edits").show();
    $(".edits").attr("id",id);

});

//Add new Announcement
$("body").on("click","#add_announcement",function(e)
{
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title = $("#title").val();
    var description = $("#description").val();
    var duration = $("#duration").val();
    

    var titlelen = (title.trim()).length;
    var descriptionlen = (description.trim()).length;
    var durationlen = (duration.trim()).length;
    
   
    var errorCounter = 0;
    var errorLog = '';
    if(titlelen < 3)
    {
        errorLog = errorLog+ "Title Is Too Short <br/>";
        errorCounter+= 1;
    }
    if(duration < 1){
        errorLog = errorLog+ " Invalid Duration <br/>";
        errorCounter+=1;
    }
    if(descriptionlen < 1){
        errorLog = errorLog+ " Description is too Short <br/>";
        errorCounter+=1;
    }
    
    
    var fd = new FormData();
    fd.append('title',title);
    fd.append('description',description);
    fd.append('duration',duration);
    fd.append('id','null');
    fd.append('cmd','put');
    
    

    if(errorCounter == 0){

        var request = $.ajax({
        url: "handlers/announcements.php",
        type: "POST",
        data:fd,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your Announcment was successfully Added!",1,'success',null);
                $("#title").val("");
                $("#description").val("");
                $("#duration").val("");
               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
})

//Edit announcement
$("body").on("click",".edit_announcement",function(e)
{
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title = $("#title").val();
    var description = $("#description").val();
    var duration = $("#duration").val();
    var id = $(this).attr("id");
    

    var titlelen = (title.trim()).length;
    var descriptionlen = (description.trim()).length;
    var durationlen = (duration.trim()).length;
    var id_len = (id.trim()).length;
    
   
    var errorCounter = 0;
    var errorLog = '';
    if(titlelen < 3)
    {
        errorLog = errorLog+ "Title Is Too Short <br/>";
        errorCounter+= 1;
    }
    if(durationlen < 1){
        errorLog = errorLog+ " Invalid Duration <br/>";
        errorCounter+=1;
    }
    if(descriptionlen < 1){
        errorLog = errorLog+ " Description is too Short <br/>";
        errorCounter+=1;
    }    
    if(id_len < 5)
    {
        errorLog = errorLog+ " Invalid Token <br/>";
        errorCounter+=1;       
    }
    
    
    var fd = new FormData();
    fd.append('title',title);
    fd.append('description',description);
    fd.append('duration',duration);
    fd.append('id',id);
    fd.append('cmd','edit');
    
    

    if(errorCounter == 0){

        var request = $.ajax({
        url: "handlers/announcements.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your Announcment was successfully Editted!",1,'success',null);
                $(".jFiler-item").remove();
                $("#title").val("");
                $("#description").val("");
                $("#duration").val("");
                $(".edits").hide();

                $("#summary").val("");
                $("#naration").hide();
                $("#add_announcement").show();
               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
})

//Delete Announcment
$("body").on("click",".delete_announcement",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");

    var id_len = (id.trim()).length;
    var errorLog = "";
    var errorCounter = 0;

    if(id_len < 5)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token <br/>";
    }
    if(errorCounter == 0){  
        sure = confirm("Do you really REALLY want to delete this Announcment?")
        if(sure)
        {
            var fd = new FormData();
            fd.append('id',id);
            fd.append('cmd','delete');  

            var request = $.ajax({
            url: "handlers/announcements.php",
            type: "POST",
            data:fd,
            dataType: "html",
            cache: false,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(html)
            {
                var MsgReturn;
                if(html == 1){

                    MsgReturn = Alert_Div("Your Announcment was successfully Deleted!",1,'success',null);
                    $(".jFiler-item").remove();
                    $("#title").val("");
                    $("#description").val("");
                    $("#duration").val("");

                    $("#summary").val("");
                    $("#naration").hide();
                    $(".edits").hide();
                    $("#"+id).remove();
                    $("#add_announcement").show();
                   
                }
                else{

                    MsgReturn = Alert_Div(html,1,'warning',null);

                }
                $("#results").html(MsgReturn);
                $(".fa-spinner").remove();
            
            
            }
          
            });
        }
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//RESET Announcment
$("body").on("click",".reset_announcement",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");

    var id_len = (id.trim()).length;
    var errorLog = "";
    var errorCounter = 0;

    if(id_len < 5)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token <br/>";
    }
    if(errorCounter == 0){  

        var fd = new FormData();
        fd.append('id',id);
        fd.append('cmd','reset');  

        var request = $.ajax({
        url: "handlers/announcements.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your Announcment was successfully Reset. It Would Restart it's duration and go live from today!",1,'success',null);
               
                $("#title").val("");
                $("#description").val("");
                $("#duration").val("");

                $("#summary").val("");
                $("#naration").hide();
                $(".edits").hide();
                $("#add_announcement").show();

               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//Delete PDF
$("body").on("click",".delete_pdf",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");

    var id_len = (id.trim()).length;
    var errorLog = "";
    var errorCounter = 0;

    if(id_len < 1)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token <br/>";
    }
    if(errorCounter == 0){  
        sure = confirm("Are you sure you want to delete this PDF :( ?")
        if(sure)
        {
            var fd = new FormData();
            fd.append('id',id);
            fd.append('cmd','delete');  

            var request = $.ajax({
            url: "handlers/pdfpage.php",
            type: "POST",
            data:fd,
            dataType: "html",
            cache: false,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(html)
            {
                var MsgReturn;
                if(html == 1){

                    MsgReturn = Alert_Div("Your PDF was successfully Deleted!",1,'success',null);
                    $("#pdf_"+id).remove();
                   
                }
                else{

                    MsgReturn = Alert_Div(html,1,'warning',null);

                }
                $("#results2").html(MsgReturn);
                $(".fa-spinner").remove();
            
            
            }
          
            });
        }
    }
    else
    {
        $("#results2").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//Activate PDF
$("body").on("click",".activation_element",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var stat = $(this).attr("data-stat");
    var elem =$(this);

    var id_len = (id.trim()).length;
    var errorLog = "";
    var errorCounter = 0;

    if(id_len < 1)
    {
        errorCounter+= 1;
        errorLog+= "Invalid Token <br/>";
    }

    if(stat == 1)
    {
        errorCounter+=1;
        errorLog+="This Pdf is already Active Mr! <br/>";
    }

    if(errorCounter == 0){  
        
        var fd = new FormData();
        fd.append('id',id);
        fd.append('cmd','set');  

        var request = $.ajax({
        url: "handlers/pdfpage.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("This Pdf has been set as home page",1,'success',null);
                $(".activation_element").removeClass("btn-success").addClass("btn-warning").html("activate").attr("data-stat",0);
                elem.removeClass("btn-warning").addClass("btn-success").html("active").attr("data-stat",1);

               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results2").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
        
    }
    else
    {
        $("#results2").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

//ADD NEW PDF
$("#upload_pdf").on('click',function(e){
    e.preventDefault;
    $(".alert-info").remove();
    $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>");
    
    var title = $("#title").val();

    var titlelen = (title.trim()).length;
    var errorCounter = 0;
    var errorLog = '';
    if(titlelen < 3)
    {
        errorLog = errorLog+ "Title Is Too Short <br/>";
        errorCounter+= 1;
    }

    
    var fd = new FormData();
    fd.append('title',title);   
    fd.append('cmd','put');
    fd.append('id',"null");
    var file = $("#filer_input")[0].files[0];
    var filename = $("#filer_input").val();
    if(filename.length>3){

        var size  = file.size;
        var type = file.type;
        if(size>8000000000){
            errorLog=errorLog + " File too large. 80MB max <br/>";
            errorCounter+=1;
        }
        if(type !== "application/pdf")
        {
            errorLog=errorLog + " You can upload PDFS only <br/>";
            errorCounter+=1;           
        }
        fd.append('file',file);
        fd.append('type','document');

    }
    else
    {
        fd.append('type',"null");
    }

    if(errorCounter == 0){
    

        var request = $.ajax({
        url: "handlers/pdfpage.php",
        type: "POST",
        data:fd,
        dataType: "html",
        cache: false,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        success: function(html)
        {
            var MsgReturn;
            if(html == 1){

                MsgReturn = Alert_Div("Your PDF was successfully Uploaded!",1,'success',null);
                $(".jFiler-item").remove();
                $("#title").val("");
                $("#filer_input").val("");
               
            }
            else{

                MsgReturn = Alert_Div(html,1,'warning',null);

            }
            $("#results").html(MsgReturn);
            $(".fa-spinner").remove();
        
        
        }
      
        });
    }
    else
    {
        $("#results").html(Alert_Div(errorLog,1,"warning",null));
        $(".fa-spinner").remove();
    }
});

$("body").on("click","#amount_type",function(e)
{
   
    var type = $(this).val();
    if(type == 1)
    {
        $(".range_element").show();
        $(".definite_element").hide();
    }
    else if(type == 2)
    {
        $(".range_element").hide();
        $(".definite_element").show();
    }

});


$("body").on("click","#add_payment",function(e)
{
    e.preventDefault;
    $(this).append("<i class='fa fa-spinner fa-spin'></i>");
    var title = $("#title").val();
    var amount_type = $("#amount_type").val();
    var description = $("#description").val();
    var min = 0;
    var max = 0;
    var errorCounter = 0;
    var errorMessage = "";
    if(amount_type == 1)
    {
        min = parseInt($("#min").val());
        max = parseInt($("#max").val());
    }
    else
    {
        min = parseInt($("#amount").val());
        max = min;
    }
    if(min > max)
    {
        errorCounter += 1;
        errorMessage += "The Minimum amount can not be greater than the maximum amount,They can only be equal,in which case you can just select Definite Amount type! <br/>";
    }

    if(title.length < 4)
    {
        errorCounter += 1;
        errorMessage += "Payment title is too short <br/>";
    }

    if(description.length < 5)
    {
        errorCounter += 1;
        errorMessage += "Payment Description is too short <br/>";
    }

    if(min == 0 || max == 0 || isNaN(min)==true || isNaN(max) == true)
    {
        errorCounter += 1;
        errorMessage += "Invalid Payment Amount(s) <br/>";
    }

    if(errorCounter == 0)
    {
        $.post("handlers/payment.php",{title:title,description:description,min:min,max:max,cmd:"put",id:"null",type:amount_type},function(res)
        {
            if(res == 1)
            {
                $("#results").html(Alert_Div("Your payment has successfully been added!",1,'success',null));
                $(".fa-spinner").remove();
                $("#min").val("");
                $("#max").val("");
                $("#title").val("");
                $("#description").val("");
            }
            else
            {
                $("#results").html(Alert_Div(res,1,'warning',null));
            }
        })
    }
    else
    {
        $("#results").html(Alert_Div(errorMessage,1,'warning',null));
        $(".fa-spinner").remove();
    }
});

$("body").on("click",".edit_payment_request",function(e)
{
    e.preventDefault;
    var title = $(this).attr("data-title");
    var description = $(this).attr("data-desc");
    var min = $(this).attr("data-min");
    var max = $(this).attr("data-max");
    var type = $(this).attr("data-type");
    var id = $(this).attr("id");
    
    if(title.length > 1 && description.length > 1 && min.length > 1 && max.length > 1 && type.length >= 1)
    {
        $("#title").val(title);
        $("#description").val(description);
        $(".edit_payment").attr("id",id);
        if(type == 1)
        {
            
            $("#min").val(min);
            $("#max").val(max);
            $(".range_element").show();
            $("#amount_type>option:eq(1)").prop('selected',true);
        }
        else
        {
            
            $("#amount").val(min);
            $(".definite_element").show();
            $("#amount_type>option:eq(2)").prop('selected',true);
        }
        $("#add_payment").hide();
        $(".edit_payment").show();
        $(".cancel_operation").show();
        $("#add_media").show();
        $("#media_list").hide();
        $(".toggle_media_list").html("Show Payment").attr("id",1);
    }
    else
    {
        $("result2").html(Alert_Div("Could not process this edit request at this time, Please refresh and try again!",1,'warning',null));
    }
});

$("body").on("click",".edit_payment",function(e)
{
    e.preventDefault;
    $(this).append("<i class='fa fa-spinner fa-spin'></i>");
    var title = $("#title").val();
    var amount_type = $("#amount_type").val();
    var description = $("#description").val();
    var id = $(this).attr("id");
    var min = 0;
    var max = 0;
    var errorCounter = 0;
    var errorMessage = "";
    if(amount_type == 1)
    {
        min = parseInt($("#min").val());
        max = parseInt($("#max").val());
    }
    else
    {
        min = parseInt($("#amount").val());
        max = min;
    }
    if(min > max)
    {
        errorCounter += 1;
        errorMessage += "The Minimum amount can not be greater than the maximum amount,They can only be equal,in which case you can just select Definite Amount type! <br/>";
    }

    if(id < 5)
    {
        errorMessage += "Invalid Payment Token <br/>";
        errorCounter += 1;
    }

    if(title.length < 4)
    {
        errorCounter += 1;
        errorMessage += "Payment title is too short <br/>";
    }

    if(description.length < 5)
    {
        errorCounter += 1;
        errorMessage += "Payment Description is too short <br/>";
    }

    if(min == 0 || max == 0 || isNaN(min)==true || isNaN(max) == true)
    {
        errorCounter += 1;
        errorMessage += "Invalid Payment Amount(s) <br/>";
    }

    if(errorCounter == 0)
    {
        $.post("handlers/payment.php",{title:title,description:description,min:min,max:max,cmd:"edit",id:id,type:amount_type},function(res)
        {
            if(res == 1)
            {
                $("#results").html(Alert_Div("Your payment has successfully been editted!",1,'success',null));
                $(".fa-spinner").remove();
                var type_sum = 0;
                if(amount_type == 1)
                {
                    type_sum = "("+min+" - "+max+")";
                }
                else
                {
                    type_sum = min;
                }
                $("#payment_"+id+">.title").html(title+" <i class='fa fa-angle-right'></i> "+type_sum);
                $("#payment_"+id+">.description").html(description);
                $("#min").val("");
                $("#max").val("");
                $("#amount").val("");
                $("#title").val("");
                $("#description").val("");
                $(".edit_payment").hide();
                $(".cancel_operation").hide();
                $("#add_payment").show();

            }
            else
            {
                $("#results").html(Alert_Div(res,1,'warning',null));
            }
        })
    }
    else
    {
        $("#results").html(Alert_Div(errorMessage,1,'warning',null));
        $(".fa-spinner").remove();
    }
});

$("body").on("click",".cancel_operation",function(e)
{
    e.preventDefault;
    $(this).hide();
    $(".edit_payment").hide();
    $("#add_payment").show();
    $("#min").val("");
    $("#max").val("");
    $("#title").val("");
    $("#description").val("");
    $("#amount").val("");
    $(".definite_element").hide();
    $(".range_element").hide();

});

$("body").on("click",".delete_payment",function(e)
{
    e.preventDefault;
    $(this).append("<i class='fa fa-spinner fa-spin'></i>");
    var id = $(this).attr("id");
    if(id.length > 5)
    {
        var sure = confirm("Are you sure you want to delete this payment?");
        if(sure)
        {
            $.post("handlers/payment.php",{cmd:"delete",id:id},function(res)
            {
                if(res == 1)
                {
                    $("#results2").html(Alert_Div("You have deleted payment successfully!",1,'success',null));
                    $(".fa-spinner").remove();
                    $("#payment_"+id).remove();
                }
                else
                {
                    $("#results2").html(Alert_Div("You can not delete this payment at this time!",1,'warning',null));
                    $(".fa-spinner").remove();                    
                }
            })
        } 

    }
    else
    {
        $("#results2").html(Alert_Div("Error Encountered, What 're you doing?",1,'warning',null));
        $(".fa-spinner").remove();
    }

});

$("body").on("click",".show_history",function(e)
{
    e.preventDefault;
    var data = {id:"null",cmd:"history"};
    $.post("handlers/payment.php",data,function(res)
    { 
        var data = parseInt(res);
        if(data !== 0)
        {
            $(".modal-body").html(res);
            $(".modal-title").html("Transaction History");
        }
        else
        {
            $(".modal-body").html(Alert_Div("No transaction was found",1,"danger",null));
            $(".modal-title").html("Transaction History"); 
        }
    })
});

$("body").on("click",".view_transaction",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var title = $(this).attr("data-title");
    var data = {id:id,cmd:"transaction"};
    $.post("handlers/payment.php",data,function(res)
    {  
        var data = parseInt(res);
        if(data !== 0)
        {
            $(".modal-body").html(res);
            $(".modal-title").html("Transaction History for "+title);
        }
        else
        {
            $(".modal-body").html(Alert_Div("No transaction was found!",1,"danger",null));
            $(".modal-title").html("Transaction History for "+title); 
        }
    })
});

$("body").on("click",".show_likers",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var data = {id:id,cmd:"likers"};
    $.post("handlers/reaction.php",data,function(res)
    {  
        var data = parseInt(res);
        if(data !== 0)
        {
            $(".modal-body").html(res);
            $(".modal-title").html("People who liked this");
        }
        else
        {
            $(".modal-body").html(Alert_Div("No one has liked this yet :) ",1,"danger",null));
            $(".modal-title").html("People who liked this"); 
        }
    })
});

$("body").on("click",".show_viewers",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var data = {id:id,cmd:"viewers"};
    $.post("handlers/reaction.php",data,function(res)
    {  
        var data = parseInt(res);
        if(data !== 0)
        {
            $(".modal-body").html(res);
            $(".modal-title").html("People who viewed this");
        }
        else
        {
            $(".modal-body").html(Alert_Div("No one has viewes this yet :) ",1,"danger",null));
            $(".modal-title").html("People who viewed this"); 
        }
    })
});

$("body").on("click",".show_comments",function(e)
{
    e.preventDefault;
    var id = $(this).attr("id");
    var data = {id:id,cmd:"commenters"};
    $.post("handlers/reaction.php",data,function(res)
    {  
        var data = parseInt(res);
        if(data !== 0)
        {
            $(".modal-body").html(res);
            $(".modal-title").html("People who commented on this");
        }
        else
        {
            $(".modal-body").html(Alert_Div("No one has commented on this yet :) ",1,"danger",null));
            $(".modal-title").html("People who commented on this"); 
        }
    })
});

})                    
     