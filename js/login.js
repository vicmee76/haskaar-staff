// JavaScript Document
function Alert_Div(message,canHide,type,cmd){
    //Do you wanna hide lol
    
    if(canHide==1){
        var DisM="alert-dismissable";
    }
    else{
        var DisM=""
    }
    //Color And alert Type
    if(type=="info"){
        var typeOf="alert-info";
    }
    else if (type=="warning"){
        var typeOf="alert-warning";
    }
    else if (type=="success"){
        var typeOf="alert-success";
    }
    else if (type=="primary"){
        var typeOf="alert-primary";
    }
    else if (type=="default"){
        var typeOf="alert-default";
    }
    
    //so here is for commands you dont know yet 
    if(cmd==null || cmd ==''){
        cmd='';
    }
    var divConst="<div class='alert "+typeOf+" "+cmd+" "+DisM+"'>"+message+"</div>";
    return divConst; 
    
    
    
    
}
$(document).ready(function(){

    function disableBtns(){
     $('#verifyCode').prop('disabled', true);
     $('#resendCode').prop('disabled', true);
     $('#emailCode').prop('disabled', true);

    }
    function enableBtns(){
        $('#verifyCode').removeProp('disabled');
        $('#resendCode').removeProp('disabled');
        $('#emailCode').removeProp('disabled');
    }

	

	$("#verifyCode").on('click',function(e){
    e.preventDefault;
     $("#results").html(Alert_Div("VERIFYING CODE...",1,"primary",null));
     $(this).append("<i class='fa fa-spinner fa-spin pull-right'></i>")
     disableBtns();
    
    var vc=$('#verificationcode').val();
    vc=vc.trim();
    if(vc.length>4){
    var request = $.ajax({
    url: "handlers/verifyVC.php",
    type: "POST",
    data: {vcCode:vc},
    dataType: "html",
    cache: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("Your Account has been verified,Thank you! Grab your KeyBoard, We re taking you to your dashboard!",1,'success',null);
            setTimeout(function(){
                window.location.href="settings.php";
            },2000);
        }
        else if(html==2){
            MsgReturn=Alert_Div("Your Account has been verified,Thank you! Grab your SmartPhone, We re taking you to the PlayStore",1,'success',null);
            setTimeout(function(){
                window.location.href="https://play.google.com/store/apps/details?id=com.eloksware.askaarmobile.askaarmobile";
            },2000);
        }
        else{
            MsgReturn=Alert_Div("Something went wrong, try again or get a new code,If issue persists,Contact Us",1,'warning',null);
            
            
        }
    $("#results").html(MsgReturn);
    $(".fa-spinner").remove();
    enableBtns();
    
  }
  
});}
else{
 $("#results").html(Alert_Div("Enter Your Token Code",1,"primary",null));
 $(".fa-spinner").remove();
 enableBtns();
}
})

$("#resendCode").on('click',function(e){
    e.preventDefault;
    $("#results").html(Alert_Div("RESENDING CODE",1,"primary",null));
    disableBtns();
    var request = $.ajax({
    url: "handlers/resendVC.php",
    type: "POST",
    dataType: "html",
    cache: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("Your verification code has been re-sent to your phone",1,'success',null);
        
        }
        else if (html==2){
            MsgReturn=Alert_Div("You have reached your maximum sms request,please use email",1,'success',null);
        }
        else{
            MsgReturn=Alert_Div("Something went wrong, try again or get code by email,If issue persists,Contact Us",1,'warning',null);
            
            
        }
    $("#results").html(MsgReturn);
    enableBtns();
  }
});
})
$("#emailCode").on('click',function(e){
    e.preventDefault;
    $("#results").html(Alert_Div("SENDING CODE TO EMAIL",1,"primary",null));
    disableBtns();
    var request = $.ajax({
    url: "handlers/getCodeByEmail.php",
    type: "POST",
    dataType: "html",
    cache: false,
    success: function(html){
        var MsgReturn;
        if(html==1){
            MsgReturn=Alert_Div("Your verification code has been sent to your email (Be sure to check your junk/spam folders)",1,'success',null);
        
        }
        else{
            MsgReturn=Alert_Div("Something went wrong, try again,If issue persists,Contact Us",1,'warning',null);
            
            
        }
    $("#results").html(MsgReturn);
    enableBtns();
  }
});
})

})