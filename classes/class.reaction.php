<?php
include_once("class.config.php");
class reactions extends db_connection
{
	
	var $client_id;
	var $stuff_id;
	var $time_now;

	public function __construct($userid,$stuffid)
	{
		$this->stuff_id = $stuffid;
		$this->client_id = $userid;
		$this->time_now = time();
		$this->connect();
	}

	public function like_stuff()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "INSERT INTO `likes`(`userid`, `postid`) VALUES ('$client_id','$stuff_id')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function comment_stuff($comment)
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `comments`(`comment`, `dateofcomment`, `userid`, `postid`) VALUES ($comment,$time_now,'$client_id','$stuff_id')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function unlike_stuff()
	{
		$is_reactor = $this->validate_reactor("likes");
		if($is_reactor == 1)
		{
			$stuff_id = $this->stuff_id;
			$client_id = $this->client_id;
			$sql = "DELETE FROM `likes` where userid = '$client_id' and postid = '$stuff_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}

	public function view_stuff()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "INSERT INTO `views`(`userid`, `postid`) VALUES ('$client_id','$stuff_id')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function edit_comment($comment,$id)
	{
		$is_reactor = $this->validate_reactor("comments");
		if($is_reactor == 1)
		{
			$stuff_id = $this->stuff_id;
			$client_id = $this->client_id;
			$time_now = $this->time_now;
			$sql = "UPDATE `comments` set `editted`=`comment`,`comment`=$comment where userid='$client_id' and postid='$stuff_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function delete_comment()
	{
		$is_reactor = $this->validate_reactor("comments");
		if($is_reactor == 1)
		{
			$stuff_id = $this->stuff_id;
			$client_id = $this->client_id;
			$time_now = $this->time_now;
			$sql = "DELETE from `comments` where userid='$client_id' and postid='$stuff_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
		
	}

	public function view_likers()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT members.fullname,members.image,members.hash FROM `likes` inner join members on members.hash = likes.userid WHERE likes.postid = '$stuff_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}	

	public function view_commenters()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT members.fullname,members.image,members.hash,comments.comment,comments.editted,comments.dateofcomment FROM `comments` inner join members on members.hash = comments.userid WHERE comments.postid = '$stuff_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}	

	public function view_viewers()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT members.fullname,members.image,members.hash FROM `likes` inner join members on members.hash = views.userid WHERE views.postid = '$stuff_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function count_likes()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT * FROM `likes` WHERE `postid`='$stuff_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			return $count;
		}
	}

	public function count_views()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT * FROM `views` WHERE `postid`='$stuff_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			return $count;
		}
	}

	public function count_comments()
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;
		$sql = "SELECT * FROM `comments` WHERE `postid`='$stuff_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			return $count;
		}
	}

	public function validate_reactor($tbl)
	{
		$stuff_id = $this->stuff_id;
		$client_id = $this->client_id;

		$sql = "SELECT * FROM `$tbl` WHERE `postid`='$stuff_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
}
?>