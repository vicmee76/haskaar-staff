<?php
include("class.config.php");
class mediaset extends db_connection
{
	var $media_type;
	var $provider_id;
	var $user_id;
	var $user_type;
	var $time_now;
	var $error;

	public function __construct($userid,$providerid,$usertype,$mediatype)
	{
		$this->media_type = $mediatype;
		$this->provider_id = $providerid;
		$this->user_id = $userid;
		$this->user_type = $usertype;
		$this->connect();
		$this->time_now = time();
	}

	public function list_media($list_command)
	{
		
		$tbl_name = $this->media_tbl();
		$row_count = $this->count_media();
		$provider_id = $this->provider_id;
		$sql = "select * from $tbl_name where orgid = '$provider_id' order by id desc limit $list_command,20";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result === false)
		{
			$response['success'] = 2;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			
			else
			{
				$response['success'] = 0;
			}
			if(((int)$count + 20) < $row_count)
			{
				$response['more'] = 1;
			}
			else
			{
				$response['more'] = 0;
			}
		}
		return $response;


	}

	public function get_media($id)
	{
		
		$tbl_name = $this->media_tbl();
		$row_count = $this->count_media();		
		$col_name = $this->media_col_id;
		$provider_id = $this->provider_id;
		$sql = "select * from $tbl_name where col_name = '$id'";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result === false)
		{
			$response['success'] = 2;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			
			else
			{
				$response['success'] = 0;
			}
			
		}
		return $response;


	}

	public function get_feed_media($id)
	{
		$sql = "select * from media where token = '$id'";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result === false)
		{
			$response['success'] = 2;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			
			else
			{
				$response['success'] = 0;
			}
			
		}
		return $response;


	}

	public function count_media()
	{
		$tbl_name = $this->media_tbl();		
		$provider_id = $this->provider_id;
		$sql = "select * from $tbl_name where orgid = '$provider_id' order by id desc";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$this->error = $result;
			$count = $result->num_rows;
			return $count;
		}
	}

	private function media_tbl()
	{
		$m_type = $this->media_type;
		$tbl_name = '';
		$response = array();
		switch ($m_type)
		{
			case "audio":
			$tbl_name = "audioalbum";
			break;

			case "video":
			$tbl_name = "videosalbum";
			break;

			case "photo":
			$tbl_name = "album";
			break;
		}
		return $tbl_name;
	}

	private function media_col_id()
	{
		$m_type = $this->media_type;
		$col_name = '';
		switch ($m_type)
		{
			case "audio":
			$col_name = "audioid";
			break;

			case "video":
			$col_name = "videoid";
			break;

			case "photo":
			$col_name = "photoid";
			break;
		}
		return $col_name;
	}

	public function add_media($title,$description,$media,$guid)
	{	
		$sql = $this->get_sql("insert",$title,$description,$media,$guid);
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}

	}

	public function delete_media($media_id)
	{
		$is_provider = $this->validate_provider($media_id);
		$sql = $this->get_sql("delete",null,null,null,$media_id);
		if($is_provider == 1)
		{
			$result = $this->sqli->query($sql);
			if($result === 0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}

	public function update_media($title,$description,$media,$guid)
	{
		$is_provider = $this->validate_provider($guid);
		$sql = $this->get_sql("update",$title,$description,$media,$guid);
		if($is_provider == 1)
		{
			$result = $this->sqli->query($sql);
			if($result === 0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}

	private function validate_provider($media_id)
	{
		$tbl_name = $this->media_tbl();
		$col_name = $this->media_col_id();
		$sql = "SELECT * from $tbl_name where $col_name = $media_id";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	private function get_sql($sql_type,$title,$description,$media,$guid)
	{
		$m_type = $this->media_type;
		$tbl_name = $this->media_tbl();
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = '';
		if($m_type == "audio")
		{
			if($sql_type == "insert")
			{
				$sql = "INSERT INTO $tbl_name (`title`, `orgid`, `description`, `audio`, `audioid`,`dateposted`) VALUES ($title,'$provider_id',$description,$media,$guid,$time_now)";
			}
			else if($sql_type == "update")
			{
				if($title !== null)
				{
					$title = ",title = $title";
				}
				else
				{
					$title = "";
				}
				if($description !== null)
				{
					$description = ",description = $description";
				}
				else
				{
					$description = "";
				}
				if($media !== null)
				{
					$media = ",audio = $media";
				}
				else
				{
					$media = "";
				}
				$sql = "UPDATE $tbl_name SET audioid = $guid $title $description $media WHERE audioid = $guid";

			}
			else if($sql_type == "delete")
			{
				$sql = "DELETE FROM $tbl_name WHERE audioid = $guid";
			}
			else if($sql_type == "select")
			{
				$sql = "SELECT * from $tbl_name where audioid = $guid";
			}
		}
		else if($m_type == "photo")
		{
			if($sql_type == "insert")
			{
				$sql = "INSERT INTO $tbl_name (`title`, `orgid`, `description`, `link`, `photoid`,`dateposted`) VALUES ($title,'$provider_id',$description,$media,$guid,$time_now)";
			}
			else if($sql_type == "update")
			{
				if($title !== null)
				{
					$title = ",title = $title";
				}
				else
				{
					$title = "";
				}
				if($description !== null)
				{
					$description = ",description = $description";
				}
				else
				{
					$description = "";
				}
				if($media !== null)
				{
					$media = ",link = $media";
				}
				else
				{
					$media = "";
				}
				$sql = "UPDATE $tbl_name SET photoid = $guid $title $description $media WHERE photoid = $guid";

			}
			else if($sql_type == "delete")
			{
				$sql = "DELETE FROM $tbl_name WHERE photoid = $guid";
			}
			else if($sql_type == "select")
			{
				$sql = "SELECT * from $tbl_name where photoid = $guid";
			}
		}
		else if($m_type == "video")
		{
			if($sql_type == "insert")
			{
				$sql = "INSERT INTO $tbl_name (`title`, `orgid`, `description`, `video`, `videoid`,`dateposted`) VALUES ($title,'$provider_id',$description,$media,$guid,$time_now)";
			}
			else if($sql_type == "update")
			{
				if($title !== null)
				{
					$title = ",title = $title";
				}
				else
				{
					$title = "";
				}
				if($description !== null)
				{
					$description = ",description = $description";
				}
				else
				{
					$description = "";
				}
				if($media !== null)
				{
					$media = ",video = $media";
				}
				else
				{
					$media = "";
				}
				$sql = "UPDATE $tbl_name SET videoid = $guid $title $description $media WHERE videoid = $guid";

			}
			else if($sql_type == "delete")
			{
				$sql = "DELETE FROM $tbl_name WHERE videoid = $guid";
			}
			else if($sql_type == "select")
			{
				$sql = "SELECT * from $tbl_name where videoid = $guid";
			}
		}

		return $sql;
	}
}


?>