<?php
include_once("class.config.php");
class announcement extends db_connection
{
	var $provider_id;
	var $time_now;
	var $client_id;

	public function __construct($providerid)
	{
		$this->provider_id = $providerid;
		$this->time_now = time();
		$this->connect();
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_announcement()
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `announcement` WHERE `orgid` = '$provider_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function add_announcement($title,$description,$duration,$id)
	{
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `announcement`(`orgid`, `title`, `description`, `duration`, `timestamp`,hash) VALUES ('$provider_id',$title,$description,$duration,$time_now,$id)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function edit_announcement($title,$description,$duration,$id)
	{
		$is_provider = $this->validate_provider($id);
		$provider_id = $this->provider_id;
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			if($title == null)
			{
				$title = '';
			}
			else
			{
				$title = ",title=$title";
			}

			if($description == null)
			{
				$description = '';
			}
			else
			{
				$description = ",description = $description";
			}

			if($duration == null)
			{
				$duration = '';
			}
			else
			{
				$duration = ",duration=$duration";
			}
			$sql = "UPDATE announcement set timestamp=timestamp $title $description $duration where hash = $id and orgid = '$provider_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}

	}

	public function reset_announcement($id)
	{
		$is_provider = $this->validate_provider($id);
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		if($is_provider == 1)
		{
			$sql = "UPDATE announcement set timestamp=$time_now where hash = $id and orgid = '$provider_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}
	public function delete_announcement($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			$sql = "DELETE from announcement where orgid = '$provider_id' and hash = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function announcement_details($id)
	{
		$sql = "Select * from announcement where hash = $id";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			else
			{
				$response['success'] = 0;
			}
			
		}

		return $response;
	}

	private function validate_provider($id)
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `announcement` WHERE `hash` = $id and  orgid = '$provider_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

}





?>