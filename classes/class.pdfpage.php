<?php
include("class.config.php");
class pdf_page extends db_connection
{
	var $provider_id;
	var $time_now;
	var $client_id;

	public function __construct($providerid)
	{
		$this->provider_id = $providerid;
		$this->time_now = time();
		$this->connect();
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_pdf()
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `pdfpager` WHERE `orgid` = '$provider_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function add_pdf($title,$pdf)
	{
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `pdfpager`(`orgid`, `title`, `timestamp`, `pdf`, `status`) VALUES ('$provider_id',$title,$time_now,$pdf,0)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function set_pdf($id)
	{
		$is_provider = $this->validate_provider($id);
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		if($is_provider == 1)
		{
			$sql = "UPDATE pdfpager set status=0 where orgid = '$provider_id'; ";
			$sql2 = "UPDATE pdfpager set status=1 where id = $id and orgid = '$provider_id';";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{

				$result2 = $this->sqli->query($sql2);
				if($result2 == false)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
		}
		else
		{
			return 0;
		}
	}

	public function delete_pdf($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			$sql = "DELETE from pdfpager where orgid = '$provider_id' and id = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	private function validate_provider($id)
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `pdfpager` WHERE `id` = $id and  orgid = '$provider_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

}
?>