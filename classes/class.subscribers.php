<?php
include("class.config.php");

class subscribers extends db_connection
{
	public $current_userid;
	public $provider_userid;
	public $device_used;
	private $time_now;
	private $handle;
	public $error_finder;

	function __construct($userid,$providerid,$device)
	{
		$this->current_userid = $userid;
		$this->provider_userid = $providerid;
		$this->device_used = $device;
		$this->connect();
		$this->handle = $this->db_handle;
		$this->time_now = time();
	}

	public function subscribe()
	{
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;
		$is_subscribed = $this->check_Subscription();
		if($is_subscribed == 0)
		{
			mysqli_query($this->handle,"INSERT into subscribers (userid,providerid,datesubscribed) values ('$current_userid','$provider_userid','$time_now')");
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function un_subscribe()
	{
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;
		$is_subscribed = $this->check_Subscription();
		if($is_subscribed == 1)
		{
			mysqli_query($this->handle,"DELETE from subscribers where userid=$current_userid and providerid=$provider_userid");
			return 1;
		}
		else
		{
			return 0;
		}
	}


	public function list_subscribers($list_command)
	{
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;
		$limit = "limit $list_command,20";
		if($list_command == "all")
		{
			$limit = "";
		}
		$sql = "Select members.image,members.fullname,members.hash from subscribers inner join members on members.hash=subscribers.userid where subscribers.providerid='$provider_userid' order by subscribers.datesubscribed $limit";
		$this->error_finder = $sql;
		$result = $this->sqli->query($sql);
		$count = mysqli_num_rows($result);
		$all_count = $this->count_subscribers(0);
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$data_userid = $data['hash'];
				$data_fullname = $data['fullname'];
				$data_image = $data['image'];
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}

		if(((int)$count + 20) < $all_count)
		{
			$response['more'] = 1;
		}
		else
		{
			$response['more'] = 0;
		}
		
		return $response;
	}

	public function count_subscribers($count_command)
	{	
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;
		if($count_command == 1)
		{
			//Without Me
			$sql = "Select * from subscribers where userid<>'$current_userid' and providerid='$provider_userid'";
		}
		else
		{		
			//With Me
			$sql = "Select * from subscribers where providerid='$provider_userid'";	
		}
		$result = mysqli_query($this->db_handle,$sql);
		$count = mysqli_num_rows($result);
		return $count;
	}

	public function check_Subscription()
	{
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;
		$sql = "Select * from subscribers where userid='$current_userid' and providerid='$provider_userid'";
		$result = mysqli_query($this->db_handle,$sql);
		$count = mysqli_num_rows($result);
		if($count > 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function check_stats($my_country)
	{
		$current_userid = $this->current_userid;
		$provider_userid = $this->provider_userid;		
		$response = array();
		$response['total'] = $this->count_subscribers(0);
		$response['male'] = 0;
		$response['female'] = 0;
		$response['home'] = 0;
		$response['abroad'] = 0;
		$response['today'] = 0;
		$cur_date = Date('Y-m-d');
		$cur_date_in_secs = strtotime($cur_date);
		$sql = "select members.gender,members.country,subscribers.datesubscribed from subscribers inner join members on members.hash = subscribers.userid where providerid = '$current_userid'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			while($data = $result->fetch_assoc())
			{
				if(strtolower($data['gender']) == "male")
				{
					$response['male']+=1;
				}
				else
				{
					$response['female']+=1;
				}

				if(strtolower($data['country']) == strtolower($my_country))
				{
					$response['home']+=1;
				}
				else
				{
					$response['abroad']+=1;
				}
				if($data['datesubscribed'] >= $cur_date_in_secs)
				{
					$response['today']+=1;
				}
			}
			return $response;
		}
	}

}


?>