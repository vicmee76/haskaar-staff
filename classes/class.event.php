<?php
include("class.config.php");
class events extends db_connection
{
	var $provider_id;
	var $time_now;
	var $client_id;

	public function __construct($providerid)
	{
		$this->provider_id = $providerid;
		$this->time_now = time();
		$this->connect();
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_events()
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `program` WHERE `orgid` = '$provider_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function add_event($title,$description,$timestamp,$address,$contact,$id,$image)
	{
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `program`( `orgid`, `title`, `description`, `address`, `timestamp`, `datetime`, `programid`,contact,image) VALUES ('$provider_id',$title,$description,$address,$timestamp,$time_now,$id,$contact,$image)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function edit_event($title,$description,$timestamp,$address,$contact,$id,$image)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			if($title == null)
			{
				$title = '';
			}
			else
			{
				$title = ",title = $title";
			}

			if($description == null)
			{
				$description = '';
			}
			else
			{
				$description = ",description = $description";
			}

			if($timestamp == null)
			{
				$timestamp = '';
			}
			else
			{
				$timestamp = ",timestamp = $timestamp";
			}

			if($address == null)
			{
				$address = '';
			}
			else
			{
				$address = ",address = $address";
			}

			if($contact == null)
			{
				$contact = '';
			}
			else
			{
				$contact = ",contact = $contact";
			}
			if($image == null)
			{
				$image = '';
			}
			else
			{
				$image = ",image = $image";
			}
			$sql = "UPDATE program set datetime=datetime $title $description $address $contact $image $timestamp where programid = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}

	}

	public function delete_event($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			$sql = "DELETE from program where orgid = '$provider_id' and progamid = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function event_details($id)
	{
		$sql = "Select * from program where programid = $id";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			else
			{
				$response['success'] = 0;
			}
			
		}

		return $response;
	}

	public function set_reminder($id)
	{
		$client_id = $this->client_id;
		$sql = "INSERT INTO `reminder`(`eventid`, `userid`) VALUES ($id,'$client_id')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	private function validate_provider($id)
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * from program where orgid = '$provider_id' and programid = $id";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
}
?>