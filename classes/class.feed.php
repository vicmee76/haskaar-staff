<?php

include_once("class.config.php");
include_once("class.event.php");
include_once("class.media.php");
include_once("class.profile.php");
class feed extends db_connection
{
	var $user_id;
	var $time_now;
	var $client_id;



	public function __construct($user_id,$client_id)
	{
		$this->user_id = $user_id;
		$this->client_id = $client_id;
		$this->connect();
	}

	public function set_client_id($value)
	{
		$this->client_id = $client_id;
	}

	public function get_all_feed($counter)
	{
		$sql = "select * from feeds limit $counter,20";
		$result = $this->sqli->query($sql);
		$response = array();
		$all_feed_count = $this->count_all_feed();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$userid = $data['userid'];
					$type = $data['type'];
					$token = $data['token'];
					$client_id =  $this->client_id;
					$user_object = new profile($client_id,$user_id);
					$user_details = $user_object->get_user_profile();
					$org_res = $this->get_responses($data['token']);
					if($org_res !== 0)
					{
						$response['response'] = $org_res;
					}
					else
					{
						$response['response'] = 0;
					}
						

					if($type == "event")
					{
						$event = new event($userid);
						$event_details = $event->event_details($token);
						if($event_details == 0)
						{
							$response[] = 0;
						}
						else
						{	$event_details['feedtype'] = "event";
							$response[] = array_merge($event_details,$user_details);
						}
					}

					elseif($type == "video")
					{
						$media = new mediaset($client_id,$userid,"user","video");
						$media_details = $media->get_media($token);
						if($media_details == 0 || $media_details == 2)
						{
							$response[] = 0;
						}
						else
						{	$media_details['feedtype'] = "video";
							$response[] =  array_merge($media_details,$user_details);
						}
					}
					elseif($type == "audio")
					{
						$media = new mediaset($client_id,$userid,"user","audio");
						$media_details = $media->get_media($token);
						if($media_details == 0 || $media_details == 2)
						{
							$response[] = 0;
						}
						else
						{	$media_details['feedtype'] = "audio";
							$response[] = array_merge($media_details,$user_details);
						}
					}

					elseif($type == "photo")
					{
						$media = new mediaset($client_id,$userid,"user","photo");
						$media_details = $media->get_media($token);
						if($media_details == 0 || $media_details == 2)
						{
							$response[] = 0;
						}
						else
						{	$media_details['feedtype'] = "photo";
							$response[] = array_merge($media_details,$user_details);
						}
					}
					elseif($type == "update")
					{
						$sql = "select members.fullname,members.image,members.hash,members.type,updates.userid,updates.msg,updates.timestamp from updates inner join members on members.hash = updates.userid where updates.token = '$token'";
						$result = $this->sqli->query($sql);
						if($result == false)
						{
							$response[] = 0;
						}
						else
						{
							$counter = $result->num_row;

							if($counter > 0)
							{

								while($data = $result->fetch_assoc())
								{	
									$data['feedtype'] = "update";
									if($data['type'] !== "post")
									{
										$media = new mediaset($client_id,$userid,"user","photo");
										$media_details = $media->get_feed_media($token);
										if($media_details !== 0)
										{
											$response[] = array_merge($data,$media_details);
										}
									}
									else
									{
										$response[] = $data;
									}
									
								}
							}
							else
							{
								$response[] = 0;
							}
						}
					}

				}
				if(((int)$counter+20)<$all_feed_count)
			    {
			      $response['more']=1;
			    }
			    else
			    {
			      $response['more']=0;
			    }

				return $response;
			}
			else
			{
				$response['success'] = 0;
				return $response;
			}
		}
	}

	public function get_user_feed($counter)
	{
		$client_id = $this->client_id;
		$sql = "select members.fullname,members.image,members.hash,members.type as usertype,updates.userid,updates.msg,updates.timestamp,updates.type from updates inner join members on members.hash = updates.userid where updates.userid = '$client_id' limit $counter,20";
		$result = $this->sqli->query($sql);
		$all_user_feed_count = $this->count_user_feed();
		$response = array();
		if($result == false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_row;

			if($counter > 0)
			{
				$response['success'] = 0;			
				while($data = $result->fetch_assoc())
				{	
					$org_res = $this->get_responses($data['token']);
					if($org_res !== 0)
					{
						$data['response'] = $org_res;
					}
					else
					{
						$data['response'] = 0;
					}
					$user_tags = $this->get_tags($data['token']);
					if($user_tags !== 0)
					{
						$data['tags'] = $user_tags;
					}
					else
					{
						$data['tags'] = 0;
					}
					if($data['type'] !== "post")
					{
						$media = new mediaset($client_id,$userid,"user","photo");
						$media_details = $media->get_feed_media($token);
						

						if($media_details !== 0)
						{
							if($data['usertype'] !== "user")
							{
								$org_info = $user_object->get_org_info_from_staff($client_id);
								if($org_info == 1)
								{
									$response['data'][] = array_merge($data,$media_details,$org_info);
								}
								else
								{
									$response['data'][] = array_merge($data,$media_details);
								}
								

							}
							else
							{
								$response['data'][] = array_merge($data,$media_details);
							}
							
						}
					}
					else
					{						
						if($data['usertype'] !== "user")
						{
							$org_info = $user_object->get_org_info_from_staff($client_id);
							if($org_info == 1)
							{
								$response['data'][] = $data;
							}
							else
							{
								$response['data'][] = $data;
							}
							

						}
						else
						{
							$response['data'][] = $data;
						}
					}
					
				}
				if(((int)$counter+20)<$all_user_feed_count)
			    {
			      $response['more']=1;
			    }
			    else
			    {
			      $response['more']=0;
			    }

			    return $response;
			}
			else
			{
				$response['success'] = 0;
				return $response;
			}
		}
	}

		public function get_room_feed($counter,$room)
	{
		$client_id = $this->client_id;
		$sql = "select members.fullname,members.image,members.hash,members.type as usertype,updates.userid,updates.msg,updates.timestamp,updates.type from updates inner join members on members.hash = updates.userid where updates.room = '$room' limit $counter,20";
		$result = $this->sqli->query($sql);
		$all_user_feed_count = $this->count_user_feed();
		$response = array();
		if($result == false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_row;

			if($counter > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{	
					$org_res = $this->get_responses($data['token']);
					if($org_res !== 0)
					{
						$data['response'] = $org_res;
					}
					else
					{
						$data['response'] = 0;
					}
					$user_tags = $this->get_tags($data['token']);
					if($user_tags !== 0)
					{
						$data['tags'] = $user_tags;
					}
					else
					{
						$data['tags'] = 0;
					}
					if($data['type'] !== "post")
					{
						$media = new mediaset($client_id,$userid,"user","photo");
						$media_details = $media->get_feed_media($token);
						

						if($media_details !== 0)
						{
							if($data['usertype'] !== "user")
							{
								$org_info = $user_object->get_org_info_from_staff($client_id);
								if($org_info == 1)
								{
									$response['data'][] = array_merge($data,$media_details,$org_info);
								}
								else
								{
									$response['data'][] = array_merge($data,$media_details);
								}
								

							}
							else
							{
								$response['data'][] = array_merge($data,$media_details);
							}
							
						}
					}
					else
					{						
						if($data['usertype'] !== "user")
						{
							$org_info = $user_object->get_org_info_from_staff($client_id);
							if($org_info == 1)
							{
								$response['data'][] = $data;
							}
							else
							{
								$response['data'][] = $data;
							}
							

						}
						else
						{
							$response['data'][] = $data;
						}
					}
					
				}
				if(((int)$counter+20)<$all_user_feed_count)
			    {
			      $response['more']=1;
			    }
			    else
			    {
			      $response['more']=0;
			    }

			    return $response;
			}
			else
			{
				$response['success'] = 0;
				return $response;
			}
		}
	}
	public function get_feed_details($id)
	{
		$client_id = $this->client_id;
		$sql = "select members.fullname,members.image,members.hash,members.type as usertype,updates.userid,updates.msg,updates.timestamp,updates.type from updates inner join members on members.hash = updates.userid where updates.token = '$id'";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result == false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_row;

			if($counter > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{	
					$org_res = $this->get_responses($data['token']);
					if($org_res !== 0)
					{
						$data['response'] = $org_res;
					}
					else
					{
						$data['response'] = 0;
					}
					$user_tags = $this->get_tags($data['token']);
					if($user_tags !== 0)
					{
						$data['tags'] = $user_tags;
					}
					else
					{
						$data['tags'] = 0;
					}
					if($data['type'] !== "post")
					{
						$media = new mediaset($client_id,$userid,"user","photo");
						$media_details = $media->get_feed_media($token);
						

						if($media_details !== 0)
						{
							if($data['usertype'] !== "user")
							{
								$org_info = $user_object->get_org_info_from_staff($client_id);
								if($org_info == 1)
								{
									$response['data'][] = array_merge($data,$media_details,$org_info);
								}
								else
								{
									$response['data'][] = array_merge($data,$media_details);
								}
								

							}
							else
							{
								$response['data'][] = array_merge($data,$media_details);
							}
							
						}
					}
					else
					{						
						if($data['usertype'] !== "user")
						{
							$org_info = $user_object->get_org_info_from_staff($client_id);
							if($org_info == 1)
							{
								$response['data'][] = $data;
							}
							else
							{
								$response['data'][] = $data;
							}
							

						}
						else
						{
							$response['data'][] = $data;
						}
					}
					
				}
				
			    return $response;
			}
			else
			{
				$response['success'] = 0;
				return $response;
			}
		}
	}
	public function add_update($msg,$tags,$media,$type,$token,$room)
	{
		$client_id = $this->client_id;
		$time_now = $this->time_now;
		if($msg == null)
		{
			$msg = "' '";
		}
		$sql = "INSERT INTO `updates`(`userid`, `msg`, `token`, `type`, `timestamp`, `room`) VALUES ('$client_id',$msg,$token,$type,'$time_now',$room)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			if(is_array($media))
			{
				for($i=0;$i <= count($media);$i++)
				{
					$add_media = $this->add_media($token,$media[$i]);
				}
			}
			if(is_array($tags))
			{
				for($i=0;$i <= count($tags); $i++)
				{
					$add_tags = $this->add_tags($token,$tags[$]);
				}
			}
			return 1;
		}
	}

	public function add_tags($a,$b)
	{
		$sql = "INSERT INTO `tags`( `userid`, `postid`, `type`) VALUES ($b,$a,(select type from members where hash = $b))";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function add_media($a,$b)
	{
		$client_id = $this->client_id;
		$sql = "INSERT INTO `media`(`userid`, `token`, `media`) VALUES ('$client_id',$a,$b)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function add_response($msg,$token)
	{
		$client_id = $this->client_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `response` (`token`, `response`, `orgid`, `timestamp`) VALUES ($token,$msg,'$client_id','$time_now')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function get_responses($id)
	{
		$sql = "Select organization.name,organization.orgid,response.response,response.timestamp from response inner join organization on organization.orgid = response.orgid where response.token = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					return $data;
				}
			}
		}
	}

	public function get_tags($id)
	{
		$sql = "Select members.hash,members.fullname,tags.postid from tags inner join members on members.hash = $tags.userid where tags.postid = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					return $data;
				}
			}
		}
	}

	public function get_last_contacted_org($id)
	{
		$sql = "Select organization.orgid,organization.image,organization.name, = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					return $data;
				}
			}
		}
	}

	public function count_all_feed()
	{
		$sql = "select * from feeds";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			return $counter;
		}
	}

	public function count_user_feed()
	{
		$client_id = $this->client_id;
		$sql = "select * from feeds where userid ='$client_id'";
		$result = $this->sqli->query($sql);
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			return $counter;
		}
	}

	public function edit_feed($id,$msg)
	{

	}

	public function delete_feed($id)
	{
		$user_id = $this->user_id;
		$sql = "Delete from feeds where token = '$id' and userid = '$user_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}

?>