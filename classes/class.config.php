<?php

class db_connection
{
	private $username='root';
	private $password='';
	private $database='e50';
	private $server='localhost';
	protected $db_handle;
	private $db_found;
	public $sqli;

	private function set_database($new_db)
	{
		$this->database=$new_db;
	}

	private function set_username($new_uname)
	{
		$this->username=$new_uname;
	}

	private function set_password($new_pword)
	{
		$this->password=$new_pword;
	}

	public function connect()
	{
		$this->db_handle = mysqli_connect($this->server, $this->username, $this->password);
		$this->db_found = mysqli_select_db($this->db_handle,$this->database);
		$this->sqli =  new mysqli($this->server, $this->username, $this->password, $this->database);
	}
}


?>