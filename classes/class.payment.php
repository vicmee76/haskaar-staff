<?php

include("class.config.php");
class payment extends db_connection
{
	var $provider_id;
	var $time_now;
	var $payment_id;
	var $client_id;

	public function __construct($providerid)
	{

		$this->provider_id = $providerid;
		$this->time_now = time();
		$this->connect();
	}

	public function set_payment_id($id)
	{
		$this->payment_id = $id;
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_payments()
	{
		$provider_id = $this->provider_id;
		$sql = "Select * from payments where orgid = '$provider_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = mysqli_fetch_array($result))
			{
				$transactions = $this->count_transactions($data['paymentid']);
				$response['data'][] = array_merge($data,$transactions);
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	private function count_transactions($id)
	{
		$sql = "Select * from transactions where stuffid = '$id'";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			$response['count'] = $count;
			return $response;
		}
		
	}

	public function list_transactions($id)
	{
		$sql = "Select members.hash,members.fullname,members.image,transactions.datepaid,transactions.token,transactions.status,payments.title,transactions.amount from payments  inner join transactions on payments.paymentid = transactions.stuffid inner join members on members.hash=transactions.userid where transactions.stuffid = $id";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			else
			{
				$response['success'] = 0;
			}
			
		}

		return $response;
	}

	public function transaction_history()
	{
		$provider_id = $this->provider_id;
		$sql = "Select members.hash,members.fullname,members.image,transactions.datepaid,transactions.token,transactions.status,payments.title,transactions.amount from payments inner join transactions on payments.paymentid = transactions.stuffid inner join members on members.hash=transactions.userid  where payments.orgid = '$provider_id'";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			else
			{
				$response['success'] = 0;
			}
			
		}

		return $response;
	}

	public function add_payment($title,$description,$min,$max,$amounttype,$id)
	{
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `payments`(`orgid`, `title`, `description`, `amounttype`, `min`, `max`, `timestamp`,paymentid) VALUES ('$provider_id',$title,$description,$amounttype,$min,$max,$time_now,$id)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public function update_payment($title,$description,$min,$max,$amounttype,$id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			if($title == null)
			{
				$title = '';
			}
			else
			{
				$title = ",title=$title";
			}

			if($description == null)
			{
				$description = '';
			}
			else
			{
				$description = ",description = $description";
			}

			if($min == null)
			{
				$min = '';
			}
			else
			{
				$min = ",min=$min";
			}

			if($max == null)
			{
				$max = '';
			}
			else
			{
				$max = ",max=$max";
			}

			if($amounttype == null)
			{
				$amounttype = '';
			}
			else
			{
				$amounttype = ",amounttype=$amounttype";
			}

			$sql = "UPDATE payments set timestamp=timestamp $title $description $min $max $amounttype where paymentid = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}

	}

	public function delete_payment($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			$sql = "DELETE from payments where orgid = '$provider_id' and paymentid = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function payment_details($id)
	{
		$sql = "Select * from payments where paymentid = $id";
		$result = $this->sqli->query($sql);		
		$response = array();
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = $result->fetch_assoc())
				{
					$response['data'][] = $data;
				}
			}
			else
			{
				$response['success'] = 0;
			}
			
		}

		return $response;
	}

	private function validate_provider($id)
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * from payments where orgid = '$provider_id' and paymentid = $id";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	public function make_transaction($paymentid)
	{
		$client_id = $this->client_id;
		$token = $this->generate_token($client_id,$paymentid);
		$sql = "INSERT INTO `transactions`(`userid`, `stuffid`, `token`, `status`) VALUES ('$client_id',$paymentid,$token,0)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}

	}

	public function set_successful_transaction($paymentid)
	{
		$sql = "UPDATE transactions set status = 1 where stuffid = $paymentid";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	private function generate_token($a,$b)
	{
		$time_now = $this->time_now;
		$token = sha1($a.$b.$time_now);
		$token = md5($token);
		$token = substr($token,-8);
		$sql = "SELECT * from transactions where token = '$token'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 2;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				$this->generate_token($a,$b);
			}
			else
			{
				return 1;
			}
		}
	}
}



?>