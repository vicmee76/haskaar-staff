<?php
include("class.config.php");
class videochat extends db_connection
{
	var $user_id;
	var $time_now;
	var $client_id;
	var $call_type;

	public function __construct($providerid,$type)
	{
		$this->user_id = $providerid;
		$this->time_now = $time_now;
		$this->call_type = $type;
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_history()
	{
		$user_id = $this->user_id;
		$call_type = $this->call_type;
		$sql = "SELECT * FROM `calls` WHERE (`caller` = '$user_id' or `receiver` = '$user_id') and type = '$call_type'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = $result->fetch_assoc())
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function new_call($type,$receiver)
	{
		$user_id = $this->user_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `livestream`(`caller`, `receiver`, `timestamp`, `type`, `duration`, `status`) VALUES ('$user_id',$receiver,$time_now,$type,0,0)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;	
			
		}
	}

	public function set_schedule($id)
	{
		$is_provider = $this->validate_provider();
		$user_id = $this->user_id;
		$time_now = $this->time_now;
		if($is_provider == 1)
		{
			$sql = "INSERT INTO `schedules`(`orgid`, `timestamp`, `eventid`) VALUES ('$user_id',$time_now,$id)";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function end_call($id)
	{
		$is_caller = $this->validate_caller();
		$user_id = $this->user_id;
		$time_now = $this->time_now;
		if($is_caller == 1)
		{
			$sql = "UPDATE calls set duration = (timestamp - $time_now) where id = $id";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function cancel_schedule($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$user_id = $this->user_id;
			$sql = "DELETE from schedules where orgid = '$user_id' and eventid = $id;DELETE from reminder where eventid = $id ";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	private function validate_caller($id)
	{
		$user_id = $this->user_id;
		$sql = "SELECT * FROM `calls` WHERE `id` = $id and  (caller = '$user_id' or receiver = '$user_id')";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
}

?>