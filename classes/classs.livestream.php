<?php
include("class.config.php");
include("class.subscribers.php");
include("class.event.php");
class livestream extends db_connection
{
	var $provider_id;
	var $time_now;
	var $client_id;

	public function __construct($providerid)
	{
		$this->provider_id = $providerid;
		$this->time_now = $time_now;
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function list_streams()
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `livestream` WHERE `orgid` = '$provider_id'";
		$result = $this->sqli->query($sql);
		$count = $result->num_rows;
		$response = array();
		if($count > 0)
		{
			$response['success'] = 1;
			while($data = $result->fetch_assoc())
			{
				$response['data'][] = $data;
			}
		}
		else
		{
			$response['success'] =  0;
		}
		return $response;
	}

	public function new_livestream($title,$source,$token,$schedule)
	{
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		$sql = "INSERT INTO `livestream`(`orgid`, `title`, `timestamp`, `source`, `duration`, `token`) VALUES ('$provider_id',$title,$time_now,$source,0,$token)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			if($schedule == 1)
			{
				$run_schedule = $this->set_schedule($token);
				if($run_schedule == 1)
				{
					$subscribers = new subscribers($provider_id,$provider_id,'web');
					$all_subscribers = $subscribers->list_subscribers("all");
					if($all_subscribers['success'] == 1)
					{
						$each_subscriber = $all_subscribers['data'];
						for($i=0;$i<count($each_subscriber);$i++)
						{
							$subscriber_id = $each_subscriber[$i]['hash'];
							$reminder = new events($provider_id);
							$reminder->set_client_id($subscriber_id);
							$set_reminder = $reminder->set_reminder($token);
						}
					}
					
					return 1;
				}
				else
				{
					return 2;
				}
			}
			else
			{
				return 1;	
			}
			
		}
	}

	public function set_schedule($id)
	{
		$is_provider = $this->validate_provider();
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		if($is_provider == 1)
		{
			$sql = "INSERT INTO `schedules`(`orgid`, `timestamp`, `eventid`) VALUES ('$provider_id',$time_now,$id)";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function end_stream($id)
	{
		$is_provider = $this->validate_provider();
		$provider_id = $this->provider_id;
		$time_now = $this->time_now;
		if($is_provider == 1)
		{
			$sql = "UPDATE livestream set duration = (timestamp - $time_now) where id = $id and orgid = '$provider_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	public function cancel_schedule($id)
	{
		$is_provider = $this->validate_provider($id);
		if($is_provider == 1)
		{
			$provider_id = $this->provider_id;
			$sql = "DELETE from schedules where orgid = '$provider_id' and eventid = $id;DELETE from reminder where eventid = $id ";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	private function validate_provider($id)
	{
		$provider_id = $this->provider_id;
		$sql = "SELECT * FROM `livestream` WHERE `token` = $id and  orgid = '$provider_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$count = $result->num_rows;
			if($count > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
}

?>