<?php
	require("config.php");

	class products extends db_connection
	{
		public $providerid;
		public $productid;
		public $userid;
		public $time_now;
		public $sqli;

		public function __construct($uid)
		{
			$this->userid = $uid;
			$this->time_now = time();
			$this->connect();
			$this->sqli =  new mysqli($server, $username, $password, $database);
		}

		public function set_productid($prodid)
		{
			$this->productid = $prodid;
		}

		public function set_providerid($value)
		{
			$this->providerid = $value;
		}

		public function list_products($limit)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$all_count = $this->count_products();
			$range = 20;
			$sql = "select * from products where orgid = $providerid limit $limit,$range";
			$result = mysqli_query($db_handle,$sql);
			$count = mysqli_num_rows($result);
			$response = array();
			if($count > 0)
			{
				$response['success'] = 1;
				while($data = mysqli_fetch_assoc($result))
				{
					$response['data'][] = $data; 
				}
			}
			else
			{
				$response['success'] = 0;
			}
			if(((int)$count + 20) < $all_count)
			{
				$response['more'] = 1;
			}
			else
			{
				$response['more'] = 0;
			}
		
			return json_encode($response);

		}

		public function count_products()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$sql = "select * from products where orgid = '$providerid'";
			$result = mysqli_query($db_handle,$sql);
			$count = mysqli_num_rows($result);
			return $count;			
		}

		public function request_product()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$check_if_requested = $this->check_request_status();
			if($check_if_requested == null)
			{
				$sql = "INSERT INTO `productrequests`(`userid`, `productid`, `timestamp`, `status`) VALUES ('$userid','$productid','$time_now',0)";
				$result = mysqli_query($db_handle,$sql);
				if($result)
				{
					return 1;
				}
				else
				{
					return 0;
				}

			}
			else
			{
				return 2;
			}
			
		}

		public function cancel_product_request()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$check_if_requested = $this->check_request_status();
			if($check_if_requested !== null)
			{
				$sql = "DELETE FROM `productrequests` WHERE `userid` = '$userid' and productid = '$productid'";
				$result = mysqli_query($db_handle,$sql);
				if($result)
				{
					return 1;
				}
				else
				{
					return 0;
				}

			}
			else
			{
				return 2;
			}
			
		}

		public function check_request_status()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$sql = "Select * from productrequests where userid = '$userid' and productid = '$productid'";
			$result = mysqli_query($db_handle,$sql);
			$count = mysqli_count_rows($result);
			if($count > 0)
			{
				while($data = mysqli_fetch_assoc($result))
				{
					return $data['status'];
				}
			}
			else
			{
				return null;
			}
		}

		public function delete_product()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			if($is_provider == 1)
			{
				$sql = "DELETE from products where orgid = '$orgid' and productid = '$productid'";
				$result = $sqli->query($sql);
				if($result !=== false)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		}

		public function add_product($prod_name,$prod_id,$prod_info,$prod_price,$prod_discount,$prod_img,$api)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$sql = "INSERT INTO `products`(`title`, `orgid`, `description`, `price`, `image`, `discount`, `productid`,api) VALUES ($prod_name,'$userid',$prod_info,$prod_price,$prod_img,$prod_discount,$prod_id,$api)";
			$result = $sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

		public function edit_product($prod_name,$prod_info,$prod_price,$prod_discount,$api)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			if($is_provider == 1)
			{

				if($prod_name == null)
				{
					$prod_name = '';
				}
				else
				{
					$prod_name = ",title = $prod_name";
				}
				if($prod_info == null)
				{
					$prod_info = '';
				}
				else
				{
					$prod_info = ",description = $prod_info";
				}
				if($prod_price == null)
				{
					$prod_price = '';
				}
				else
				{
					$prod_price = ",price = $prod_price";
				}
				if($prod_discount == null)
				{
					$prod_discount = '';
				}
				else
				{
					$prod_discount = ",price = $prod_discount";
				}

				if($api == null)
				{
					$api = '';
				}
				else
				{
					$api = ",api = $api";
				}

				$sql = "UPDATE `products` SET productid = '$productid' $prod_name $prod_discount $prod_price $prod_info $api where productid = '$productid' and orgid = '$userid'";
				$result = $sqli->query($sql);
				if($result === false)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
			else
			{
				return 0;
			}
		}

		public function add_product_image($img_name)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			if($is_provider == 1)
			{
				$sql = "INSERT INTO `productimages`(`productid`, `image`) VALUES ('$productid',$img_name)";
				$result = $sqli->query($sql);
				if($result === false)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
			else
			{
				return 0;
			}
		}

		public function approve_product_request()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			$check_if_requested = $this->check_request_status();
			if($is_provider == 1)
			{
				$sql = "UPDATE `productrequests` set status = 1 WHERE  productid = '$productid'";
				$result = mysqli_query($db_handle,$sql);
				if($result)
				{
					return 1;
				}
				else
				{
					return 0;
				}

			}
			else
			{
				return 0;
			}
		}

		public function delete_product_image($product_image_id)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			if($is_provider == 1)
			{
				$sql = "DELETE from productimages where id = '$product_image_id' and productid = '$productid'";
				$result = $sqli->query($sql);
				if($result === false)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
		}

		public function change_product_thumb($new_image)
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$is_provider = $this->validate_product_provider();
			if($is_provider == 1)
			{
				$sql = "UPDATE products set image = '$new_image' where productid = '$productid'";
				$result = $sqli->query($sql);
				if($result === false)
				{
					return 0;
				}
				else
				{
					return 1;
				}
		}

		public function count_requests()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$sql = "SELECT * from productrequests where productid = '$productid'";
			$result = $sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				return 1;
			}

		}

		public function validate_product_provider()
		{
			$providerid = $this->providerid;
			$productid = $this->productid;
			$userid = $this->userid;
			$time_now =$this->time_now;
			$sql = "select * from products where orgid = '$userid' and productid = '$productid'";
			$result = $sqli->query($sql);
			if($result === false)
			{
				return 0;
			}
			else
			{
				$count = $sqli->num_rows($result);
				if($count > 0)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}

		}
	}


?>