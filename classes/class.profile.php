<?php
include("class.config.php");
class profile extends db_connection
{
	var $client_id;
	var $user_id;
	var $time_now;

	public function __contruct($client_id,$user_id)
	{
		$this->client_id = $client_id;
		$this->user_id = $user_id;
		$this->time_now = time();
		$this->connect();
	}

	public function set_client_id($id)
	{
		$this->client_id = $id;
	}

	public function get_user_profile()
	{
		$client_id = $this->client_id;
		$sql = "Select fullname,hash,image,description as about,type,gender,lastseen,country from members where hash = '$client_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					return $data;
				}
			}
		}
	}

	public function get_organization_profile($id)
	{
		$client_id = $this->client_id;
		$sql = "Select * from organization where orgid = '$id'";
		$result = $this->mysqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					return $data;
				}
			}
			else
			{
				return 0;
			}
		}
	}

	public function get_staff_profile()
	{
		$client_id = $this->client_id;
		$sql = "Select * from members where userid = '$client_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					$user_or_staff = $data['type'];
					if($user_or_staff == "staff")
					{
						$get_org_info = $this->get_org_info_from_staff($client_id);
						if($get_org_info == 0)
						{
							return 0;
						}
						else
						{
							$all_results = array_merge($data,$get_org_info);
							return $all_results;
						}
					}
				}
			}
		}
	}

	public function get_org_info_from_staff($id)
	{
		$sql = "select * from staff where staffid = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data =  $result->fetch_assoc())
				{
					$org_id = $data['orgid'];
					$org_info = $this->$get_organization_profile($org_id);
					if($org_info == 0)
					{
						return 0;
					}
					else
					{
						return $org_info;
					}
				}
			}
		}
	}

	public function create_profile($fullname,$gender,$country,$status,$username,$password,$email,$type,$description,$lastseen,$image,$phone,$password)
	{
		$user_id = $this->user_id;
		$time_now = $this->time_now;

		$sql = "INSERT INTO `members`(`hash`, `status`, `fullname`, `username`, `email`, `password`, `type`, `description`, `dateofreg`, `lastseen`, `gender`, `phone`, `image`, `country`) VALUES ('$user_id',1,$fullname,$username,$email,$password,'$type','I Love Haskaar! No, Seriously , I do. :)','$time_now','$time_now',$gender,$phone,$image,$country)";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$sql = "INSERT INTO `brief` (`sex`, `status`, `relationship`, `education`, `religion`, `userid`) VALUES ($gender,'Single','none','none','none','none')";
			$sql2 = "INSERT INTO `lastseen`(`hash`, `dateof`) VALUES ('$user_id','$time_now')";
			$sql3 = "INSERT INTO `logins`(`type`, `hash`, `dateof`) VALUES ('login',$user_id,$time_now)";
			$result = $this->sqli->query($sql);
			$result2 = $this->sqli->query($sql2);
			$result3 = $this->sqli->query($sql3);
			if($result === false || $result2 ===  false || $result3 === false)
			{
				return 0;
			}
			else
			{
				if($type == "provider")
				{
					$org_id = sha1(md5($user_id.$time_now));
					$sql = "INSERT INTO `staff`(`orgid`, `staffid`, `dateadded`) VALUES ('$org_id','$user_id','$time_now')";
					$sql2 = "INSERT INTO `organization`( `orgid`, `email`, `status`) VALUES ('$org_id',$email,0)";
					$result = $this->sqli->query($sql);
					$result2 = $this->sqli->query($sql2);
					if($result === false || $result2 === false)
					{
						return 0;
					}
					else
					{
						return 1;
					}

				}
				else
				{
					return 1;
				}
			}
			
		}
	}

	public function edit_profile($fullname,$email,$phone,$relationship,$religion,$status,$education)
	{
		$user_id =  $this->user_id;
		$profile_info_count = 0;
		$brief_info_count = 0;
		$edit_committed = 0;
		if($fullname !== "" && $fullname !== null && $fullname !== "null")
		{
			$fullname = "$fullname";
			$profile_info_count = $profile_info_count + 1;
		}
		else
		{
			$fullname = "fullname";
		}
		if($email !== "" && $email !== null && $email !== "null")
		{
			$email = "$email";
			$profile_info_count = $profile_info_count + 1;
		}
		else
		{
			$email = "email";
		}
		if($phone !== "" && $phone !== null && $phone !== "null")
		{
			$phone = "$phone";
			$profile_info_count = $profile_info_count + 1;
		}
		else
		{
			$phone = "phone";
		}
		if($relationship !== "" && $relationship !== null && $relationship !== "null")
		{
			$relationship = "$relationship";
			$brief_info_count = $brief_info_count + 1;
		}
		else
		{
			$relationship = "relationship";
		}
		if($religion !== "" && $religion !== null && $religion !== "null")
		{
			$religion = "$religion";
			$brief_info_count = $brief_info_count + 1;
		}
		else
		{
			$religion = "religion";
		}
		if($education !== "" && $education !== null && $education !== "null")
		{
			$education = "$education";
			$brief_info_count = $brief_info_count + 1;
		}
		else
		{
			$education = "education";
		}
		if($status !== "" && $status !== null && $status !== "null")
		{
			$status = "$status";
			$brief_info_count = $brief_info_count + 1;
		}
		else
		{
			$status = "status";
		}

		if($brief_info_count > 0)
		{
			$sql = "UPDATE `brief` SET `status`=$status,`relationship`=$relationship,`education`=$education
			,`religion`=$religion
			WHERE `userid`= '$user_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				$edit_committed = $edit_committed - 1;
			}
			else
			{
				$edit_committed = $edit_committed + 1;
			}
		}
		if($profile_info_count > 0)
		{
			$sql = "UPDATE members SET fullname = $fullname , email = $email , phone = $phone WHERE hash = '$user_id'";
			$result = $this->sqli->query($sql);
			if($result === false)
			{
				$edit_committed = $edit_committed - 1;
			}
			else
			{
				$edit_committed = $edit_committed + 1;
			}
		}
		if($edit_committed > 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}

	}

	public function edit_profile_photo($photo)
	{
		$user_id = $this->user_id;
		$sql = "Update members set image = '$photo' where hash = '$user_id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	publci function set_organization_info()
	{

	}

	public function validate_user_type()
	{
		$sql = "Select * from members where userid = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				while($data = $result->fetch_assoc())
				{
					$type = $data['type'];
					return $type;
				}
			}
			else
			{
				return 0;
			}
		}
	
	}

	public function validate_user($id)
	{
		$sql = "Select * from members where userid = '$id'";
		$result = $this->sqli->query($sql);
		if($result === false)
		{
			return 0;
		}
		else
		{
			$counter = $result->num_rows;
			if($counter > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
?>