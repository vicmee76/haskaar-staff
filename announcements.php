<?php
session_start();
if (!(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != '')) 
{
  header ("Location: login.php"); 
}
else
{

  include"classes/class.announcement.php";
  include"classes/class.reaction.php"; 
  include"config.php";
  include"modules/input_module.php";
  include"modules/sql_module.php";
  $userid=$_SESSION['userid'];
  $fullname=$_SESSION['fullname'];
  $orgid=$_SESSION['loggedin'];
  last_seen($userid,$db_handle); 
  $announcement = new announcement($orgid);


}
include"header.php";
include"menu.php";
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
   <div class="content-main">
		<!--banner-->	
	     <div class="banner">
	    	<h2>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Announcement</span>
			<a href='#!' class='btn btn-sm btn-default pull-right  toggle_media_list' data-type='Announcement' id='0'>Add New Announcement</a>
			</h2>
	    </div>
	<!--//banner-->
	 	<div class="blank">
		

			<div class="blank-page" id='media_list'>
				
				<div class='gallery'>
					<div class='list-group list-group-alternate'> 
		        	<?php

		        		$announcement_list = $announcement->list_announcement();
		        		if($announcement_list['success'] == 1)
		        		{
		        			$announcement_data = $announcement_list['data'];
		        			for($i=0;$i<count($announcement_data);$i++)
		        			{
		        				$title = $announcement_data[$i]['title'];
		        				$description = $announcement_data[$i]['description'];
		        				$announcement_id = $announcement_data[$i]['hash'];
		        				$duration = $announcement_data[$i]['duration'];
		        				$timestamp = $announcement_data[$i]['timestamp'];
		        				$expiry_date = date('D d M Y',$timestamp + (86400 * $duration));
		        				$datetime = date('D d M Y',$timestamp);
		        				$reaction = new reactions($orgid,$announcement_id);
		        				$announcement_views = $reaction->count_views();
		        				
		        				print 
		        				"
									<a href='#!' class='list-group-item edit_notice' id='$announcement_id'data-title='$title' data-desc='$description' data-duration='$duration' data-exp='$expiry_date' data-views='$announcement_views' data-dt='$datetime'>
												<span class='badge'>$announcement_views</span> <i class='ti ti-email'></i> $title 
									</a>

		        				";
		        			}

		        		}
		        		else
		        		{
		        			print
		        			"
		        				<div class='alert alert-info'> You have no  announcements yet. <a href='#!' data-type='Announcement' class='toggle_media_list' id='0'> Add New </a></div>

		        			";
		        		}

		        	?>
	        		</div>
				</div>
				<div class='col-md-8 pull-right'>

				</div>
		    </div>
		    <div class="blank-page" id='add_media' style='display:none'>

		    	<div class="gallery">
				    <div id='results'></div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Title</label>
				        <input type="text" class="form-control1" id="title">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Description</label>
				        <input type="text" class="form-control1" id="description">
				    </div>
				    <div class="form-group">
				        <label class="control-label" for="inputSuccess1">Duration (Days)</label>
				        <input type="text" class="form-control1" id="duration">
				    </div>
				    <div class="form-group" id='naration' style='display:none'>
				        <label class="control-label" for="inputSuccess1">Summary</label>
				        <div class='alert alert-info' id='summary'>

				        </div>
				    </div>

				    
				       <a href='#!' id='add_announcement' class='btn btn-primary'> Post announcement </a>
				       <a href='#!' id='' class='btn btn-warning edits edit_announcement' style='display:none'>Edit announcement</a>
				       <a href='#!' id='' class='btn btn-info edits reset_announcement' style='display:none'>Reset announcement</a>
				       <a href='#!' id='' class='btn btn-danger edits delete_announcement' style='display:none'>Delete announcement</a>
				    <!-- end of Example 2 -->

				</div>


		    </div>
		</div>
	</div>
    <script type='text/javascript'>
        $(document).ready(function(){

          $('#filer_input').filer({
            showThumbs: true,
            addMore: true,
            allowDuplicates: false
          });
          $("#datetime").datepicker();

        });
    </script> 
			
		<!--//content-->
<?php
include"footer.php";
?>        